package com.letianpai.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.letianpai.R;

public class ListDialogAdapter extends BaseAdapter{
    private Context context;
    private LayoutInflater inflater;
    ArrayList<String>listItem;
    public ListDialogAdapter(Context context, ArrayList<String>listItem) {
        super();
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.listItem = listItem;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return listItem.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return listItem.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View layout = inflater.inflate(R.layout.list_dialog_item, parent, false);
        TextView dialog = (TextView) layout.findViewById(R.id.dialog);
        dialog.setText(listItem.get(position));
        return layout;
    }





}
