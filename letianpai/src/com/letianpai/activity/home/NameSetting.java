package com.letianpai.activity.home;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.letianpai.R;
import com.letianpai.application.ManageApplication;
import com.letianpai.utils.ChatUtils;
import com.letianpai.utils.XmppTool;

public class NameSetting extends Activity implements OnClickListener{
    //
    TextView pwd_1,pwd_2,title,headShare;
    View ll_up,ll_down,nextstep,headBackText;
    CheckBox cb_pwd;
    EditText number;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ManageApplication.getInstance().addActivity(this);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.home_reg);
        inint();
    }

    private void inint(){
        headBackText = findViewById(R.id.headBackText);
        headBackText.setOnClickListener(this);
        ll_up=findViewById(R.id.ll_up);
        ll_down=findViewById(R.id.ll_down);
        cb_pwd=(CheckBox) findViewById(R.id.cb_pwd);
        number=(EditText) findViewById(R.id.number);
        pwd_1=(TextView) findViewById(R.id.pwd_1);
        pwd_2=(TextView) findViewById(R.id.pwd_2);
        title=(TextView) findViewById(R.id.title);
        nextstep=findViewById(R.id.nextstep);
        nextstep.setOnClickListener(this);
        headShare = (TextView) findViewById(R.id.headShare);
        headShare.setText("");
        title.setText("输入您的名字");
        title.setGravity(Gravity.CENTER);
        number.setHint("输入真名或其他家人知道的名字");
        ll_up.setVisibility(View.GONE);
        ll_down.setVisibility(View.GONE);

        cb_pwd.setVisibility(View.GONE);
        pwd_1.setVisibility(View.GONE);
        pwd_2.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View arg0) {
        switch(arg0.getId()){
            case R.id.nextstep:
                // 获得名字
                String name=number.getText().toString();

                Intent intent1=new Intent(NameSetting.this,HeadSetting.class);
                startActivity(intent1);
                boolean a = ChatUtils.changename(XmppTool.getConnection(),name);
                Log.d("zzzz", a+"");
                break;
            case R.id.headBackText:
                finish();
                break;
            default:
                break;
        }
    }

}
