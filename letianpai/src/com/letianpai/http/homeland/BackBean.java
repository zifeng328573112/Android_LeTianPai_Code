package com.letianpai.http.homeland;

public class BackBean {
	private String ofserviceid;
	private String ofroomid;
	private String gname;
	private String owner;
	private String max_num;
	private String user_num;
	private String addtime;
	private String is_managers;
	private boolean checked = false;
	public String getOfserviceid() {
		return ofserviceid;
	}

	public void setOfserviceid(String ofserviceid) {
		this.ofserviceid = ofserviceid;
	}
	public String getOfroomid() {
		return ofroomid;
	}
	public void setOfroomid(String ofroomid) {
		this.ofroomid = ofroomid;
	}
	public String getGname() {
		return gname;
	}
	public void setGname(String gname) {
		this.gname = gname;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public String getMax_num() {
		return max_num;
	}
	public void setMax_num(String max_num) {
		this.max_num = max_num;
	}
	public String getUser_num() {
		return user_num;
	}
	public void setUser_num(String user_num) {
		this.user_num = user_num;
	}
	public String getAddtime() {
		return addtime;
	}
	public void setAddtime(String addtime) {
		this.addtime = addtime;
	}
	public String getIs_managers() {
		return is_managers;
	}
	public void setIs_managers(String is_managers) {
		this.is_managers = is_managers;
	}
	public boolean isChecked() {
		return checked;
	}
	public void setChecked(boolean checked) {
		this.checked = checked;
	}
	
}
