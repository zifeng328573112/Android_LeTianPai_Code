package com.letianpai.adapter.photoalbum;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import com.letianpai.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 14-9-27
 * Time: 下午3:50
 * 日期分组里面的照片
 */
public class PhotoMemberAdapter extends BaseAdapter {
    DisplayImageOptions options; // 配置图片加载及显示选项
    private ImageLoader imageLoader;
    private Context context;
    private List<PhotoInfo> photoInfoList;
    private LayoutInflater layoutInflater;

    public PhotoMemberAdapter(Context context, List<PhotoInfo> photoInfoList) {
        this.context = context;
        this.photoInfoList = photoInfoList;
        layoutInflater = LayoutInflater.from(context);
        // 配置图片加载及显示选项（还有一些其他的配置，查阅doc文档吧）
        options = new DisplayImageOptions.Builder()
//                .showStubImage(R.drawable.ic_launcher) // 在ImageView加载过程中显示图片
                .showImageForEmptyUri(R.drawable.ic_launcher) // image连接地址为空时
                .showImageOnFail(R.drawable.add_home) // image加载失败
                .cacheInMemory(true) // 加载图片时会在内存中加载缓存
                .cacheOnDisc(true) // 加载图片时会在磁盘中加载缓存
                .displayer(new RoundedBitmapDisplayer(5, false)) // 设置用户加载图片task(这里是圆角图片显示)
                .build();
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
    }

    @Override
    public int getCount() {
        if (null == photoInfoList) {
            return 0;
        }
        return photoInfoList.size();
    }


    @Override
    public Object getItem(int position) {
        return photoInfoList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        ViewHolder holder = null;
        if (view == null) {
            holder = new ViewHolder();
            view = layoutInflater.inflate(R.layout.local_album_photo_member_item, null);
            holder.memberCb = (CheckBox) view.findViewById(R.id.local_album_member_cb);
            holder.memberIv = (ImageView) view.findViewById(R.id.local_album_member_iv);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        holder.memberCb.setOnCheckedChangeListener(new CheckChangeListener(position));
        PhotoInfo photoInfo = photoInfoList.get(position);
        imageLoader.displayImage("file://" + photoInfo.getPhotoPath(), holder.memberIv, options, null);
        if (photoInfo.isWhetherSelected()) {
            holder.memberCb.setChecked(true);
        } else {
            holder.memberCb.setChecked(false);
        }
//        view.setOnClickListener(new CheckListener(photoInfo, holder.memberCb));
        return view;
    }

    class CheckChangeListener implements CompoundButton.OnCheckedChangeListener {
        int position;

        CheckChangeListener(int position) {
            this.position = position;
        }

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (isChecked) {
                photoInfoList.get(position).setWhetherSelected(true);
            } else {
                photoInfoList.get(position).setWhetherSelected(false);
            }
        }
    }


    private class ViewHolder {
        ImageView memberIv;
        CheckBox memberCb;
    }
}
