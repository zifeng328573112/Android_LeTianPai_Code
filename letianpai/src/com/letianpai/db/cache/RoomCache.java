package com.letianpai.db.cache;

import android.content.Context;
import android.content.SharedPreferences;
import com.letianpai.db.bean.Room;

/**
 * Created by Administrator on 2014/10/8.
 */
public class RoomCache {
    private static final String ROOM_CACHE = "room_letianlun";

    public static void modifyRoom(Context context, Room room) {
        SharedPreferences sharePre = context.getSharedPreferences(ROOM_CACHE, 0);
        SharedPreferences.Editor editor = sharePre.edit();
        editor.putString("roomname", room.getRoomname());
        editor.putString("roomId", room.getRoomId());
        editor.commit();
    }

    public static Room getRoom(Context context) {
        SharedPreferences sharePre = context.getSharedPreferences(ROOM_CACHE, 0);
        Room room = new Room();
        room.setRoomId(sharePre.getString("roomId", ""));
        room.setRoomname(sharePre.getString("roomname", ""));
        return room;
    }

}
