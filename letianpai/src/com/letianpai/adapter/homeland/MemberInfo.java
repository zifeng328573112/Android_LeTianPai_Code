package com.letianpai.adapter.homeland;

import java.io.Serializable;

/**
 * Created by Administrator on 2014/10/13.
 */
public class MemberInfo implements Serializable {
    private String headImaUrl;
    private String username;
    private String relation;
    private String jointime;
    private String nickname;
    private String sex;

    public String getHeadImaUrl() {
        return headImaUrl;
    }

    public void setHeadImaUrl(String headImaUrl) {
        this.headImaUrl = headImaUrl;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

    public String getJointime() {
        return jointime;
    }

    public void setJointime(String jointime) {
        this.jointime = jointime;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }
}
