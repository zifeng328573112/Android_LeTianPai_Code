package com.letianpai.adapter.photoalbum;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 14-9-27
 * Time: 上午11:37
 * 照片信息
 */
public class PhotoInfo implements Serializable {
    //标识，路径，大小，名称，类型，标题，创建时间、来源
//   {"_id", "_data", "_size", "_display_name", "mime_type",
//            "title", "datetaken", "bucket_display_name");
    private int id;
    private String photoPath;
    private long size;
    private String photoName;//带后缀的   _display_name
    private String mimeType;
    private String title;
    private String createDate;//创建时间 long to string  datetaken
    private String sourceFrom;//图片来源，相机，网络 bucket_display_name
    private String thumbnailPath; //缩略图路径
    private boolean whetherSelected = false;//是否选中

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPhotoPath() {
        return photoPath;
    }

    public void setPhotoPath(String photoPath) {
        this.photoPath = photoPath;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public String getPhotoName() {
        return photoName;
    }

    public void setPhotoName(String photoName) {
        this.photoName = photoName;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getSourceFrom() {
        return sourceFrom;
    }

    public void setSourceFrom(String sourceFrom) {
        this.sourceFrom = sourceFrom;
    }

    public String getThumbnailPath() {
        return thumbnailPath;
    }

    public void setThumbnailPath(String thumbnailPath) {
        this.thumbnailPath = thumbnailPath;
    }

    public boolean isWhetherSelected() {
        return whetherSelected;
    }

    public void setWhetherSelected(boolean whetherSelected) {
        this.whetherSelected = whetherSelected;
    }
}

