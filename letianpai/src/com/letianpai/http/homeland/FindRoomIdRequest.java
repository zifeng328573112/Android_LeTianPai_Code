package com.letianpai.http.homeland;


public class FindRoomIdRequest {
	private String head;
	private FindRoomIdBean content;
	public String getHead() {
		return head;
	}
	public void setHead(String head) {
		this.head = head;
	}
	public FindRoomIdBean getContent() {
		return content;
	}
	public void setContent(FindRoomIdBean content) {
		this.content = content;
	}
	
}
