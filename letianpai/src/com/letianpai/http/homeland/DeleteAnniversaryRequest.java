package com.letianpai.http.homeland;

public class DeleteAnniversaryRequest {
	private String head;
	private DeleteAnniversaryBean content;

	public String getHead() {
		return head;
	}

	public void setHead(String head) {
		this.head = head;
	}

	public DeleteAnniversaryBean getContent() {
		return content;
	}

	public void setContent(DeleteAnniversaryBean content) {
		this.content = content;
	}

}
