package com.letianpai.activity.home;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.RequestQueue;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.letianpai.R;
import com.letianpai.application.ManageApplication;
import com.letianpai.bean.BackResponse;
import com.letianpai.db.Constant;
import com.letianpai.dialog.wheelview.NumericWheelAdapter;
import com.letianpai.dialog.wheelview.OnWheelChangedListener;
import com.letianpai.dialog.wheelview.WheelView;
import com.letianpai.http.RequestBean.UserBirthday;
import com.letianpai.http.RequestBean.UserBirthdaybean;
import com.letianpai.utils.JsonUtil;

import java.text.DecimalFormat;
import java.util.*;

public class DaySetting extends Activity {

    private TextView btn;
    private Dialog dialog;
    private static int START_YEAR = 1000, END_YEAR = 2014;
    // boolean is_slip=false;
    RequestQueue requestQueue;
    StringBuffer birthday = new StringBuffer();
    String myear;
    String mmonth;
    String mday;
    TextView headShare;
    View headBackText;
    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            super.handleMessage(msg);
            switch (msg.what) {
                case 1:
                    btn.setText("下一步");

            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        ManageApplication.getInstance().addActivity(this);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.home_time_layout);
        headShare = (TextView) findViewById(R.id.headShare);
        headShare.setText("");
        headBackText = findViewById(R.id.headBackText);
        headBackText.setOnClickListener(new Dayclick());
        btn = (TextView) findViewById(R.id.btn_datetime_sure);
        requestQueue = Volley.newRequestQueue(this);
        showDateTimePicker();
    }

    private void showDateTimePicker() {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        myear = calendar.get(Calendar.YEAR) + "";
        int month = calendar.get(Calendar.MONTH);
        mmonth = calendar.get(Calendar.MONTH) + "";
        int day = calendar.get(Calendar.DATE);
        mday = calendar.get(Calendar.DATE) + "";
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        Log.d("zzzz", day + hour + minute + "");
        // 添加大小月月份并将其转换为list,方便之后的判断
        String[] months_big = {"1", "3", "5", "7", "8", "10", "12"};
        String[] months_little = {"4", "6", "9", "11"};

        final List<String> list_big = Arrays.asList(months_big);
        final List<String> list_little = Arrays.asList(months_little);

        // 年
        final WheelView wv_year = (WheelView) this.findViewById(R.id.year);
        wv_year.setAdapter(new NumericWheelAdapter(START_YEAR, END_YEAR));// 设置"年"的显示数据
        wv_year.setCyclic(true);// 可循环滚动
        // 添加文字 wv_year.setLabel("年");
        wv_year.setCurrentItem(year - START_YEAR);// 初始化时显示的数据

        // 月
        final WheelView wv_month = (WheelView) this.findViewById(R.id.month);
        wv_month.setAdapter(new NumericWheelAdapter(1, 12));
        wv_month.setCyclic(true);
        // 添加文字 wv_month.setLabel("月");
        wv_month.setCurrentItem(month);

        // 日
        final WheelView wv_day = (WheelView) this.findViewById(R.id.day);
        wv_day.setCyclic(true);
        // 判断大小月及是否闰年,用来确定"日"的数据
        if (list_big.contains(String.valueOf(month + 1))) {
            wv_day.setAdapter(new NumericWheelAdapter(1, 31));
        } else if (list_little.contains(String.valueOf(month + 1))) {
            wv_day.setAdapter(new NumericWheelAdapter(1, 30));
        } else {
            // 闰年
            if ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0)
                wv_day.setAdapter(new NumericWheelAdapter(1, 29));
            else
                wv_day.setAdapter(new NumericWheelAdapter(1, 28));
        }
        // 添加文字 wv_day.setLabel("日");
        wv_day.setCurrentItem(day - 1);

        // 添加"年"监听
        OnWheelChangedListener wheelListener_year = new OnWheelChangedListener() {
            public void onChanged(WheelView wheel, int oldValue, int newValue) {
                handler.sendEmptyMessage(1);
                int year_num = newValue + START_YEAR;
                Log.i("info", year_num + "年");
                myear = year_num + "-";
                // 判断大小月及是否闰年,用来确定"日"的数据
                if (list_big
                        .contains(String.valueOf(wv_month.getCurrentItem() + 1))) {
                    wv_day.setAdapter(new NumericWheelAdapter(1, 31));
                } else if (list_little.contains(String.valueOf(wv_month
                        .getCurrentItem() + 1))) {
                    wv_day.setAdapter(new NumericWheelAdapter(1, 30));
                } else {
                    if ((year_num % 4 == 0 && year_num % 100 != 0)
                            || year_num % 400 == 0)
                        wv_day.setAdapter(new NumericWheelAdapter(1, 29));
                    else
                        wv_day.setAdapter(new NumericWheelAdapter(1, 28));
                }
            }
        };
        // 添加"月"监听
        OnWheelChangedListener wheelListener_month = new OnWheelChangedListener() {
            public void onChanged(WheelView wheel, int oldValue, int newValue) {
                handler.sendEmptyMessage(1);
                int month_num = newValue + 1;
                Log.i("info", month_num + "月");
                mmonth = month_num + "-";
                // 判断大小月及是否闰年,用来确定"日"的数据
                if (list_big.contains(String.valueOf(month_num))) {
                    wv_day.setAdapter(new NumericWheelAdapter(1, 31));
                } else if (list_little.contains(String.valueOf(month_num))) {
                    wv_day.setAdapter(new NumericWheelAdapter(1, 30));
                } else {
                    if (((wv_year.getCurrentItem() + START_YEAR) % 4 == 0 && (wv_year
                            .getCurrentItem() + START_YEAR) % 100 != 0)
                            || (wv_year.getCurrentItem() + START_YEAR) % 400 == 0)
                        wv_day.setAdapter(new NumericWheelAdapter(1, 29));
                    else
                        wv_day.setAdapter(new NumericWheelAdapter(1, 28));
                }
            }
        };
        OnWheelChangedListener wheelListener_day = new OnWheelChangedListener() {

            @Override
            public void onChanged(WheelView wheel, int oldValue, int newValue) {
                int day_num = newValue + 1;
                Log.i("info", day_num + "日");
                mday = day_num + "-";
                handler.sendEmptyMessage(1);
            }
        };

        wv_year.addChangingListener(wheelListener_year);
        wv_month.addChangingListener(wheelListener_month);
        wv_day.addChangingListener(wheelListener_day);

        // 根据屏幕密度来指定选择器字体的大小
        int textSize = 0;

        textSize = 40;

        wv_day.TEXT_SIZE = textSize;
        wv_month.TEXT_SIZE = textSize;
        wv_year.TEXT_SIZE = textSize;

        // 确定
        btn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                // 如果是个数,则显示为"02"的样式
                String parten = "00";
                DecimalFormat decimal = new DecimalFormat(parten);
                new Thread(new Runnable() {

                    @Override
                    public void run() {
                        // TODO Auto-generated method stub
                        post();
                    }
                }).start();
            }
        });

    }

    public void post() {
        birthday.append(myear);
        birthday.append(mmonth);
        birthday.append(mday);
        Log.d("zzzz", birthday + "");
        Map<String, String> map = new HashMap<String, String>();
        UserBirthday userInfo = new UserBirthday();
        userInfo.setUsername(RegsActivity.phonenumber);
        userInfo.setBirthday(birthday + "");
        UserBirthdaybean user = new UserBirthdaybean();
        user.setHead("changeUserInfoServlet");
        user.setContent(userInfo);
        // msg值
        String msg = JsonUtil.objectToString(user, UserBirthdaybean.class);
        String url = Constant.URL + "?msg=" + msg;
        Log.d("zzzz", url);
        map.put("msg", msg);
        StringRequest request = new StringRequest(url, new ls(), new el());
        requestQueue.add(request);

    }

    class ls implements Listener<String> {

        @Override
        public void onResponse(String arg0) {
            BackResponse br = (BackResponse) JsonUtil.StringToObject(arg0,
                    BackResponse.class);
            if (br.getStatus().equals("SUCCESS")) {
                Toast.makeText(DaySetting.this, br.getData().getScalar(),
                        Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(DaySetting.this,
                        SuccessActivity.class);
                startActivity(intent);
            }

        }

    }

    class el implements ErrorListener {

        @Override
        public void onErrorResponse(VolleyError arg0) {
            Toast.makeText(DaySetting.this, "请求失败", Toast.LENGTH_SHORT).show();
        }

    }

    class Dayclick implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.headBackText:
                    finish();
                    break;

                default:
                    break;
            }
        }

    }
}
