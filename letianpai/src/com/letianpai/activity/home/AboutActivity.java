package com.letianpai.activity.home;

import com.letianpai.R;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

/*
 * 关于乐天家书
 */
public class AboutActivity extends Activity{
	TextView headText;
	View headBackText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.about_letianpai);
        initView();
    }
    private void initView() {
    	headText = (TextView) findViewById(R.id.headText);
    	headBackText = findViewById(R.id.headBackText);
    	headBackText.setVisibility(View.VISIBLE);
    	headBackText.setOnClickListener(new AboutClick());
    	headText.setText("关于乐天派");
	}
    class AboutClick implements View.OnClickListener{

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.headBackText:
				finish();
				break;

			default:
				break;
			}
		}
    	
    }
}
