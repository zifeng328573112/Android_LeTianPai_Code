package com.letianpai.http.homeland;

public class MemBean {

	private String username;
	private String relation;
	private String jointime;
	private String nickname;
	private String is_managers;
	private String email;
	private String sex;
	private String birthdayte;
	private String avatar;
	
	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public String getBirthdayte() {
		return birthdayte;
	}

	public void setBirthdayte(String birthdayte) {
		this.birthdayte = birthdayte;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getIs_managers() {
		return is_managers;
	}

	public void setIs_managers(String is_managers) {
		this.is_managers = is_managers;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getRelation() {
		return relation;
	}

	public void setRelation(String relation) {
		this.relation = relation;
	}

	public String getJointime() {
		return jointime;
	}

	public void setJointime(String jointime) {
		this.jointime = jointime;
	}

}
