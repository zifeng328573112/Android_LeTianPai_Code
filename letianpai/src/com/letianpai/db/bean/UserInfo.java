package com.letianpai.db.bean;

import java.io.Serializable;

/**
 * to
 * Created by Administrator on 2014/10/8.
 */
public class UserInfo implements Serializable {
    private String userId = "";
    private String username;
    private String password;
    private String nickname;
    private String sex;
    private String headPhoto;
    private String email;
    private String roomId;
    private boolean loginType = false;

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getHeadPhoto() {
        return headPhoto;
    }

    public void setHeadPhoto(String headPhoto) {
        this.headPhoto = headPhoto;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isLoginType() {
        return loginType;
    }

    public void setLoginType(boolean loginType) {
        this.loginType = loginType;
    }
}
