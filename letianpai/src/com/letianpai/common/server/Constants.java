package com.letianpai.common.server;

/**
 * 服务永久常量
 */
public final class Constants {
    public static final String URL = "http://app.dial.letianpai.com/accept/api";
    public static final String USER_CACHE = "letianlun_user_cache";
}
