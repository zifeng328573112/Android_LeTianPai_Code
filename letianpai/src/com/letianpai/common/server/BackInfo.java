package com.letianpai.common.server;

import com.letianpai.utils.JsonUtil;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 14-9-25
 * Time: 上午9:18
 * To change this template use File | Settings | File Templates.
 */
public class BackInfo {
    public BackInfo() {
    }

    public BackInfo(String json) {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(json);
            this.status = jsonObject.getString("status");
            this.data = jsonObject.getString("data");
            this.message = jsonObject.getString("message");
        } catch (Exception e) {
            this.status = "EXCEPTION";
            this.message = "格式设计";
            this.data = "";
        }


    }

    private String status;//返回状态
    private String data;//返回内容
    private String message;  //  返回内容

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

