package com.letianpai.http.RequestBean;

import java.util.List;

import com.letianpai.bean.InviteUser;

public class UserinformationData {
	private String username;
	private String nickname;
	private String email;
	private String is_temporaryPassword;
	private InviteUser invite_user;
	private String sex;
	private String birthdate;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getIs_temporaryPassword() {
		return is_temporaryPassword;
	}

	public void setIs_temporaryPassword(String is_temporaryPassword) {
		this.is_temporaryPassword = is_temporaryPassword;
	}

	public InviteUser getInvite_user() {
		return invite_user;
	}

	public void setInvite_user(InviteUser invite_user) {
		this.invite_user = invite_user;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(String birthdate) {
		this.birthdate = birthdate;
	}

}
