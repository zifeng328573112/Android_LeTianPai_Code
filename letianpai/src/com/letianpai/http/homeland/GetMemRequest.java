package com.letianpai.http.homeland;

public class GetMemRequest {
	private String head;
	private GetMemBean content;
	public String getHead() {
		return head;
	}
	public void setHead(String head) {
		this.head = head;
	}
	public GetMemBean getContent() {
		return content;
	}
	public void setContent(GetMemBean content) {
		this.content = content;
	}
	
}
