package com.letianpai.bean;

public class InviteUser {
	private String invite_from_username;
	private String invite_to_username;
	private String invite_time;
	public String getInvite_from_username() {
		return invite_from_username;
	}
	public void setInvite_from_username(String invite_from_username) {
		this.invite_from_username = invite_from_username;
	}
	public String getInvite_to_username() {
		return invite_to_username;
	}
	public void setInvite_to_username(String invite_to_username) {
		this.invite_to_username = invite_to_username;
	}
	public String getInvite_time() {
		return invite_time;
	}
	public void setInvite_time(String invite_time) {
		this.invite_time = invite_time;
	}
	
}
