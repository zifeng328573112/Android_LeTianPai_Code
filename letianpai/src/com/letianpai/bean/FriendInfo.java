package com.letianpai.bean;

import java.io.Serializable;

import com.letianpai.utils.XmppTool;

import android.graphics.Bitmap;
import android.text.TextUtils;


@SuppressWarnings("serial")
public class FriendInfo implements Serializable{
	private String username;
	private String nickname;
	private String mood;
	private Bitmap pic;
	private int messageCount;
	
	
	public Bitmap getPic() {
		return pic;
	}
	public void setPic(Bitmap pic) {
		this.pic = pic;
	}
	public int getMessageCount() {
		return messageCount;
	}
	public void setMessageCount(int messageCount) {
		this.messageCount = messageCount;
	}
	public String getMood() {
		return mood;
	}
	public void setMood(String mood) {
		this.mood = mood;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getNickname() {
		if(TextUtils.isEmpty(nickname))
			return username;
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	
	public String getJid(){
		if (username == null) return null;
		return username + "@" +XmppTool.SERVER_NAME;
	}
}
