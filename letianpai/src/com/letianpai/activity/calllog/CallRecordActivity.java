package com.letianpai.activity.calllog;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;

import com.letianpai.R;
import com.letianpai.adapter.CallLogAdapter;
import com.letianpai.bean.UserCallLog;
import com.letianpai.dialog.CustomDialog;
import com.letianpai.utils.CallUtils;
import com.umeng.analytics.MobclickAgent;

public class CallRecordActivity extends Activity implements OnScrollListener {
	/*
	 * 通话记录
	 */
	private CallLogAdapter calllog;
	TextView select;
	View headBackText;
	TextView headText;
	Intent intent;
	static List<UserCallLog> arraylist = new ArrayList<UserCallLog>();
	List<UserCallLog> arraylistSum = new ArrayList<UserCallLog>();
	List<Integer> useridList = new ArrayList<Integer>();// 删除通话记录
	ListView lv;
	TextView headShare;
	private int lastItem;
	private int count;
	int ten = 1;

	public static boolean checkBoxVisible = false;// 显示CheckBox
	public static boolean checkBoxSelected = false;// CheckBox全选
	public static boolean checkBoxUnSelected = false;// CheckBox取消

	CheckBox checkBox;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.call_record);
		initView();

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		MobclickAgent.onPageStart("SplashScreen"); // 统计页面
		MobclickAgent.onResume(this); // 统计时长
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		MobclickAgent.onPageEnd("SplashScreen"); // 保证 onPageEnd 在onPause
													// 之前调用,因为 onPause 中会保存信息
		MobclickAgent.onPause(this);
	}

	public void initView() {
		new Thread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub

				arraylist = CallUtils.etCallLog(CallRecordActivity.this, 0, 10);
				count = arraylist.size();
				mHandler.sendEmptyMessage(1);
			}
		}).start();
		intent = new Intent(this, CallRecordEditActivity.class);
		calllog = new CallLogAdapter(this, arraylist,
				new TitleOnClickListener());
		lv = (ListView) findViewById(R.id.lv_tv);
		headText = (TextView) findViewById(R.id.headText);
		select = (TextView) findViewById(R.id.select);
		headText.setText("通话记录");
		headShare = (TextView) findViewById(R.id.headShare);
		headShare.setText("删除");
		headShare.setOnClickListener(new TitleOnClickListener());
		checkBox = (CheckBox) findViewById(R.id.checkBox);
		headBackText = findViewById(R.id.headBackText);
		headBackText.setVisibility(View.INVISIBLE);
		headBackText.setOnClickListener(new TitleOnClickListener());
		select.setOnClickListener(new TitleOnClickListener());
		lv.setOnItemClickListener(new ListViewListener());
		lv.setOnScrollListener(this);
		lv.setAdapter(calllog);

	}

	class TitleOnClickListener implements View.OnClickListener {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			switch (v.getId()) {
			case R.id.headShare:
				if (headText.getVisibility() == View.VISIBLE
						&& checkBoxVisible == false) {
					select.setVisibility(View.VISIBLE);
					headBackText.setVisibility(View.VISIBLE);
					headText.setVisibility(View.INVISIBLE);
					checkBoxVisible = true;
					calllog.notifyDataSetChanged();
				}
				// 删除通话记录
				else if (checkBoxVisible == true) {
					dialog();
				}

				break;
			case R.id.headBackText:
				backCallLog();
				break;
			case R.id.select:
				if (select.getVisibility() == View.VISIBLE
						&& select.getText().equals("全选")) {
					checkBoxSelected = true;
					for (int i = 0; i < arraylist.size(); i++) {
						arraylist.get(i).setChecked(true);
					}
					calllog.setArraylist(arraylist);
					select.setText("取消");
					calllog.notifyDataSetChanged();
				} else if (select.getVisibility() == View.VISIBLE
						&& select.getText().equals("取消")) {
					checkBoxSelected = false;
					for (int i = 0; i < arraylist.size(); i++) {
						arraylist.get(i).setChecked(false);
					}
					calllog.setArraylist(arraylist);
					select.setText("全选");
					calllog.notifyDataSetChanged();
				}
				break;
			case R.id.checkBox:
				if (arraylist.get((Integer) v.getTag()).isChecked() == false) {
					arraylist.get((Integer) v.getTag()).setChecked(true);
				} else {
					arraylist.get((Integer) v.getTag()).setChecked(false);
				}
				for(int i = 0;i<arraylist.size();i++){
					Log.d("zzzz", arraylist.get(i).isChecked()+"");
				}
				break;
			default:
				break;
			}
		}

	}

	class ListViewListener implements OnItemClickListener {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			// TODO Auto-generated method stub
			intent.setClass(CallRecordActivity.this,
					CallRecordEditActivity.class);
			Bundle bundle = new Bundle();
			bundle.putString("username", arraylist.get(position).getUserName());
			bundle.putString("number", arraylist.get(position).getPhoneNum());
			bundle.putString("photoid", arraylist.get(position).getPhotoId());
			intent.putExtras(bundle);
			startActivity(intent);
		}

	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		// TODO Auto-generated method stub
		if (lastItem == count && scrollState == this.SCROLL_STATE_IDLE) {

			mHandler.sendEmptyMessage(0);

		}
	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {
		// TODO Auto-generated method stub

		lastItem = firstVisibleItem + visibleItemCount;

	}

	// 加载更多数据
	private void loadMoreData() {
		count = calllog.getCount();
		List<UserCallLog> arraylist1 = new ArrayList<UserCallLog>();
		arraylist1 = CallUtils.etCallLog(this, count, count + 10);
		arraylist.addAll(arraylist1);
		count = arraylist.size();

	}

	// 声明Handler
	private Handler mHandler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			switch (msg.what) {
			case 0:
				try {
					Thread.sleep(300);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				loadMoreData(); // 加载更多数据，这里可以使用异步加载
				calllog.setArraylist(arraylist);
				calllog.notifyDataSetChanged();

				/*
				 * int sum = arraylistSum.size(); if (count == sum) {
				 * Toast.makeText(CallLogActivity.this, "木有更多数据！", 3000)
				 * .show(); }
				 */
				break;
			case 1:
				calllog.setArraylist(arraylist);
				calllog.notifyDataSetChanged();

				break;
			case 2:
				calllog.setArraylist(arraylist);
				calllog.notifyDataSetChanged();
				backCallLog();
				break;
			default:
				break;
			}
		};
	};

	protected void dialog() {
		final CustomDialog dialog = new CustomDialog(this,
				R.layout.custom_dialog);
		dialog.setLeftOnClick(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				deleteCallLog();
				dialog.dismiss();
			}
		});
		dialog.setRightOnClick(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		dialog.show();
	}

	public void deleteCallLog() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				for (int i = 0; i < arraylist.size(); i++) {
					if (arraylist.get(i).isChecked()) {
						useridList.add(arraylist.get(i).getUserId());
					}

				}
				CallUtils.delete(CallRecordActivity.this, useridList);
				List<UserCallLog> arraylist2 = new ArrayList<UserCallLog>();
				List<UserCallLog> arraylist3 = new ArrayList<UserCallLog>();
				for (int i = 0; i < arraylist.size(); i++) {
					arraylist3.add(arraylist.get(i));
				}
				int j = 0;
				for (int i = 0; i < arraylist3.size(); i++) {
					if (arraylist3.get(i).isChecked()) {
						arraylist.remove(i - j);
						j++;
					}
				}
				Log.d("zzzz", count+"count"+"        "+(count-useridList.size())+"");
				arraylist2 = CallUtils.etCallLog(CallRecordActivity.this, count
						- useridList.size(), count);
				Log.d("zzzz", count+"");
				arraylist.addAll(arraylist2);
				count = arraylist.size();
				useridList.clear();
				mHandler.sendEmptyMessage(2);
				
			}
		}).start();
	}
	
	public void backCallLog(){
		
		if (headBackText.getVisibility() == View.VISIBLE) {
			select.setVisibility(View.INVISIBLE);
			headBackText.setVisibility(View.INVISIBLE);
			headText.setVisibility(View.VISIBLE);
			checkBoxVisible = false;
			calllog.notifyDataSetChanged();
		}
		
		
	}
}