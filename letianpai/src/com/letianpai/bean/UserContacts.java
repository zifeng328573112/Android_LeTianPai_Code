package com.letianpai.bean;

import java.io.Serializable;
import java.util.ArrayList;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;

/*
 *  联系人
 */
public class UserContacts implements Serializable{
    private String userId;
    private String name;// 名字
    private String oldname;
    private ArrayList<String> nickName;
    private ArrayList<String> number;// 号码集合
    private String pictureId;// 图片ID
    private Bitmap picture;
    private ArrayList<String> email;//邮箱
    private ArrayList<String> address;// 地址
    private ArrayList<String> remarks;// 备注
    private ArrayList<String> userIM;
    private Drawable head;
    private String place;
    
    private String sortLetters;  //显示数据拼音的首字母
    private String sort_number;  //位置排序
     
    
    
    public String getSort_number() {
		return sort_number;
	}

	public void setSort_number(String sort_number) {
		this.sort_number = sort_number;
	}

	private boolean haveHead=false; 
    private boolean haveNickName=false;
    private boolean haveEmail=false;
    private boolean haveAddress=false;
    private boolean haveRemarks=false;
    private boolean haveIm=false;
    
    

    

    public boolean isHaveHead() {
		return haveHead;
	}

	public void setHaveHead(boolean haveHead) {
		this.haveHead = haveHead;
	}

	public boolean isHaveNickName() {
		return haveNickName;
	}

	public void setHaveNickName(boolean haveNickName) {
		this.haveNickName = haveNickName;
	}

	public boolean isHaveEmail() {
		return haveEmail;
	}

	public void setHaveEmail(boolean haveEmail) {
		this.haveEmail = haveEmail;
	}

	public boolean isHaveAddress() {
		return haveAddress;
	}

	public void setHaveAddress(boolean haveAddress) {
		this.haveAddress = haveAddress;
	}

	public boolean isHaveRemarks() {
		return haveRemarks;
	}

	public void setHaveRemarks(boolean haveRemarks) {
		this.haveRemarks = haveRemarks;
	}

	public boolean isHaveIm() {
		return haveIm;
	}

	public void setHaveIm(boolean haveIm) {
		this.haveIm = haveIm;
	}

	public String getSortLetters() {
        return sortLetters;
    }

    public void setSortLetters(String sortLetters) {
        this.sortLetters = sortLetters;
    }

    public Drawable getHead() {
        return head;
    }

    public void setHead(Drawable head) {
        this.head = head;
    }



    public ArrayList<String> getNickName() {
        return nickName;
    }

    public void setNickName(ArrayList<String> nickName) {
        this.nickName = nickName;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public Bitmap getPicture() {
        return picture;
    }

    public void setPicture(Bitmap picture) {
        this.picture = picture;
    }

    public ArrayList<String> getRemarks() {
        return remarks;
    }

    public void setRemarks(ArrayList<String> remarks) {
        this.remarks = remarks;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<String> getNumber() {
        return number;
    }

    public void setNumber(ArrayList<String> number) {
        this.number = number;
    }

    public String getPictureId() {
        return pictureId;
    }

    public void setPictureId(String pictureId) {
        this.pictureId = pictureId;
    }




    public ArrayList<String> getAddress() {
        return address;
    }

    public void setAddress(ArrayList<String> address) {
        this.address = address;
    }




    public ArrayList<String> getEmail() {
        return email;
    }

    public void setEmail(ArrayList<String> email) {
        this.email = email;
    }

    public ArrayList<String> getUserIM() {
        return userIM;
    }

    public void setUserIM(ArrayList<String> userIM) {
        this.userIM = userIM;
    }

    public String getOldname() {
        return oldname;
    }

    public void setOldname(String oldname) {
        this.oldname = oldname;
    }
}
