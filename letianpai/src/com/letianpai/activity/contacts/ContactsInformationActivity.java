package com.letianpai.activity.contacts;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.letianpai.R;
import com.letianpai.adapter.contacts.ContactsInformationAdapter;
import com.letianpai.bean.UserContacts;
import com.letianpai.utils.ContactsUtils;
import com.umeng.analytics.MobclickAgent;

public class ContactsInformationActivity extends Activity {
	TextView name;
	TextView phoneNum;
	View call, headBackText;
	TextView headText;
	TextView headShare;
	String username;
	String number;
	String email;
	public static UserContacts uc;
	ImageView Sendsms;
	private ListView listView;
	public final int REQUEST_CODE_INFO = 102;
	private TextView sort_tv;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.contacts_information);
		initView();
		Intent intent = getIntent();
		Bundle bundle = intent.getExtras();
		uc = new UserContacts();
		username = bundle.getString("name");
		String id = bundle.getString("id");
		Bitmap head = bundle.getParcelable("head");
        int  position=bundle.getInt("position");
        position=position+1;
		sort_tv.setText(position+"");
		uc = ContactsUtils.getOneContacts(this, id);
		uc.setName(username);
		if (head != null) {
			uc.setPicture(head);
		}
		listView = (ListView) findViewById(R.id.listView);
		List<String> value = ContactsUtils.getUserInformation(uc);
		List<String> type = ContactsUtils.getContactsName(uc);
		ContactsInformationAdapter con = new ContactsInformationAdapter(this,
				value, type);

		listView.setAdapter(con);
		name.setText(uc.getName());
		number = uc.getNumber().get(0);
		String p_number=uc.getNumber().get(0);
		   if(p_number.contains("+86")){
			   p_number=p_number.substring(3, 14);
		    }
		if (p_number.length()==11) {
			p_number=p_number.substring(0, 3)+"-"+p_number.substring(3,7)+"-"+p_number.substring(7,11);
		}
		phoneNum.setText(p_number);

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		MobclickAgent.onPageStart("SplashScreen"); // 统计页面
		MobclickAgent.onResume(this); // 统计时长
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		MobclickAgent.onPageEnd("SplashScreen"); // 保证 onPageEnd 在onPause
		// 之前调用,因为 onPause 中会保存信息
		MobclickAgent.onPause(this);
	}

	private void initView() {
		// TODO Auto-generated method stub
		name = (TextView) findViewById(R.id.name);
		headText = (TextView) findViewById(R.id.headText);
		Sendsms = (ImageView) findViewById(R.id.sendSms);
		headText.setText("联系人");
		phoneNum = (TextView) findViewById(R.id.phoneNum);
		headShare = (TextView) findViewById(R.id.headShare);
		headShare.setText("编辑");
		headShare.setOnClickListener(new InformationListener());
		headBackText = findViewById(R.id.headBackText);
		sort_tv = (TextView) findViewById(R.id.sort_tv);
		call = findViewById(R.id.call);
		call.setOnClickListener(new InformationListener());
		headBackText.setOnClickListener(new InformationListener());
		Sendsms.setOnClickListener(new InformationListener());
		phoneNum.setOnClickListener(new InformationListener());
	}

	class InformationListener implements View.OnClickListener {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			switch (v.getId()) {
			case R.id.call:
				Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"
						+ number));
				ContactsInformationActivity.this.startActivity(intent);

				break;
			case R.id.phoneNum:
				Intent intent_number_one = new Intent(Intent.ACTION_CALL,
						Uri.parse("tel:" + phoneNum.getText().toString()));
				ContactsInformationActivity.this
						.startActivity(intent_number_one);
				break;
			case R.id.headBackText:
				finish();
				break;
			case R.id.headShare:
				Intent intent1 = new Intent(ContactsInformationActivity.this,
						EditContactsActivity.class);
				Bundle bundle = new Bundle();
				bundle.putString("id", uc.getUserId());
				bundle.putString("name", uc.getName());
				if (uc.getPicture() != null) {
					bundle.putParcelable("head", uc.getPicture());
				}
				bundle.putString("phoneNumber", uc.getNumber().get(0));
				intent1.putExtras(bundle);
				ContactsInformationActivity.this.startActivityForResult(
						intent1, REQUEST_CODE_INFO);
				finish();
				break;
			case R.id.sendSms:
				Uri smsToUri = Uri.parse("smsto:" + number);// 联系人地址
				Intent mIntent = new Intent(
						android.content.Intent.ACTION_SENDTO, smsToUri);
				startActivity(mIntent);
				break;
			default:
				break;
			}
		}

	}

	// @Override
	// protected void onActivityResult(int requestCode, int resultCode, Intent
	// data) {
	// // TODO Auto-generated method stub
	// super.onActivityResult(requestCode, resultCode, data);
	// if(resultCode==144){
	// Toast.makeText(
	// this, "info", 1000).show();
	// }
	// }

}
