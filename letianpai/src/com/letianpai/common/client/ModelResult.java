package com.letianpai.common.client;

import android.content.Context;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Administrator on 2014/9/29.
 */
public class ModelResult<T> implements Serializable {
    private boolean returnResult;//返回结果
    private String message;//返回提示信息
    private String content;//返回内容
    private int code = 0;//服务器返回的结果类型
    //200 请求成功  400 请求错误  401 为授权 403 禁止访问  404 文件未找到  500  服务器错误
    private String requestClass;// 请求的类，用于log日志
    private T persisObj;
    private List<T> persisObjList;

    public boolean isReturnResult() {
        return returnResult;
    }

    public void setReturnResult(boolean returnResult) {
        this.returnResult = returnResult;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getRequestClass() {
        return requestClass;
    }

    public void setRequestClass(String requestClass) {
        this.requestClass = requestClass;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public T getPersisObj() {
        return persisObj;
    }

    public void setPersisObj(T persisObj) {
        this.persisObj = persisObj;
    }

    public List<T> getPersisObjList() {
        return persisObjList;
    }

    public void setPersisObjList(List<T> persisObjList) {
        this.persisObjList = persisObjList;
    }
}

