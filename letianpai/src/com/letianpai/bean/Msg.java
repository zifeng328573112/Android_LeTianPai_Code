package com.letianpai.bean;

import java.io.Serializable;

import org.json.JSONException;
import org.json.JSONObject;

import android.graphics.Bitmap;
import android.util.Log;

public class Msg implements Serializable{
    String userid;
    String msg;
    String date;
    String from;
    Bitmap chat_pc;
    String time;// 语音时长
    String filePath;
    String receive;// 接收
    String type  =TYPE[2];//类型 normal 普通消息
    int playstatu;


    public static final String[] STATUS = { "success", "refused", "fail",
            "wait" };
    public static final String[] TYPE = { "record", "photo", "normal" };
    public static final String[] FROM_TYPE = { "IN", "OUT" };

    public Msg(String userid, String msg, String date, String from,
               Bitmap chat_pc) {
        this.userid = userid;
        this.msg = msg;
        this.date = date;
        this.from = from;
        this.chat_pc = chat_pc;
    }
    public Msg(){};
    public Msg(String userid, String msg, String date, String from,
               String type, String receive, String time, String filePath) {
        super();
        this.userid = userid;
        this.msg = msg;
        this.date = date;
        this.from = from;
        this.type = type;
        this.receive = receive;
        this.time = time;
        this.filePath = filePath;
    }
    public Msg(String userid, String msg, String date, String from,
               String type, String receive, String time, String filePath,Bitmap chat_pc) {
        super();
        this.userid = userid;
        this.msg = msg;
        this.date = date;
        this.from = from;
        this.type = type;
        this.receive = receive;
        this.time = time;
        this.filePath = filePath;
        this.chat_pc = chat_pc;
    }
    public Msg(String userid, String msg, String date, String from,
               String type, String receive) {
        super();
        this.userid = userid;
        this.msg = msg;
        this.date = date;
        this.from = from;
        this.type = type;
        this.receive = receive;
    }
    public String getTime() {
        return time;
    }
    public void setTime(String time) {
        this.time = time;
    }
    public String getFilePath() {
        return filePath;
    }
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
    public String getReceive() {
        return receive;
    }
    public void setReceive(String receive) {
        this.receive = receive;
    }
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public Bitmap getChat_pc() {
        return chat_pc;
    }

    public void setChat_pc(Bitmap chat_pc) {
        this.chat_pc = chat_pc;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }


    public int getPlaystatu() {
		return playstatu;
	}
	public void setPlaystatu(int playstatu) {
		this.playstatu = playstatu;
	}


	public static final String USERID ="userid";
    public static final String MSG_CONTENT ="msg";//消息内容
    public static final String DATE ="date";
    public static final String FROM ="from";
    public static final String MSG_TYPE ="type";
    public static final String RECEIVE_STAUTS="receive";// 接收状态
    public static final String TIME_REDIO="time";
    public static final String FIL_PAHT="filePath";
    /**
     * 传json
     */
    public static  String  toJson(Msg msg){
        JSONObject jsonObject=new JSONObject();
        String jsonStr="";
        try {
            jsonObject.put(Msg.USERID, msg.getUserid()+"");
            jsonObject.put(Msg.MSG_CONTENT, msg.getMsg()+"");
            jsonObject.put(Msg.DATE, msg.getDate()+"");
            jsonObject.put(Msg.FROM, msg.getFrom()+"");
            jsonObject.put(Msg.MSG_TYPE, msg.getType()+"");
            jsonObject.put(Msg.RECEIVE_STAUTS, msg.getReceive()+"");
            jsonObject.put(Msg.TIME_REDIO, msg.getTime());
            jsonObject.put(Msg.FIL_PAHT, msg.getFilePath());
            jsonStr= jsonObject.toString();
            Log.d("msg json", jsonStr+"");

        } catch (JSONException e) {
            e.printStackTrace();
        }finally{
            return jsonStr;
        }
    }
}
