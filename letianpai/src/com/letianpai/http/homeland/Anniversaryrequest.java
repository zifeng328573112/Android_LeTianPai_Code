package com.letianpai.http.homeland;

import com.letianpai.bean.homeland.AnniversaryBean;

public class Anniversaryrequest {
	private String head;
	private AnniversaryBean content;
	public String getHead() {
		return head;
	}
	public void setHead(String head) {
		this.head = head;
	}
	public AnniversaryBean getContent() {
		return content;
	}
	public void setContent(AnniversaryBean content) {
		this.content = content;
	}
	
}
