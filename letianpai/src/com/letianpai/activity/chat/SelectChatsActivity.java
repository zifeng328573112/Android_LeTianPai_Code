package com.letianpai.activity.chat;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.letianpai.R;
import com.letianpai.activity.LoginActivity;
import com.letianpai.activity.family.FamilyListActivity;
import com.letianpai.adapter.family.FamilyListAdapter;
import com.letianpai.bean.FriendInfo;

public class SelectChatsActivity extends Activity {

    ListView listview;
    FamilyListAdapter adapter;
    ArrayList<FriendInfo> arraylist = new ArrayList<FriendInfo>();
    EditText edit;
    String information;
    String USERID = LoginActivity.USERID;
    TextView headShare;
    View headBackText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.selectchat_layout);
        headShare = (TextView) findViewById(R.id.headShare);
        headShare.setText("");
        headBackText = findViewById(R.id.headBackText);
        headBackText.setOnClickListener(new FamilyClick());
        listview = (ListView) findViewById(R.id.listView);
        edit = (EditText) findViewById(R.id.edit);
        // arraylist=ContactsUtils.getContacts(this);
        // adapter=new FamilyListAdapter(this, arraylist);
        // listview.setAdapter(adapter);

        edit.addTextChangedListener(new TextWatcher() {

            private ArrayList<FriendInfo> arraylist2;

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                information = edit.getText().toString();
                Log.i("info", information + "000");
                for (int i = 0; i < FamilyListActivity.arrayList.size(); i++) {
                    String name = String.valueOf(FamilyListActivity.arrayList
                            .get(i).getNickname().charAt(0));
                    Log.i("info", name + "111");
                    if (name.equals(information)) {
                        listview.setVisibility(View.VISIBLE);
                        Log.i("info", information + name);
                        arraylist.add(FamilyListActivity.arrayList.get(i));

                    }
                }

                if (information.equals("")) {
                    listview.setVisibility(View.GONE);
                    arraylist.clear();
                }
                Log.i("info", arraylist.size() + "changdu");
                adapter = new FamilyListAdapter(SelectChatsActivity.this,
                        arraylist, new FamilyClick());
                listview.setAdapter(adapter);
            }
        });
        listview.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                /*
                 * Intent intent_image = new Intent(SelectChatsActivity.this,
				 * ChatActivity.class); intent_image.putExtra("USERID", USERID);
				 * intent_image.putExtra("name", arraylist.get((Integer)
				 * arg1.getTag()).getJid()); intent_image.putExtra("nick",
				 * arraylist.get((Integer) arg1.getTag()).getNickname());
				 *
				 * intent_image.putExtra("chatUserId", arraylist.get((Integer)
				 * arg1.getTag()).getJid()); startActivity(intent_image);
				 */
            }
        });
    }

    class FamilyClick implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                // 点击电话短信聊天
                case R.id.bottomLeft2:
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"
                            + arraylist.get((Integer) v.getTag()).getJid()));
                    SelectChatsActivity.this.startActivity(intent);
                    break;
                case R.id.sms_image:
                    Uri smsToUri = Uri.parse("smsto:"
                            + arraylist.get((Integer) v.getTag()).getJid());// 联系人地址
                    Intent mIntent = new Intent(
                            android.content.Intent.ACTION_SENDTO, smsToUri);
                    startActivity(mIntent);
                    break;
                case R.id.chat_image:
                    Intent intent_image = new Intent(SelectChatsActivity.this,
                            ChatActivity.class);
                    intent_image.putExtra("USERID", USERID);
                    intent_image.putExtra("name",
                            arraylist.get((Integer) v.getTag()).getJid());
                    intent_image.putExtra("nick",
                            arraylist.get((Integer) v.getTag()).getNickname());

                    intent_image.putExtra("chatUserId",
                            arraylist.get((Integer) v.getTag()).getJid());
                    startActivity(intent_image);
                    break;
                case R.id.headBackText:
                    finish();
                    break;
                default:
                    break;
            }
        }
    }
}
