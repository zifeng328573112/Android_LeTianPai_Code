package com.letianpai.adapter.chat;

import java.io.File;
import java.util.ArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.AnimationDrawable;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.letianpai.R;
import com.letianpai.bean.Msg;
import com.letianpai.utils.ChatUtils;

public class ChatInformationAdapter extends BaseAdapter {
	private LayoutInflater inflater;
	private ArrayList<Msg> arraylist;
	private Context context;
	public int id;
	Bitmap chat_pc;
	Bitmap mb, ub;
	long playtime;
	private AnimationDrawable animationDrawable_left;
	private AnimationDrawable animationDrawable_right;
	public static String RECORD_ROOT_PATH = Environment
			.getExternalStorageDirectory().getPath() + "/chat/record/";

	public void setBitmap(Bitmap mb, Bitmap ub) {
		this.mb = mb;
		this.ub = ub;
	}

	public void setArraylist(ArrayList<Msg> arraylist) {
		this.arraylist = arraylist;
	}

	public ChatInformationAdapter(Context context, ArrayList<Msg> arraylist,
			Bitmap mb, Bitmap ub) {
		super();
		this.context = context;
		inflater = LayoutInflater.from(context);
		this.arraylist = arraylist;
		this.mb = mb;
		this.ub = ub;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return arraylist.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return arraylist.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {

		if (arraylist.get(position).getFrom().equals("OUT")) {
			view = inflater.inflate(R.layout.chat_text_rigth, parent, false);
			ImageView userImage = (ImageView) view.findViewById(R.id.userImage);
			ProgressBar pb = (ProgressBar) view
					.findViewById(R.id.progress_chat);
			if (!(mb == null)) {
				userImage.setImageBitmap(mb);
			}
			TextView time1 = (TextView) view.findViewById(R.id.time);

			if (arraylist.get(position).getType().equals("record")) {
				Log.d("zzzz", arraylist.get(position).getReceive());
				if (!arraylist.get(position).getReceive().equals("success")) {
					pb.setVisibility(View.VISIBLE);
				} else {
					pb.setVisibility(View.GONE);
				}
				final ImageView chat_right = (ImageView) view
						.findViewById(R.id.chat_pc);
				chat_right.setVisibility(View.VISIBLE);

				TextView chat_time = (TextView) view
						.findViewById(R.id.chat_time);
				// 判断是否
				if (arraylist.get(position).getPlaystatu() == 1) {
					playtime = Integer.parseInt(arraylist.get(position)
							.getTime());
					animationDrawable_right = (AnimationDrawable) chat_right
							.getDrawable();
					animationDrawable_right.start();
					id = position;
					new Thread(new Runnable() {

						@Override
						public void run() {

							try {
								
								Thread.sleep(playtime*1000);
								
							} catch (NumberFormatException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							Log.d("zzzz", 2+"尼玛");
							animationDrawable_right.stop();
							arraylist.get(id).setPlaystatu(0);
						}
					}).start();
				}

				chat_time.setVisibility(View.VISIBLE);
				chat_time.setText(arraylist.get(position).getTime() + "''");
				time1.setText(arraylist.get(position).getDate());
			}
			if (arraylist.get(position).getType().equals("photo")) {
				if (!arraylist.get(position).getReceive().equals("success")) {
					pb.setVisibility(View.VISIBLE);
				} else {
					pb.setVisibility(View.GONE);
				}
				ImageView chat_right = (ImageView) view
						.findViewById(R.id.chat_pc);
				chat_right.setVisibility(View.VISIBLE);
				Bitmap bitmap = ChatUtils.decodeBitmap(arraylist.get(position)
						.getFilePath(), 200);
				chat_right.setImageBitmap(bitmap);
				time1.setText(arraylist.get(position).getDate());
			}
			if (arraylist.get(position).getType().equals("normal")) {
				TextView information1 = (TextView) view
						.findViewById(R.id.chat_textview);
				information1.setText(arraylist.get(position).getMsg());
				time1.setText(arraylist.get(position).getDate());
			}
		} else {
			view = inflater.inflate(R.layout.chat_text, parent, false);
			ProgressBar pb = (ProgressBar) view
					.findViewById(R.id.progress_chat);

			ImageView userImage = (ImageView) view.findViewById(R.id.userImage);
			if (!(null == ub)) {

				userImage.setImageBitmap(ub);
			}
			TextView time = (TextView) view.findViewById(R.id.time);

			if (arraylist.get(position).getType().equals("record")) {
				Log.d("zzzz", arraylist.get(position).getReceive() + "状态");
				if (arraylist.get(position).getReceive().equals("success")
						|| arraylist.get(position).getReceive()
								.equals("refused")) {
					pb.setVisibility(View.GONE);

				} else {
					pb.setVisibility(View.VISIBLE);
				}
				ImageView chat_pc1 = (ImageView) view
						.findViewById(R.id.chat_pc1);
				chat_pc1.setVisibility(View.VISIBLE);
				TextView chat_time1 = (TextView) view
						.findViewById(R.id.chat_time1);
				chat_time1.setVisibility(View.VISIBLE);
				chat_time1.setText(arraylist.get(position).getTime() + "''");
				time.setText(arraylist.get(position).getDate());
			}
			if (arraylist.get(position).getType().equals("photo")) {
				if (arraylist.get(position).getReceive().equals("success")
						|| arraylist.get(position).getReceive()
								.equals("refused")) {
					pb.setVisibility(View.GONE);

				} else {
					pb.setVisibility(View.VISIBLE);
				}
				ImageView chat_pc1 = (ImageView) view
						.findViewById(R.id.chat_pc1);
				chat_pc1.setVisibility(View.VISIBLE);

				Bitmap bitmap = ChatUtils.decodeBitmap(RECORD_ROOT_PATH
						+ arraylist.get(position).getFilePath(), 200);
				File file = new File(RECORD_ROOT_PATH
						+ arraylist.get(position).getFilePath());
				Log.d("zzzz", file.exists() + "文件是否存在");
				chat_pc1.setImageBitmap(bitmap);
				time.setText(arraylist.get(position).getDate());

			}
			if (arraylist.get(position).getType().equals("normal")) {
				TextView information = (TextView) view
						.findViewById(R.id.chat_textview);
				information.setText(arraylist.get(position).getMsg());
				time.setText(arraylist.get(position).getDate());
			}
		}
		return view;
	}
}
