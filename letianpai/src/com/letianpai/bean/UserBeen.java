package com.letianpai.bean;

public class UserBeen {
	private String head;
	private UserInfo content;
	
	
	public UserBeen() {
	}
	public UserBeen(UserInfo content,String head ) {
		this.head = head;
		this.content = content;
	}
	public String getHead() {
		return head;
	}
	public void setHead(String head) {
		this.head = head;
	}
	public UserInfo getContent() {
		return content;
	}
	public void setContent(UserInfo content) {
		this.content = content;
	}
	
	
}
