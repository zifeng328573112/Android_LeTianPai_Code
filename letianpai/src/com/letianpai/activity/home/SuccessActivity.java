package com.letianpai.activity.home;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.TextView;

import com.letianpai.R;
import com.letianpai.activity.TabGroupActivity;
import com.letianpai.application.ManageApplication;
import com.letianpai.utils.XmppTool;

public class SuccessActivity extends Activity {
    TextView regs,back_home,headShare;
    View headBackText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        ManageApplication.getInstance().addActivity(this);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.home_success);
        headBackText = findViewById(R.id.headBackText);
        headBackText.setVisibility(View.INVISIBLE);
        regs=(TextView) findViewById(R.id.regs);
        back_home=(TextView) findViewById(R.id.back_home);
        regs.setText("注册成功");
        back_home.setText("返回我家");
        headShare = (TextView) findViewById(R.id.headShare);
        headShare.setText("");
        back_home.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                XmppTool.closeConnection();
                Intent intent = new Intent(SuccessActivity.this,
                        HomeMainActivity.class);
                Window w = TabGroupActivity.group_activity
                        .getLocalActivityManager().startActivity("family",
                                intent);
                View v = w.getDecorView();
                TabGroupActivity.group_activity.setContentView(v);
                ManageApplication.getInstance().unLogin();
            }
        });
    }
}
