package com.letianpai.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.letianpai.R;
import com.letianpai.activity.calllog.CallRecordActivity;
import com.letianpai.activity.calllog.CallRecordEditActivity;
import com.letianpai.bean.UserCallLog;

/**
 * 通话记录适配器
 * 
 * @author luo
 * 
 */
public class CallLogAdapter extends BaseAdapter {
	private Context context;
	private LayoutInflater inflater;
	public UserCallLog ucl;
	List<UserCallLog> arraylist = new ArrayList<UserCallLog>();
	View.OnClickListener cliker;

	public void setArraylist(List<UserCallLog> arraylist) {
		this.arraylist = arraylist;
	}

	public CallLogAdapter(Context context, List<UserCallLog> arraylist,
			View.OnClickListener clicker) {
		super();
		this.context = context;
		inflater = LayoutInflater.from(context);
		this.arraylist = arraylist;
		this.cliker = clicker;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return arraylist.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return arraylist.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		ViewHolder viewHolder = null;
		if (view == null) {
			viewHolder = new ViewHolder();
			view = inflater.inflate(R.layout.call_log_content, parent, false);
			viewHolder.checkBox = (CheckBox) view.findViewById(R.id.checkBox);

			viewHolder.userImg = (ImageView) view.findViewById(R.id.userImage);
			viewHolder.userName = (TextView) view.findViewById(R.id.topLeft);
			viewHolder.userNumber = (TextView) view
					.findViewById(R.id.bottomLeft2);
			viewHolder.numberImg = (ImageView) view.findViewById(R.id.callIcon);
			viewHolder.bottomLeft1 = (TextView) view
					.findViewById(R.id.bottomLeft1);
			viewHolder.time = (TextView) view.findViewById(R.id.time);
			viewHolder.duration = (TextView) view.findViewById(R.id.duration);
			viewHolder.type = (ImageView) view.findViewById(R.id.callIcon);
			view.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) view.getTag();
		}
		viewHolder.checkBox.setTag(position);
		viewHolder.checkBox.setOnClickListener(cliker);
		// 设置图片
		if (null == arraylist.get(position).getPhoto()) {
			viewHolder.userImg.setImageResource(R.drawable.contact_default_head);;
		} else if (null != arraylist.get(position).getPhoto()) {
			viewHolder.userImg.setImageBitmap(arraylist.get(position)
					.getPhoto());
		}
		viewHolder.time.setText(arraylist.get(position).getConverseTime());
		viewHolder.bottomLeft1.setVisibility(View.INVISIBLE);// 隐藏
		viewHolder.userName.setText(arraylist.get(position).getUserName());
		viewHolder.userNumber.setText(arraylist.get(position).getPhoneNum());
		viewHolder.duration.setText(arraylist.get(position).getDuration() + "");
		// 显示checkbox
		if (CallRecordActivity.checkBoxVisible) {
			viewHolder.checkBox.setVisibility(CheckBox.VISIBLE);
			viewHolder.duration.setVisibility(View.INVISIBLE);
			viewHolder.time.setVisibility(View.INVISIBLE);
		}
		// 显示时间、时长
		if (CallRecordActivity.checkBoxVisible == false) {
			viewHolder.checkBox.setVisibility(CheckBox.INVISIBLE);
			viewHolder.duration.setVisibility(view.VISIBLE);
			viewHolder.time.setVisibility(View.VISIBLE);
		}
		// 设置Checkbox为选中、不选中添加到集合
		if (arraylist.get(position).isChecked() == true) {
			viewHolder.checkBox.setChecked(true);
		} else if (arraylist.get(position).isChecked() == false) {
			viewHolder.checkBox.setChecked(false);

		}

		// 通话记录类型
		int type = arraylist.get(position).getCallType();
		switch (type) {
		case 1:
			viewHolder.type.setImageResource(R.drawable.call_go_icon);
			break;
		case 2:
			viewHolder.type.setImageResource(R.drawable.call_out_icon);
			break;
		case 3:
			viewHolder.type.setImageResource(R.drawable.call_hang_icon);
			break;
		default:
			break;
		}
		return view;
	}

	final static class ViewHolder {
		CheckBox checkBox;
		ImageView userImg;
		TextView userName;
		TextView userNumber;
		ImageView numberImg;
		TextView bottomLeft1;
		TextView time;
		TextView duration;
		ImageView type;
	}

}
