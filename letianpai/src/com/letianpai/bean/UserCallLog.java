package com.letianpai.bean;

import android.graphics.Bitmap;

/*
 * 通话记录
 */
public class UserCallLog {

    private int userId;
    private String userName;// 用户姓名
    private String phoneNum;// 电话
    private int callType;// 通话类型
    private String duration;// 通话时长
    private String converseTime;// 通话时间
    private Bitmap photo;// 照片
    private String photoId;// 照片ID
    private boolean isChecked = false;// 是否选中

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public Bitmap getPhoto() {
        return photo;
    }

    public void setPhoto(Bitmap photo) {
        this.photo = photo;
    }

    public String getPhotoId() {
        return photoId;
    }

    public void setPhotoId(String photoId) {
        this.photoId = photoId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPhoneNum() {
        return phoneNum;
    }

    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }

    public int getCallType() {
        return callType;
    }

    public void setCallType(int callType) {
        this.callType = callType;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getConverseTime() {
        return converseTime;
    }

    public void setConverseTime(String converseTime) {
        this.converseTime = converseTime;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean isChecked) {
        this.isChecked = isChecked;
    }

}
