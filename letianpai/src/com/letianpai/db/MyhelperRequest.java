package com.letianpai.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class MyhelperRequest extends SQLiteOpenHelper {
	String table_name;

	public MyhelperRequest(Context context, String name, String table_name,CursorFactory factory,
			int version) {
		super(context, name, factory, version);
	    this.table_name=table_name;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL("create table if not exists " + "d_"+table_name
				+ " (_id integer primary key autoincrement , uname varchar(100), utime varchar(100))");
	}

	@Override
	public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {

	}

}
