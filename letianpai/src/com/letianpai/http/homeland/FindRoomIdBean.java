package com.letianpai.http.homeland;

public class FindRoomIdBean {
	private String action;
	private String roomid;
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getRoomid() {
		return roomid;
	}
	public void setRoomid(String roomid) {
		this.roomid = roomid;
	}
	
}
