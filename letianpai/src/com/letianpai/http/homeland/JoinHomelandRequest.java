package com.letianpai.http.homeland;

public class JoinHomelandRequest {
	private String head;
	private JoinHoemlandBean content;

	public String getHead() {
		return head;
	}

	public void setHead(String head) {
		this.head = head;
	}

	public JoinHoemlandBean getContent() {
		return content;
	}

	public void setContent(JoinHoemlandBean content) {
		this.content = content;
	}

}
