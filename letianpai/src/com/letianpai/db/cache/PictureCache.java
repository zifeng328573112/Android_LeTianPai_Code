package com.letianpai.db.cache;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Administrator on 2014/10/10.
 */
public class PictureCache {
    private static final String PICTURE_DELETE_CACHE = "picture_delete_letianlun";

    public static void modifyPicture(Context context, String picture_id) {
        SharedPreferences sharePre = context.getSharedPreferences(PICTURE_DELETE_CACHE, 0);
        Set<String> sets = getPicture(context);
        sets.add(picture_id);
        SharedPreferences.Editor editor = sharePre.edit();
        editor.putStringSet("picture_id_set", sets);
        editor.commit();
    }

    public static void movePicture(Context context, String picture_id) {
        SharedPreferences sharePre = context.getSharedPreferences(PICTURE_DELETE_CACHE, 0);
        Set<String> sets = getPicture(context);
        sets.remove(picture_id);
        SharedPreferences.Editor editor = sharePre.edit();
        editor.putStringSet("picture_id_set", sets);
        editor.commit();
    }

    public static void movePictureAll(Context context) {
        SharedPreferences sharePre = context.getSharedPreferences(PICTURE_DELETE_CACHE, 0);
        Set<String> sets = new HashSet<String>();
        SharedPreferences.Editor editor = sharePre.edit();
        editor.putStringSet("picture_id_set", sets);
        editor.commit();
    }

    public static Set<String> getPicture(Context context) {
        SharedPreferences sharePre = context.getSharedPreferences(PICTURE_DELETE_CACHE, 0);
        Set<String> sets = new HashSet<String>();
        return sharePre.getStringSet("picture_id_set", sets);
    }
}
