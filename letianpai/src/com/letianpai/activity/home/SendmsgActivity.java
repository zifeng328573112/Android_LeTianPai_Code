package com.letianpai.activity.home;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.letianpai.R;
import com.letianpai.application.ManageApplication;
import com.letianpai.bean.BackResponse;
import com.letianpai.bean.UserBeen;
import com.letianpai.db.Constant;
import com.letianpai.dialog.HomegetDialog;
import com.letianpai.http.RequestBean.IdentifyUserBean;
import com.letianpai.http.RequestBean.Identifying;
import com.letianpai.utils.JsonUtil;

public class SendmsgActivity extends Activity implements OnClickListener {
    View ll_down, nextstep;
    EditText number;
    String phonenumber;
    TextView title, phone, time, reget, back,headShare;
    RequestQueue requestQueue;
    int s = 60;
    boolean isclick = true;
    Timer timer = new Timer();
    ProgressDialog pd;
    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            super.handleMessage(msg);

            switch (msg.what) {

                case 1:
                    s--;
                    time.setText(s + "");
                    if (s == 0) {

                        time.setText(s + "");
                        stopTimer();
                        isclick = true;
                    } else {
                        isclick = false;
                    }
                    break;
                default:
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        ManageApplication.getInstance().addActivity(this);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.home_reg);

        init();

        number.setHint("请填写您手机接收到的注册码");
        ll_down.setVisibility(View.GONE);
        phone.setVisibility(View.VISIBLE);
        Intent intent = getIntent();
        //注册的号码
        phonenumber = intent.getStringExtra("number");
        title.setText("注册码已经收到：");
        title.setGravity(Gravity.CENTER);
        phone.setText(phonenumber);

        reget.setOnClickListener(this);
        back.setOnClickListener(this);
        nextstep.setOnClickListener(this);
        startTimer();
    }

    private void init() {
        // TODO Auto-generated method stub
        requestQueue = Volley.newRequestQueue(this);
        headShare = (TextView) findViewById(R.id.headShare);
        headShare.setText("");
        ll_down = findViewById(R.id.ll_down);
        title = (TextView) findViewById(R.id.title);
        phone = (TextView) findViewById(R.id.phone);
        number = (EditText) findViewById(R.id.number);
        time = (TextView) findViewById(R.id.time);
        reget = (TextView) findViewById(R.id.reget);
        back = (TextView) findViewById(R.id.back);
        nextstep = findViewById(R.id.nextstep);

    }

    protected void dialog() {
        final HomegetDialog dialog = new HomegetDialog(this);
        dialog.setLeftOnClick(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    @Override
    public void onClick(View arg0) {
        switch (arg0.getId()) {
            case R.id.reget:
                // 点击之后 开启定时器
                if (isclick) {
                    startTimer();
                    // 弹出对话框
                    dialog();
                    // 设置时间
                    s = 60;
                }
                break;
            case R.id.back:
                finish();
                break;
            case R.id.nextstep:
                final String verifycode = number.getText().toString();
                if (verifycode.equals("")) {
                    Toast.makeText(SendmsgActivity.this, "请输入验证码",
                            Toast.LENGTH_SHORT).show();
                }
                if (verifycode.length() != 6) {
                    Toast.makeText(SendmsgActivity.this, "请输入6为手机验证码",
                            Toast.LENGTH_SHORT).show();
                }
                if (verifycode.length() == 6) {
                	 pd = new ProgressDialog(SendmsgActivity.this);
                     pd.setTitle("请稍等");
                     pd.show();
                     pd.setCanceledOnTouchOutside(false);
                    new Thread(new Runnable() {

                        @Override
                        public void run() {
                            post(phonenumber, verifycode);
                        }
                    }).start();
                }

                break;
            default:
                break;
        }
    }

    public void startTimer() {

        timer = new Timer();
        timer.schedule(new TimerTask() {

            @Override
            public void run() {
                handler.sendEmptyMessage(1);
            }
        }, 0, 1000);
    }

    public void stopTimer() {
        timer.cancel();
    }

    // 调用接口
    public void post(String username, String verifycode) {

        Map<String, String> map = new HashMap<String, String>();
        Identifying userInfo = new Identifying();
        userInfo.setUsername(username);
        userInfo.setAction("step2");
        userInfo.setVerifycode(verifycode);
        IdentifyUserBean user = new IdentifyUserBean();
        user.setHead("registerServlet");
        user.setIdentifying(userInfo);
        // msg值
        String msg = JsonUtil.objectToString(user, IdentifyUserBean.class);
        String url = Constant.URL + "?msg=" + msg;
        Log.d("zzzz", url);
        map.put("msg", msg);
        StringRequest request = new StringRequest(url, new ls(), new el());
        requestQueue.add(request);
    }

    class ls implements Listener<String> {

        @Override
        public void onResponse(String arg0) {
        	pd.cancel();
            BackResponse br = (BackResponse) JsonUtil.StringToObject(arg0,
                    BackResponse.class);
            Toast.makeText(SendmsgActivity.this, br.getMessage(),
                    Toast.LENGTH_SHORT).show();
            Log.d("zzzz", br.getData() + br.getMessage() + br.getStatus());
            if (br.getStatus().equals("SUCCESS")) {
                Intent intent1 = new Intent(SendmsgActivity.this,
                        PwdSettingActivity.class);
                //继续传递
                intent1.putExtra("username", phonenumber);
                startActivity(intent1);
            }

        }

    }

    class el implements ErrorListener {

        @Override
        public void onErrorResponse(VolleyError arg0) {
        	pd.cancel();
            Toast.makeText(SendmsgActivity.this, "请求失败", Toast.LENGTH_SHORT)
                    .show();
        }

    }

}
