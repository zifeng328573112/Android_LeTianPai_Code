package com.letianpai.activity.contacts;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import com.letianpai.R;
import com.letianpai.adapter.ListDialogAdapter;
import com.letianpai.bean.AddType;

import java.util.ArrayList;

/**
 * Created by Administrator on 2014/10/9.
 * 添加联系 人属性
 */
public class ContactAddPropertyDialog extends Dialog {
    Context context;
    ArrayList<String> listItem = new ArrayList<String>();
    ArrayList<String> userlist = new ArrayList<String>();
    TextView dialog = (TextView) findViewById(R.id.dialog);
    public static String somemore;
    private ListDialogAdapter ld;
    private ListView listview;
    SharedPreferences sp;

    public ContactAddPropertyDialog(Context context) {
        super(context);
        this.context = context;
        // TODO Auto-generated constructor stub
    }

    public ContactAddPropertyDialog(Context context, int theme) {
        super(context, theme);
        this.context = context;
        sp = context.getSharedPreferences("sp", context.MODE_PRIVATE);
        // TODO Auto-generated constructor stub
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.list_dialog);
        TextView title = (TextView) this.findViewById(R.id.title);
        // listItem.add( "电话" );
        initAdd();
        if (AddContactsActivity.info.size() >= 0) {
            for (int i = 0; i < AddContactsActivity.info.size(); i++) {
                AddType type = AddContactsActivity.info.get(i);
                listItem.remove(type.getType());
            }
        }
        if (listItem.size() <= 0) {
            title.setText("不能再添加更多了");
        }

        String have = sp.getString("STRING_KEY", "");
        if (have == null) {
            userlist = listItem;
        } else {
            for (int i = 0; i < listItem.size(); i++) {
                if (have != listItem.get(i)) {
                    userlist.add(listItem.get(i));
                }
            }

        }

        ld = new ListDialogAdapter(context, userlist);
        listview = (ListView) findViewById(R.id.listView);
        listview.setAdapter(ld);
        listview.setOnItemClickListener(new DialogListener());

    }

    private void initAdd() {
        listItem.add("昵称");
        listItem.add("邮箱");
        listItem.add("地址");
        listItem.add("备注");
        listItem.add("IM");
    }

    class DialogListener implements OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {

            AddContactsActivity.handler.sendEmptyMessage(3);
            somemore = userlist.get(position);

            Editor editor = sp.edit();
            // editor.putString("STRING_KEY",userlist.get(position));
            editor.commit();
            Log.i("info", position + "");
            cancel();
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        this.dismiss();
        return super.onTouchEvent(event);
    }
}
