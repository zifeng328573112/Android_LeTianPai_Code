package com.letianpai.db.cache;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Administrator on 2014/10/10.
 */
public class PhotoAlbumCache {
    private static final String PHOTO_ALBUM_DELETE_CACHE = "photo_album_delete_letianlun";

    public static void modifyPhotoAlbum(Context context, String album_id) {
        SharedPreferences sharePre = context.getSharedPreferences(PHOTO_ALBUM_DELETE_CACHE, 0);
        Set<String> sets = getPhotoAlbum(context);
        sets.add(album_id);
        SharedPreferences.Editor editor = sharePre.edit();
        editor.putStringSet("photo_album__id_set", sets);
        editor.commit();
    }

    public static void movePhotoAlbum(Context context, String album_id) {
        SharedPreferences sharePre = context.getSharedPreferences(PHOTO_ALBUM_DELETE_CACHE, 0);
        Set<String> sets = getPhotoAlbum(context);
        sets.remove(album_id);
        SharedPreferences.Editor editor = sharePre.edit();
        editor.putStringSet("photo_album__id_set", sets);
        editor.commit();
    }

    public static void movePhotoAlbumAll(Context context) {
        SharedPreferences sharePre = context.getSharedPreferences(PHOTO_ALBUM_DELETE_CACHE, 0);
        Set<String> sets = new HashSet<String>();
        SharedPreferences.Editor editor = sharePre.edit();
        editor.putStringSet("photo_album__id_set", sets);
        editor.commit();
    }

    public static Set<String> getPhotoAlbum(Context context) {
        SharedPreferences sharePre = context.getSharedPreferences(PHOTO_ALBUM_DELETE_CACHE, 0);
        Set<String> sets = new HashSet<String>();
        return sharePre.getStringSet("photo_album__id_set", sets);
    }
}

