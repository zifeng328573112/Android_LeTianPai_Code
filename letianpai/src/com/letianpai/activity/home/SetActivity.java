package com.letianpai.activity.home;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.letianpai.R;
import com.letianpai.activity.LoginActivity;
import com.letianpai.activity.family.FamilyListActivity;
import com.letianpai.application.ManageApplication;
import com.letianpai.db.DBUtiles;

public class SetActivity extends Activity implements OnClickListener {
	View basic, newmsg_bt, voice_bt, retri_bt, empty_chat, about_letianpai,
			headBackText, help_letianpai, suggestion_letianpai;
	boolean newmsg_is, voice_is, retri_is;
	ImageView set_image;
	SharedPreferences sp;
	Editor editor;
	TextView headShare, headText, nick;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ManageApplication.getInstance().addActivity(this);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.set);
		init();
		basic.setOnClickListener(this);
		newmsg_bt.setOnClickListener(this);
		retri_bt.setOnClickListener(this);
		voice_bt.setOnClickListener(this);
		empty_chat.setOnClickListener(this);
		about_letianpai.setOnClickListener(this);
		headBackText.setOnClickListener(this);
		help_letianpai.setOnClickListener(this);
		suggestion_letianpai.setOnClickListener(this);
		// 设置联系人头像
		//Bitmap bitmap = null;
		Bitmap bitmap = FamilyListActivity.mb;
		/*try {
			bitmap = ChatUtils.getUserImage(LoginActivity.USERID);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		*/
		if (bitmap != null) {
			set_image.setImageBitmap(bitmap);
		}		// 设置联系人昵称
		/*String nickname = ChatUtils.getname(XmppTool.getConnection(),
				LoginActivity.account);*/
		String nickname = FamilyListActivity.nickname;
		if (nickname != null) {
			nick.setText(nickname);
		} else {
			nick.setText("还未设置昵称");
		}
	}

	private void init() {
		sp = getSharedPreferences("myPref", MODE_WORLD_READABLE);
		// 获取数据后面一个参数表示无返回时的默认值
		editor = sp.edit();
		nick = (TextView) findViewById(R.id.nick);
		basic = findViewById(R.id.basic);
		headShare = (TextView) findViewById(R.id.headShare);
		headText = (TextView) findViewById(R.id.headText);
		headText.setText("设置");
		headShare.setText("");
		set_image = (ImageView) findViewById(R.id.pic);
		suggestion_letianpai = findViewById(R.id.suggestion_letianpai);
		help_letianpai = findViewById(R.id.help_letianpai);
		headBackText = findViewById(R.id.headBackText);
		about_letianpai = findViewById(R.id.about_letianpai);
		newmsg_bt = findViewById(R.id.newmsg_bt);
		empty_chat = findViewById(R.id.empty_chat);
		voice_bt = findViewById(R.id.voice_bt);
		retri_bt = findViewById(R.id.retrieval_bt);
		TextView setnumber = (TextView) findViewById(R.id.set_number);
		setnumber.setText(sp.getString("username", ""));
		String name = sp.getString("sort", "2");
		String name1 = sp.getString("remind", "2");
		if (name.equals("0")) {
			retri_is = true;
			retri_bt.setBackgroundResource(R.drawable.btn_on);

		} else if (name.equals("1")) {
			retri_is = false;
			retri_bt.setBackgroundResource(R.drawable.btn_close);

		}

		if (name1.equals(0)) {
			voice_bt.setBackgroundResource(R.drawable.btn_on);
			voice_is = true;
		} else if (name1.equals("1")) {
			voice_bt.setBackgroundResource(R.drawable.btn_close);
			voice_is = false;
		}

	}

	@Override
	public void onClick(View arg0) {
		switch (arg0.getId()) {
		case R.id.basic:
			Intent intent = new Intent(this, SetDetailActivity.class);
			startActivity(intent);
			break;
		case R.id.newmsg_bt:
			if (newmsg_is) {
				newmsg_bt.setBackgroundResource(R.drawable.btn_on);
				newmsg_is = false;
			} else {
				newmsg_bt.setBackgroundResource(R.drawable.btn_close);
				newmsg_is = true;
			}

			break;
		case R.id.retrieval_bt:
			if (retri_is == false) {
				retri_bt.setBackgroundResource(R.drawable.btn_on);
				retri_is = true;
				SharedPreferences sp = getSharedPreferences("myPref",
						MODE_WORLD_READABLE);
				// 获取数据后面一个参数表示无返回时的默认值
				Editor editor = sp.edit();
				editor.putString("sort", "0");
				editor.commit();
			} else {
				retri_bt.setBackgroundResource(R.drawable.btn_close);
				retri_is = false;

				editor.putString("sort", "1");
				editor.commit();

			}
			break;
		case R.id.voice_bt:
			if (voice_is == false) {
				voice_bt.setBackgroundResource(R.drawable.btn_on);
				voice_is = true;
				editor.putString("remind", "0");
				editor.commit();

			} else {
				voice_bt.setBackgroundResource(R.drawable.btn_close);
				voice_is = false;
				editor.putString("remind", "1");
				editor.commit();

			}
			break;
		case R.id.empty_chat:
			DBUtiles.deleteDB(this, sp.getString("username", ""));
			Log.d("zzzz", sp.getString("username", ""));
			break;
		case R.id.about_letianpai:
			startActivity(new Intent(this, AboutActivity.class));
			break;
		case R.id.headBackText:
			finish();
			break;
		case R.id.help_letianpai:
			startActivity(new Intent(this, AboutActivity.class));

			break;
		case R.id.suggestion_letianpai:
			startActivity(new Intent(this, AboutActivity.class));

			break;
		default:
			break;

		}
		;
	}

}
