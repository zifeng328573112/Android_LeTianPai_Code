package com.letianpai.adapter.homeland;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.letianpai.R;
import com.letianpai.bean.homeland.FriendListHome;

public class MemListAdapter extends BaseAdapter {
	private Context context;
	private LayoutInflater inflater;
	List<FriendListHome> arraylist = new ArrayList<FriendListHome>();
	View.OnClickListener clicker;

	public void setArray(ArrayList<FriendListHome> arraylist) {
		this.arraylist = arraylist;
	}

	public MemListAdapter(Context context, ArrayList<FriendListHome> arraylist,
			View.OnClickListener clicker) {
		super();
		this.context = context;
		inflater = LayoutInflater.from(context);
		this.arraylist = arraylist;
		this.clicker = clicker;
	}

	@Override
	public int getCount() {
		return arraylist.size();
	}

	@Override
	public Object getItem(int position) {
		return arraylist.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		view = inflater.inflate(R.layout.memlist_item, parent, false);
		ImageView list_mem_image = (ImageView) view
				.findViewById(R.id.get_mem_image);
		if (!(null == arraylist.get(position).getPic())) {
			list_mem_image.setImageBitmap(arraylist.get(position).getPic());
		}
		TextView list_mem_name = (TextView) view
				.findViewById(R.id.get_mem_name);
		list_mem_name.setText(arraylist.get(position).getNickname());
		CheckBox checkBox_homeland = (CheckBox) view
				.findViewById(R.id.checkBox_homeland);
		checkBox_homeland.setOnClickListener(clicker);
		checkBox_homeland.setTag(position);
		return view;
	}

}
