package com.letianpai.adapter.photoalbum;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 14-9-25
 * Time: 下午3:54
 * To change this template use File | Settings | File Templates.
 */
public class HomePhotoBean implements Serializable {
    private String imageUrl = null; //图片地址
    private String homeTitle; //相册标题
    private String number;
    private String uploadPeople;
    private String time;
    private boolean whetherPublic;
    private String albumId;//相册分类的标示

    public String getAlbumId() {
        return albumId;
    }

    public void setAlbumId(String albumId) {
        this.albumId = albumId;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getHomeTitle() {
        return homeTitle;
    }

    public void setHomeTitle(String homeTitle) {
        this.homeTitle = homeTitle;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getUploadPeople() {
        return uploadPeople;
    }

    public void setUploadPeople(String uploadPeople) {
        this.uploadPeople = uploadPeople;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public boolean isWhetherPublic() {
        return whetherPublic;
    }

    public void setWhetherPublic(boolean whetherPublic) {
        this.whetherPublic = whetherPublic;
    }
}


