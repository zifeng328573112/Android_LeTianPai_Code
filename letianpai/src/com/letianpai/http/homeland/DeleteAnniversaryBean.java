package com.letianpai.http.homeland;

public class DeleteAnniversaryBean {
	private String remindid;
	private String username;
	private String roomid;
	private String action;
	
	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getRemindid() {
		return remindid;
	}

	public void setRemindid(String remindid) {
		this.remindid = remindid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getRoomid() {
		return roomid;
	}

	public void setRoomid(String roomid) {
		this.roomid = roomid;
	}

}
