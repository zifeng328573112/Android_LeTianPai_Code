package com.letianpai.adapter.photoalbum;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;

import com.letianpai.R;
import com.letianpai.activity.photoalbum.BrowsePhotoActivity;
import com.letianpai.activity.photoalbum.NewAlbumActivity;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2014/10/5.
 */
public class PictureNetwokAdapter extends BaseAdapter {
    DisplayImageOptions options; // 配置图片加载及显示选项
    private ImageLoader imageLoader;
    private Context context;
    private List<PictureNetworkInfo> photoInfoList;
    private LayoutInflater layoutInflater;
    private boolean delete;
    private String username;

    public PictureNetwokAdapter(Context context, boolean delete, String username, List<PictureNetworkInfo> photoInfoList) {
        this.context = context;
        this.delete = delete;
        this.username = username;
        this.photoInfoList = photoInfoList;
        layoutInflater = LayoutInflater.from(context);
        // 配置图片加载及显示选项（还有一些其他的配置，查阅doc文档吧）
        options = new DisplayImageOptions.Builder()
//                .showStubImage(R.drawable.ic_launcher) // 在ImageView加载过程中显示图片
                .showImageForEmptyUri(R.drawable.ic_launcher) // image连接地址为空时
                .showImageOnFail(R.drawable.add_home) // image加载失败
                .cacheInMemory(true) // 加载图片时会在内存中加载缓存
                .cacheOnDisc(true) // 加载图片时会在磁盘中加载缓存
                .displayer(new RoundedBitmapDisplayer(5, false)) // 设置用户加载图片task(这里是圆角图片显示)
                .build();
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
    }

    @Override
    public int getCount() {
        if (null == photoInfoList) {
            return 0;
        }
        return photoInfoList.size();
    }


    @Override
    public Object getItem(int position) {
        return photoInfoList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        ViewHolder holder = null;
        if (view == null) {
            holder = new ViewHolder();
            view = layoutInflater.inflate(R.layout.new_album_picture_netwok_item, null);
            holder.memberCb = (CheckBox) view.findViewById(R.id.new_album_member_cb);
            holder.memberIv = (ImageView) view.findViewById(R.id.new_album_member_iv);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        PictureNetworkInfo photoInfo = photoInfoList.get(position);
        imageLoader.displayImage(photoInfo.getPhotoUrl(), holder.memberIv, options, null);
        holder.memberCb.setOnCheckedChangeListener(new CheckChangeListener(position));
        if (delete) {
            if (photoInfo.isWhetherSelected()) {
                holder.memberCb.setChecked(true);
            } else {
                holder.memberCb.setChecked(false);
            }
            holder.memberCb.setVisibility(View.VISIBLE);
        } else {
        	ArrayList <String> photoUrlList = new ArrayList<String>(); 
        	for (PictureNetworkInfo mPhotoInfo : photoInfoList){
        		photoUrlList.add(mPhotoInfo.getPhotoUrl());
        	}
        	view.setOnClickListener(new PhotoOnClickListener(photoUrlList,position));
            holder.memberCb.setVisibility(View.GONE);
        }
        return view;
    }

    class PhotoOnClickListener implements View.OnClickListener {
    	ArrayList <String> photoUrlList;
        private int position;
		PhotoOnClickListener(ArrayList <String> photoUrlList,int position) {
        	this.photoUrlList = photoUrlList;
        	this.position = position;
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(context, BrowsePhotoActivity.class);
            intent.putStringArrayListExtra("PHOTO_INFO_LIST", photoUrlList);
//            intent.putExtra("CURRENT_URL", photoUrlList.get(position));
            intent.putExtra("CURRENT_POSITION", position);
            context.startActivity(intent);
        }
    }

    class CheckChangeListener implements CompoundButton.OnCheckedChangeListener {
        int position;

        CheckChangeListener(int position) {
            this.position = position;
        }

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (isChecked) {
                photoInfoList.get(position).setWhetherSelected(true);
            } else {
                photoInfoList.get(position).setWhetherSelected(false);
            }
        }
    }


    private class ViewHolder {
        ImageView memberIv;
        CheckBox memberCb;
    }
}

