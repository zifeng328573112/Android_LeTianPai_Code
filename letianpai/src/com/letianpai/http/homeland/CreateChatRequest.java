package com.letianpai.http.homeland;


public class CreateChatRequest {
	private String head;
	private ChatRoomBean content;
	public String getHead() {
		return head;
	}
	public void setHead(String head) {
		this.head = head;
	}
	public ChatRoomBean getContent() {
		return content;
	}
	public void setContent(ChatRoomBean content) {
		this.content = content;
	}
	
}
