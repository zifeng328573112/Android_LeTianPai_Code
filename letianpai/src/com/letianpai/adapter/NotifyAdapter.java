package com.letianpai.adapter;

import java.util.ArrayList;
import java.util.List;

import com.letianpai.R;
import com.letianpai.bean.FriendInfo;
import com.letianpai.db.RequestInformation;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.BaseAdapter;
import android.widget.TextView;

/*
 * ֪ͨ��Ϣ������
 */
public class NotifyAdapter extends BaseAdapter {
	List<RequestInformation> arraylist = new ArrayList<RequestInformation>();
	private Context context;
	private LayoutInflater inflater;

	public void setArraylist(List<RequestInformation> arraylist) {
		this.arraylist = arraylist;
	}

	public NotifyAdapter(Context context, ArrayList<RequestInformation> arraylist) {
		super();
		this.context = context;
		inflater = LayoutInflater.from(context);
		this.arraylist = arraylist;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return arraylist.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return arraylist.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		view = inflater.inflate(R.layout.notify_item, parent, false);
		TextView topLeft = (TextView) view.findViewById(R.id.topLeft);// �û���
		TextView bottomLeft1 = (TextView) view.findViewById(R.id.bottomLeft1);// �û��˺�
		TextView notify_time = (TextView) view.findViewById(R.id.notify_time);
		topLeft.setText(arraylist.get(position).getuName());
		bottomLeft1.setText(arraylist.get(position).getuName());
		notify_time.setText(arraylist.get(position).getUtime());
		return view;
	}

}
