package com.letianpai.activity.homeland;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smackx.muc.MultiUserChat;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectPostRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.letianpai.R;
import com.letianpai.activity.photoalbum.HomeLandPhotoActivity;
import com.letianpai.adapter.homeland.GetMemAdapter;
import com.letianpai.adapter.homeland.MemberInfo;
import com.letianpai.common.server.ResultBean;
import com.letianpai.common.server.ServerInterface;
import com.letianpai.common.server.ServerTool;
import com.letianpai.db.Constant;
import com.letianpai.http.homeland.ChatRoomBean;
import com.letianpai.http.homeland.CreateChatRequest;
import com.letianpai.http.homeland.FindRoomIdBean;
import com.letianpai.http.homeland.FindRoomIdRequest;
import com.letianpai.http.homeland.GetRoomData;
import com.letianpai.http.homeland.JoinHoemlandBean;
import com.letianpai.http.homeland.JoinHomelandRequest;
import com.letianpai.http.homeland.UniqueBack;
import com.letianpai.http.homeland.UniqueBean;
import com.letianpai.http.homeland.UniqueRequest;
import com.letianpai.utils.JsonUtil;
import com.letianpai.utils.XmppTool;

/*
 * 家园
 */
public class HomelandActivity extends Activity {
	TextView headText, button_homeland;
	public static TextView name_homeland, number_homeland;
	View homeland_create, homeland_add, change_homeland,add_member,delete_member;
	View seacher_homeland, sure_homeland, function_homeland, invite_homeland;// 搜索确定
	EditText edit, edit_invite;// 房间名称,邀请
	View homeland_picture, homeland_events, homeland_setup;// 三个模块
	String roomName;// 获取的房间名称
	String username;// 登录用户名
	String temporarysignId;// 临时的房间ID
	View mem_homeland, homeland, home1, home2, no_framelayout;
	RequestQueue requestQueue;
	Editor editor;
	MultiUserChat mu;
	static String signId;// 聊天室标志
	SharedPreferences sp;
	public boolean flag = true;// 判断是加入还是创建
	ProgressDialog pd;
	String handleText = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.homeland_main);
		initView();
		automaticLogin();
	}

	// 初始化
	private void initView() {
		sp = getSharedPreferences("myPref", MODE_WORLD_READABLE);
		editor = sp.edit();
		homeland = findViewById(R.id.homeland);
		mem_homeland = findViewById(R.id.mem_homeland);
		delete_member = findViewById(R.id.delete_member);
		add_member = findViewById(R.id.add_member);
		add_member.setOnClickListener(new HomelandClick());
		delete_member.setOnClickListener(new HomelandClick());
		home1 = findViewById(R.id.home1);
		home1.setOnClickListener(new HomelandClick());
		home1.setSelected(true);
		home2 = findViewById(R.id.home2);
		home2.setOnClickListener(new HomelandClick());
		headText = (TextView) findViewById(R.id.headText);
		headText.setText("家园");
		no_framelayout = findViewById(R.id.no_framelayout);
		homeland_create = (View) findViewById(R.id.homeland_create);
		homeland_create.setOnClickListener(new HomelandClick());
		homeland_add = (View) findViewById(R.id.homeland_add);
		homeland_add.setOnClickListener(new HomelandClick());
		seacher_homeland = findViewById(R.id.seacher_homeland);
		sure_homeland = findViewById(R.id.sure_homeland);
		sure_homeland.setOnClickListener(new HomelandClick());
		button_homeland = (TextView) findViewById(R.id.button_homeland);
		edit = (EditText) findViewById(R.id.edit);
		edit_invite = (EditText) findViewById(R.id.edit_invite);
		function_homeland = findViewById(R.id.function_homeland);
		invite_homeland = findViewById(R.id.invite_homeland);
		// 相册，纪念日，设置
		homeland_picture = findViewById(R.id.homeland_picture);
		homeland_picture.setOnClickListener(new HomelandClick());
		homeland_events = findViewById(R.id.homeland_events);
		homeland_events.setOnClickListener(new HomelandClick());
		homeland_setup = findViewById(R.id.homeland_setup);
		homeland_setup.setOnClickListener(new HomelandClick());

		number_homeland = (TextView) findViewById(R.id.number_homeland);
		name_homeland = (TextView) findViewById(R.id.name_homeland);
		change_homeland = (View) findViewById(R.id.change_homeland);
		change_homeland.setOnClickListener(new HomelandClick());
		requestQueue = Volley.newRequestQueue(this);
	}

	// 是否有登录记录如果有记录就自动登录
	public void automaticLogin() {
		// 自动登录用户名密码
		final String username1 = sp.getString("username", "1");
		username = username1;
		final String password = sp.getString("password", "1");
		String islogin = sp.getString("islogin", "1");
		// 加入聊天室最后一个的标志
		signId = sp.getString("signId", "1");
		// 自动登录若果有记录
		if ("0".equals(islogin)) {
			Log.d("zzzz", "islogin" + islogin);
			pd = new ProgressDialog(this);
			pd.setTitle("请稍等");
			pd.setMessage("正在加入...");
			pd.show();
			pd.setCanceledOnTouchOutside(false);
			new Thread(new Runnable() {

				@Override
				public void run() {
					pd.cancel();

					if(!signId.equals("1")){
						mu = new MultiUserChat(XmppTool.getConnection(), signId
								+ "@conference." + XmppTool.con.getServiceName());

						try {
							mu.join(username, "123456");
						} catch (XMPPException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						handler.sendEmptyMessage(1);
						Log.d("zzzz", "加入成功");
					}else{
						// 提醒没有家园
						handler.sendEmptyMessage(19);
					}
					
				}
			}).start();

		} else {
			if ("1".equals(username)) {
				handler.sendEmptyMessage(9);//跳转登录界面
			} else {
				pd = new ProgressDialog(this);
				pd.setTitle("请稍等");
				pd.setMessage("正在登录...");
				pd.show();
				pd.setCanceledOnTouchOutside(false);
				new Thread(new Runnable() {
					@Override
					public void run() {
						try {
							XmppTool.getConnection().login(username, password);
							// 状态
							Presence presence = new Presence(
									Presence.Type.available);
							XmppTool.getConnection().sendPacket(presence);
							// 登录openfire
							// 加入聊天室
							editor.putString("islogin", "0");
							editor.commit();
							Log.d("zzzz", signId + "signId");

							if (!signId.equals("1")) {
								mu = new MultiUserChat(
										XmppTool.getConnection(), signId
												+ "@conference."
												+ XmppTool.con.getServiceName());

								mu.join(signId, "123456");
								joinHomeland();
								handler.sendEmptyMessage(1);
								Log.d("zzzz", "加入成功");
							} else {
								// 提醒没有家园
								handler.sendEmptyMessage(19);
							}

							pd.cancel();

						} catch (XMPPException e) {
							//登录失败跳转登录界面
							handler.sendEmptyMessage(9);
							pd.cancel();
						} catch (IllegalStateException e) {
							handler.sendEmptyMessage(5);
							handler.sendEmptyMessage(9);
							pd.cancel();
						}

					}

				}).start();
			}
		}

	}

	// 点击事件
	class HomelandClick implements View.OnClickListener {

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			// 点击创建家园
			case R.id.homeland_create:
				flag = true;
				no_framelayout.setVisibility(View.GONE);
				seacher_homeland.setVisibility(View.VISIBLE);
				sure_homeland.setVisibility(View.VISIBLE);
				button_homeland.setText("创建");
				function_homeland.setVisibility(View.GONE);
				invite_homeland.setVisibility(View.INVISIBLE);
				edit.setVisibility(View.VISIBLE);
				edit_invite.setVisibility(View.VISIBLE);
				break;
			// 加入家园
			case R.id.homeland_add:
				flag = false;
				no_framelayout.setVisibility(View.GONE);
				seacher_homeland.setVisibility(View.VISIBLE);
				invite_homeland.setVisibility(View.VISIBLE);
				function_homeland.setVisibility(View.GONE);
				sure_homeland.setVisibility(View.VISIBLE);
				edit_invite.setHint("请输入家园号");
				button_homeland.setText("确定");
				break;
			case R.id.sure_homeland:
				// 创建家园
				if (flag == true) {
					boolean isTrue = isCreateLegal();
					Log.d("info", isTrue + "");
					if (isTrue == true) {
						pd = new ProgressDialog(HomelandActivity.this);
						pd.setTitle("请稍等");
						pd.setMessage("正在创建...");
						pd.show();
						pd.setCanceledOnTouchOutside(false);
						new Thread(new Runnable() {
							@Override
							public void run() {
								/*
								 * try { XmppTool.closeConnection(); String
								 * password = sp.getString("password", "1");
								 * 
								 * XmppTool.getConnection().login(username,
								 * password); // 状态 Presence presence = new
								 * Presence( Presence.Type.available);
								 * XmppTool.getConnection().sendPacket(
								 * presence); // 登录openfire } catch
								 * (XMPPException e) { // TODO Auto-generated
								 * catch block e.printStackTrace(); }
								 */
								uniqueChatPost();
							}
						}).start();
					}

				} else {
					// 加入聊天室
					boolean isTrue = isLegal();
					if (isTrue == true) {
						pd = new ProgressDialog(HomelandActivity.this);
						pd.setTitle("请稍等");
						pd.setMessage("正在加入...");
						pd.show();
						pd.setCanceledOnTouchOutside(false);
						new Thread(new Runnable() {
							@Override
							public void run() {
								temporarysignId = edit.getText().toString();
								// try {
								String password = sp.getString("password", "1");

								isExistPost(temporarysignId);

							}
						}).start();
					}

				}
				break;
			case R.id.homeland_picture:
				Intent intent1 = new Intent(HomelandActivity.this,
						HomeLandPhotoActivity.class);
				intent1.putExtra("username", username);
				intent1.putExtra("roomid", signId);
				startActivity(intent1);
				break;
			case R.id.homeland_events:
				Intent intent = new Intent(HomelandActivity.this,
						AnniversaryActivity.class);
				intent.putExtra("signId", signId);
				intent.putExtra("username", username);
				startActivity(intent);
				break;
			case R.id.homeland_setup:
				break;
			// 切换家园获取家园列表
			case R.id.change_homeland:
				intent = new Intent(HomelandActivity.this,
						ChangeHomelandActivity.class);
				intent.putExtra("username", username);
				startActivityForResult(intent, 200);
				break;
			case R.id.delete_member:
				intent = new Intent(HomelandActivity.this, AddMActivity.class);
				intent.putExtra("title", "移除成员");
				intent.putExtra("username", username);
				intent.putExtra("signId", signId);
				startActivityForResult(intent, 300);
				break;
			case R.id.add_member:
				intent = new Intent(HomelandActivity.this, AddMActivity.class);
				intent.putExtra("title", "添加成员");
				intent.putExtra("username", username);
				intent.putExtra("signId", signId);
				startActivityForResult(intent, 100);
				break;
			case R.id.home2:
				no_framelayout.setVisibility(View.GONE);
				home1.setSelected(false);
				home2.setSelected(true);
				mem_homeland.setVisibility(View.VISIBLE);
				homeland.setVisibility(View.GONE);
				getMemPost();
				break;
			case R.id.home1:
				home1.setSelected(true);
				home2.setSelected(false);
				mem_homeland.setVisibility(View.GONE);
				homeland.setVisibility(View.VISIBLE);
				if (!"1".equals(signId)) {
					handler.sendEmptyMessage(1);
				} else {
					seacher_homeland.setVisibility(View.GONE);
					invite_homeland.setVisibility(View.GONE);
					no_framelayout.setVisibility(View.VISIBLE);
					sure_homeland.setVisibility(View.GONE);
				}
				break;
			default:
				break;
			}
		}

	}

	// handler
	private Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			switch (msg.what) {
			case 1:
				no_framelayout.setVisibility(View.GONE);

				function_homeland.setVisibility(View.VISIBLE);
				seacher_homeland.setVisibility(View.GONE);
				sure_homeland.setVisibility(View.INVISIBLE);
				if("".equals(handleText)){
					
					name_homeland.setText(roomName);
				}else{
					name_homeland.setText(handleText);

				}
				invite_homeland.setVisibility(View.GONE);
				number_homeland.setText(signId + "");
				break;
			case 2:
				createChatRoom();
				break;
			case 3:
				joinHomeland();
				break;
			case 4:
				Toast.makeText(HomelandActivity.this, "密码错误或账号不存在",
						Toast.LENGTH_SHORT).show();
				break;
			case 5:
				Toast.makeText(HomelandActivity.this, "连接异常,请检查网络",
						Toast.LENGTH_SHORT).show();
				break;
			case 6:
				// 是否存在房间
				addHomeland();
				break;
			case 7:
				pd.cancel();
				Toast.makeText(HomelandActivity.this, "房间不存在",
						Toast.LENGTH_SHORT).show();
				break;
			// 没有连接跳转登录界面
			case 9:

				break;
			// 没有创建家园提醒创建家园
			case 19:
				no_framelayout.setVisibility(View.VISIBLE);
				break;
			default:
				break;
			case 20:
				name_homeland.setText(handleText);

				break;
			}
		}
	};

	// 创建聊天室post方法
	public void createChatPost() {
		Log.d("zzzz", "正在创建房间");
		ChatRoomBean userInfo = new ChatRoomBean();
		CreateChatRequest user = new CreateChatRequest();
		userInfo.setGname(roomName);
		userInfo.setPasswd("123456");
		userInfo.setMax_num(30);
		userInfo.setOfroomid(signId);
		userInfo.setOwner(username);
		userInfo.setAction("add");
		user.setHead("roomServlet");
		user.setContent(userInfo);
		// msg值
		String msg = JsonUtil.objectToString(user, CreateChatRequest.class);
		String url = Constant.URL + "?msg=" + msg;
		Log.d("zzzz", url);
		Map<String, String> map = new HashMap<String, String>();
		map.put("msg", msg);
		JsonObjectPostRequest request = new JsonObjectPostRequest(Constant.URL,
				new ls(), new el(), map) {

		};
		requestQueue.add(request);
	}

	class ls implements Listener<JSONObject> {

		@Override
		public void onResponse(JSONObject arg0) {
			pd.cancel();
			JSONTokener jsonParser = new JSONTokener(arg0.toString());
			JSONObject person;
			String a = "";
			String b = "";
			String c = "";
			try {
				person = (JSONObject) jsonParser.nextValue();
				Log.d("zzzzz", person.getString("status"));
				a = person.getString("data");
				b = person.getString("status");
				c = person.getString("message");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Log.d("zzzz", b + "创建状态" + c);
			if (b.equals("SUCCESS")) {
				Toast.makeText(HomelandActivity.this, "创建成功",
						Toast.LENGTH_SHORT).show();
				editor.putString("signId", signId + "");
				Log.d("zzzz", signId + "signId");
				editor.commit();
				handler.sendEmptyMessage(1);
			}

			if (b.equals("FALURE")) {
				pd.cancel();
				Toast.makeText(HomelandActivity.this, "创建失败",
						Toast.LENGTH_SHORT).show();
			}

		}

	}

	class el implements ErrorListener {

		@Override
		public void onErrorResponse(VolleyError arg0) {
			Toast.makeText(HomelandActivity.this, "请求失败", Toast.LENGTH_SHORT)
					.show();
		}

	}

	// 获取唯一标志方法
	public void uniqueChatPost() {
		UniqueBean userInfo = new UniqueBean();
		UniqueRequest user = new UniqueRequest();

		userInfo.setAction("cdj");
		user.setHead("roomServlet");
		user.setContent(userInfo);
		// msg值
		String msg = JsonUtil.objectToString(user, UniqueRequest.class);
		String url = Constant.URL + "?msg=" + msg;
		Log.d("zzzz", url);
		StringRequest request = new StringRequest(url, new ls1(), new el1());
		requestQueue.add(request);
	}

	class ls1 implements Listener<String> {

		@Override
		public void onResponse(String arg0) {
			Log.d("zzzz", arg0);
			UniqueBack br = (UniqueBack) JsonUtil.StringToObject(arg0,
					UniqueBack.class);
			if (br.getStatus().equals("SUCCESS")) {
				signId = br.getData();// 获取唯一标志
				handler.sendEmptyMessage(2);
			}

			if (br.getStatus().equals("FALURE")) {
				Toast.makeText(HomelandActivity.this, "创建失败",
						Toast.LENGTH_SHORT).show();
			}

		}

	}

	class el1 implements ErrorListener {

		@Override
		public void onErrorResponse(VolleyError arg0) {
			Toast.makeText(HomelandActivity.this, "请求失败", Toast.LENGTH_SHORT)
					.show();
		}

	}

	// 创建房间的方法
	public void createChatRoom() {
		// 保存聊天室
		roomName = edit.getText().toString();

		String password = sp.getString("password", "1");

		/*
		 * try { XmppTool.disconnect(); XmppTool.getConnection().login(username,
		 * password); // 状态 Presence presence = new
		 * Presence(Presence.Type.available);
		 * XmppTool.getConnection().sendPacket(presence); } catch (XMPPException
		 * e) { } catch (IllegalStateException e) { handler.sendEmptyMessage(5);
		 * }
		 */

		new Thread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				mu = XmppTool.createRoom(signId + "", "123456");
				Log.d("zzzz", mu.toString());
				if (null == mu) {

				} else {
					// 在服务器创建
					new Thread(new Runnable() {

						@Override
						public void run() {
							createChatPost();

						}
					}).start();
				}
			}
		}).start();

	}

	// 第一次加入聊天室
	public void joinHomeland() {
		new Thread(new Runnable() {

			@Override
			public void run() {
				String roomid = edit.getText().toString();
				Log.d("zzzz", roomid+"这里咩有打印");
				if(roomid.equals("")){
					roomid = sp.getString("signId", "");
					Log.d("zzzz", roomid+"这里咩有打印");
				}
				
				JoinHoemlandBean userInfo = new JoinHoemlandBean();
				JoinHomelandRequest user = new JoinHomelandRequest();
				userInfo.setAction("join");
				userInfo.setRoomid(roomid);
				userInfo.setUsername(username);
				user.setHead("roomServlet");
				user.setContent(userInfo);
				String msg = JsonUtil.objectToString(user,
						JoinHomelandRequest.class);
				String url = Constant.URL + "?msg=" + msg;
				Log.d("zzzz", url + "聊天室");
				StringRequest request = new StringRequest(url,
						new Listener<String>() {

							@Override
							public void onResponse(String arg0) {
								Log.d("zzzz", arg0 + "加入聊天室");
								handler.sendEmptyMessage(1);
								editor = sp.edit();
								editor.putString("signId", signId + "");
								editor.commit();
								 GetRoomData b = (GetRoomData) JsonUtil.StringToObject(arg0,
										 GetRoomData.class);
								 handleText = b.getData().getGname();
								 handler.sendEmptyMessage(20);
							}
						}, new ErrorListener() {

							@Override
							public void onErrorResponse(VolleyError arg0) {
								Log.d("zzzz", arg0.toString());
							}
						});
				requestQueue.add(request);
			}
		}).start();
	}

	// 获取成员
	public void getMemPost() {
		Map<String, String> maps = new HashMap<String, String>();
		maps.put("action", "get");
		maps.put("roomid", signId);
		maps.put("username", username);
		ServerTool.getInstance().methodPost(HomelandActivity.this,
				"roomMembersServlet", maps, new ContactRealize());

	}

	class ContactRealize implements ServerInterface {

		@Override
		public void gainData(ResultBean bean) {
			if (bean.isSuccess()) {
				List<MemberInfo> infoList = new ArrayList<MemberInfo>();
				try {

					JSONObject parentObj = new JSONObject(
							bean.getResultContent());
					String static_url = parentObj.getString("static_url");
					JSONArray childArray = parentObj
							.getJSONArray("member_list");
					int count = childArray.length();
					for (int i = 0; i < count; i++) {
						JSONObject childObj = childArray.getJSONObject(i);
						MemberInfo member = new MemberInfo();
						member.setUsername(childObj.getString("username"));
						member.setNickname(childObj.getString("nickname"));
						member.setSex(childObj.getString("sex"));
						member.setRelation(childObj.getString("relation"));
						infoList.add(member);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				GetMemAdapter ga = new GetMemAdapter(HomelandActivity.this,
						infoList);
				ListView list = (ListView) findViewById(R.id.listView);
				list.setAdapter(ga);

			}

		}
	}

	// 查询成员是否存在
	public void isExistPost(String id) {
		FindRoomIdBean userInfo = new FindRoomIdBean();
		FindRoomIdRequest user = new FindRoomIdRequest();
		userInfo.setAction("find");
		userInfo.setRoomid(id);
		user.setContent(userInfo);
		user.setHead("roomServlet");
		String msg = JsonUtil.objectToString(user, FindRoomIdRequest.class);
		String url = Constant.URL + "?msg=" + msg;
		Log.d("zzzz", url);
		StringRequest request = new StringRequest(url, new Listener<String>() {

			@Override
			public void onResponse(String arg0) {
				Log.d("zzzz", arg0);
				JSONTokener jsonParser = new JSONTokener(arg0);
				JSONObject person;
				String a = "";
				try {
					person = (JSONObject) jsonParser.nextValue();
					Log.d("zzzz", person.getString("status"));
					a = person.getString("status");
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				if (a.equals("SUCCESS")) {
					handler.sendEmptyMessage(6);
					signId = temporarysignId;
				} else {
					temporarysignId = "";
					handler.sendEmptyMessage(7);
				}
			}
		}, new ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError arg0) {

			}
		});
		requestQueue.add(request);
	}

	// 判断加入输入是否合法
	public boolean isLegal() {
		String aEdit = edit.getText().toString();
		String bEdit = edit_invite.getText().toString();
		if (!"".equals(aEdit) && !"".equals(bEdit)) {
			return true;
		} else {
			Toast.makeText(HomelandActivity.this, "请输入家园名称", Toast.LENGTH_LONG)
					.show();
			return false;

		}
	}

	// 判断创建输入是否合法
	public boolean isCreateLegal() {
		String aEdit = edit.getText().toString();
		if (!"".equals(aEdit)) {
			return true;
		} else {
			Toast.makeText(HomelandActivity.this, "请输入家园名称", Toast.LENGTH_LONG)
					.show();
			return false;
		}
	}

	public void addHomeland() {
		new Thread(new Runnable() {

			@Override
			public void run() {
				String password = edit_invite.getText().toString();
				/*
				 * String username = sp.getString("username", "1"); String
				 * password1 = sp.getString("password", "1");
				 * 
				 * try { XmppTool.getConnection().login(username, password1); }
				 * catch (XMPPException e1) { // TODO Auto-generated catch block
				 * e1.printStackTrace(); }
				 * 
				 * // 状态
				 * 
				 * Presence presence = new Presence(Presence.Type.available);
				 * XmppTool.getConnection().sendPacket(presence);
				 */
				mu = new MultiUserChat(XmppTool.con, signId + "@conference."
						+ XmppTool.con.getServiceName());
				try {
					Log.d("zzzz", signId);
					pd.cancel();
					mu.join(signId, password);
					handler.sendEmptyMessage(3);
					Log.d("zzzz", "加入成功");
				} catch (XMPPException e) {
					Log.d("zzzz", "加入失败");
					handler.sendEmptyMessage(4);
					e.printStackTrace();
				}
			}
		}).start();
	}
}
