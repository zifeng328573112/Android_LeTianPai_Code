package com.letianpai.common.server;

import android.content.Context;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.letianpai.bean.BackUserinformation;
import com.letianpai.common.component.loading.LoadingDialog;
import com.letianpai.db.Constant;
import com.letianpai.utils.JsonUtil;
import org.json.JSONException;
import org.json.JSONObject;
import com.android.volley.toolbox.JsonObjectPostRequest;

import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 14-9-24
 * Time: 下午4:04
 * To change this template use File | Settings | File Templates.
 */
public class ServerTool {
    private static class ServerToolBuilder {
        static ServerTool _instance = new ServerTool();
    }

    public static ServerTool getInstance() {
        return ServerToolBuilder._instance;
    }

    public ServerTool() {

    }

    ServerInterface serverInterface;
    private LoadingDialog loadingDialog;

    public void methodPost(Context context, String head, Map<String, String> params, ServerInterface serverInterface) {
        loadingDialog = new LoadingDialog(context);
        loadingDialog.show();
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        this.serverInterface = serverInterface;
        JSONObject parentJson = null;
        try {
            parentJson = new JSONObject();
            parentJson.put("head", head);
            JSONObject childJson = new JSONObject();
            for (String key : params.keySet()) {
                childJson.put(key, params.get(key));
            }
            parentJson.put("content", childJson);
            Map<String, String> map = new HashMap<String, String>();
            map.put("msg", parentJson.toString());
            System.out.println("msg值" + parentJson.toString());
            JsonObjectPostRequest request = new JsonObjectPostRequest(Constant.URL, new SuccessListener(), new WrongListener(), map);
            requestQueue.add(request);
        } catch (Exception e) {
            loadingDialog.dismiss();
            ResultBean bean = new ResultBean();
            bean.setSuccess(false);
            bean.setResultContent("出现异常");
            serverInterface.gainData(bean);
        }
//
    }

    class SuccessListener implements Response.Listener<JSONObject> {

        @Override
        public void onResponse(JSONObject jsonObject) {
            loadingDialog.dismiss();
            BackInfo backInfo = new BackInfo(jsonObject.toString());
            ResultBean bean = new ResultBean();
            if ("SUCCESS".equals(backInfo.getStatus())) {
                bean.setSuccess(true);
                bean.setResultContent(backInfo.getData());
            } else {
                bean.setSuccess(false);
            }
            bean.setMessage(backInfo.getMessage());
            serverInterface.gainData(bean);
        }
    }

    class WrongListener implements Response.ErrorListener {

        @Override
        public void onErrorResponse(VolleyError volleyError) {
            loadingDialog.dismiss();
            ResultBean bean = new ResultBean();
            bean.setSuccess(false);
            bean.setResultContent("出现错误");
            serverInterface.gainData(bean);
        }
    }
}
