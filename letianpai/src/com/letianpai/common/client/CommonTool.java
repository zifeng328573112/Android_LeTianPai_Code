package com.letianpai.common.client;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 14-9-25
 * Time: 下午5:11
 * To change this template use File | Settings | File Templates.
 */
public class CommonTool {
    public static final String PROJECT_NAME = "letianlun";
    public static final String SHJI_PATH = android.os.Environment.getDataDirectory().getAbsolutePath() + "/" + PROJECT_NAME;
    public static final String DATABASE_PATH = android.os.Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + PROJECT_NAME;//路径
    public static final String DATA_FILE_PATH = existSDcard() ? DATABASE_PATH : SHJI_PATH;
    /**
     * ****存放图片的文件夹*****************
     */
    public static final String PROJECT_IMAGE_PATH = DATA_FILE_PATH + "/Image";//路径
    public static final String PROJECT_VIDEO_PATH = DATA_FILE_PATH + "/Video";
    public static final String PROJECT_TEMP_PATH = DATA_FILE_PATH + "/Temp";
    public static final String PROJECT_CACHE_PATH = DATA_FILE_PATH + "/Cache";

    /**
     * ***********************
     */


    //判断是否存在网络
    public static boolean isNotNetworkAvailable(Context context) {
        return !isNetworkAvailable(context);
    }

    //判断是否存在网络
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity == null) {
            return false;
        } else {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null) {
                for (int i = 0; i < info.length; i++) {
                    if ((info[i].getState() == NetworkInfo.State.CONNECTED) || (info[i].getState() == NetworkInfo.State.CONNECTING)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    //判断SD卡是否存在
    public static boolean existSDcard() {
        if (android.os.Environment.getExternalStorageState().equals(
                android.os.Environment.MEDIA_MOUNTED)) {
            return true;
        } else {
            return false;
        }
    }
}

