package com.letianpai.activity.homeland;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.letianpai.R;
import com.letianpai.adapter.homeland.AnniversaryAdapter;
import com.letianpai.bean.homeland.AnniversaryBean;
import com.letianpai.db.Constant;
import com.letianpai.dialog.CustomDialog;
import com.letianpai.http.homeland.AnniverListBean;
import com.letianpai.http.homeland.BackAnniversary;
import com.letianpai.http.homeland.DeleteAnniversaryBean;
import com.letianpai.http.homeland.DeleteAnniversaryRequest;
import com.letianpai.http.homeland.GetMemBean;
import com.letianpai.http.homeland.GetMemRequest;
import com.letianpai.service.PollingService;
import com.letianpai.service.PollingUtils;
import com.letianpai.utils.JsonUtil;

public class AnniversaryActivity extends Activity {
	TextView headText, more;
	View headBackText;
	ListView listview;
	static AnniversaryAdapter ann;
	static ArrayList<AnniversaryBean> list;
	int deleteNum;// 删除的条目
	Intent intent;
	String username;
	String signId;
	RequestQueue requestQueue;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.anniversary_homeland);
		initView();
	}

	private void initView() {
		username = getIntent().getStringExtra("username");
		signId = getIntent().getStringExtra("signId");
		headText = (TextView) findViewById(R.id.headText);
		headText.setText("纪念日");
		
		headBackText = findViewById(R.id.headBackText);
		headBackText.setVisibility(View.VISIBLE);
		headBackText.setOnClickListener(new AnniversaryClick());
		more = (TextView) findViewById(R.id.more);
		more.setVisibility(View.VISIBLE);
		more.setOnClickListener(new AnniversaryClick());
		listview = (ListView) findViewById(R.id.listView);
		list = new ArrayList<AnniversaryBean>();
		// 显示数据
		requestQueue = Volley.newRequestQueue(this);

		new Thread(new Runnable() {

			@Override
			public void run() {
				getPost();
			}
		}).start();
	}

	class AnniversaryClick implements View.OnClickListener {

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			// 删除纪念日
			case R.id.homeland_delete:
				deleteNum = (Integer) v.getTag();
				dialog();
				break;
			// 添加
			case R.id.more:
				intent = new Intent(AnniversaryActivity.this,
						AddAnniversaryActivity.class);
				intent.putExtra("username", username);
				intent.putExtra("signId", signId);
				startActivityForResult(intent,204);
				break;
			// 编辑
			case R.id.home_land_title:
				intent = new Intent(AnniversaryActivity.this,
						EditAnniversaryActivity.class);
				intent.putExtra("ab", list.get((Integer) v.getTag()));
				intent.putExtra("id", (Integer) v.getTag());
				startActivity(intent);
				break;
			case R.id.headBackText:
				finish();
				break;
			default:
				break;
			}
		}

	}

	// 删除纪念日dialog
	protected void dialog() {
		final CustomDialog dialog = new CustomDialog(this,
				R.layout.dialog_add_family);
		View view = dialog.getCustomView();
		TextView family_dialog_detial = (TextView) view
				.findViewById(R.id.family_dialog_detial);
		TextView custom_dialog_title = (TextView) view
				.findViewById(R.id.custom_dialog_title);
		custom_dialog_title.setText("关爱提醒");
		family_dialog_detial.setText("确认删除纪念日吗?");
		dialog.setLeftOnClick(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
				new Thread(new Runnable() {
					@Override
					public void run() {
						deletePost(list.get(deleteNum).getRemindid(), list.get(deleteNum).getRoomid(), username);
					}
				}).start();
				
			}
		});
		dialog.setRightOnClick(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		dialog.show();
	}

	// 获取提醒
	public void getPost() {
		GetMemBean userInfo = new GetMemBean();
		GetMemRequest user = new GetMemRequest();
		userInfo.setAction("get");
		userInfo.setRoomid(signId);
		userInfo.setUsername(username);
		user.setContent(userInfo);
		user.setHead("roomRemindServlet");
		String msg = JsonUtil.objectToString(user, GetMemRequest.class);
		String url = Constant.URL + "?msg=" + msg;
		Log.d("zzzz", url);
		StringRequest request = new StringRequest(url, new Listener<String>() {

			@Override
			public void onResponse(String arg0) {
				// {"status":"","data":{},"message":""}
				Log.d("nimei", arg0);
				JSONTokener jsonParser = new JSONTokener(arg0);
				JSONObject person;
				String a = "";
				try {
					person = (JSONObject) jsonParser.nextValue();
					Log.d("zzzz", person.getString("status"));
					a = person.getJSONObject("data").toString();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				ann = new AnniversaryAdapter(AnniversaryActivity.this, list,
						new AnniversaryClick());
				listview.setAdapter(ann);
				if (!a.equals("{}")) {
					BackAnniversary br = (BackAnniversary) JsonUtil
							.StringToObject(arg0, BackAnniversary.class);
					AnniverListBean[] data = br.getData();
					list.clear();
					for (int i = 0; i < data.length; i++) {
						
						AnniversaryBean ab = new AnniversaryBean();
						ab.setAdvancedays(data[i].getAdvancedays());
						ab.setNote(data[i].getNote());
						ab.setTitle(data[i].getTitle());
						ab.setPeriod(data[i].getPeriod());
						ab.setRemindid(data[i].getId());
						ab.setRoomid(data[i].getRoomid());
						ab.setUsername(username);
						list.add(ab);
						ann.setArraylist(list);
						ann.notifyDataSetChanged();
						PollingUtils.startPollingService(AnniversaryActivity.this, 24, PollingService.class, PollingService.ACTION,ab);
					}
				}

			}
		}, new ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError arg0) {
				Log.d("zzzz", arg0.toString());
			}
		});
		requestQueue.add(request);
	}

	// 删除纪念日
	public void deletePost(String remindid, String roomid, String username) {
		DeleteAnniversaryBean userInfo = new DeleteAnniversaryBean();
		DeleteAnniversaryRequest user = new DeleteAnniversaryRequest();
		userInfo.setAction("del");
		userInfo.setRemindid(remindid);
		userInfo.setRoomid(roomid);
		Log.d("zzzz", roomid);
		userInfo.setUsername(username);
		user.setContent(userInfo);
		user.setHead("roomRemindServlet");
		String msg = JsonUtil.objectToString(user,
				DeleteAnniversaryRequest.class);
		String url = Constant.URL + "?msg=" + msg;
		Log.d("zzzz", url);
		StringRequest request = new StringRequest(url, new Listener<String>() {

			@Override
			public void onResponse(String arg0) {
				Log.d("zzzz", arg0);
				list.remove(deleteNum);
				ann.setArraylist(list);
				ann.notifyDataSetChanged();
			}
		}, new ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError arg0) {

			}
		});
		requestQueue.add(request);
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode==204){
			getPost();
		}
	}
}
