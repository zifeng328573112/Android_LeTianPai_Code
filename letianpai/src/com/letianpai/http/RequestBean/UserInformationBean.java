package com.letianpai.http.RequestBean;

public class UserInformationBean {
	private String head;
	private UserInformation content;
	public String getHead() {
		return head;
	}
	public void setHead(String head) {
		this.head = head;
	}
	public UserInformation getContent() {
		return content;
	}
	public void setContent(UserInformation content) {
		this.content = content;
	}
	
}
