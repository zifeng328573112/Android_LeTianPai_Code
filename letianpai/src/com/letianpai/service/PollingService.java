package com.letianpai.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.letianpai.R;
import com.letianpai.bean.homeland.AnniversaryBean;
/**
 * Polling service
 *
 * 
 */
public class PollingService extends Service {

	public static final String ACTION = "com.letianpai.service.PollingService";
	
	private Notification mNotification;
	private NotificationManager mManager;
	private AnniversaryBean set_date;
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public void onCreate() {
		initNotifiManager();
	}
	
	@Override
	public void onStart(Intent intent, int startId) {
	
	    //拿到设置的时间
		
		try {
	     set_date =(AnniversaryBean) intent.getSerializableExtra("set_date");
	     SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
	     //拿到当前年份 
	     Calendar calendar = Calendar.getInstance();
	     String year=String.valueOf(calendar.get(Calendar.YEAR));
	     
		 Date date = sdf.parse(year+"-"+set_date.getPeriod());
		
		 calendar.setTime(date);
		    //设置的日期加上 提前多少天的 日期
		    calendar.add(Calendar.DAY_OF_MONTH,Integer.valueOf("-"+set_date.getAdvancedays()));
		    //当前日期
		    Calendar calender_2=Calendar.getInstance();
          
			String  before_day=sdf.format(calendar.getTime());
			String today=sdf.format(calender_2.getTime());
			 
			
			if(before_day.equals(today)){
	   	    new PollingThread().start();
		   }
		} catch (ParseException e) {
			e.printStackTrace();
		}
	
	    

	}

	private void initNotifiManager(){
	
		mManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		int icon = R.drawable.ic_launcher;
		mNotification = new Notification();
		mNotification.icon = icon;
		mNotification.tickerText = "New Message";
		mNotification.defaults |= Notification.DEFAULT_SOUND;
		mNotification.flags = Notification.FLAG_AUTO_CANCEL;
	}

	private void showNotification() {
		mNotification.when = System.currentTimeMillis();
		//Navigator to the new activity when click the notification title
		Intent i = new Intent(this, MessageActivity.class);
		PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, i,
				Intent.FLAG_ACTIVITY_NEW_TASK);
		mNotification.setLatestEventInfo(this,
				getResources().getString(R.string.app_name),set_date.getAdvancedays().replace("-", "")+"天后是您的"+set_date.getTitle()+"哦", pendingIntent);
		mManager.notify(0, mNotification);
		
	}

	/**
	 * Polling thread
	 * @Author Ryan
	 * @Create 2013-7-13 上午10:18:34"
	 */

    
	
    
	class PollingThread extends Thread {
		@Override
		public void run() {
			System.out.println("Polling...");
//			if (curDate.equals("")) {
				showNotification();
				System.out.println("New message!");
//			}
		}
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		System.out.println("Service:onDestroy");
	}

}
