package com.letianpai.activity.photoalbum;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import com.letianpai.R;
import com.letianpai.common.server.ResultBean;
import com.letianpai.common.server.ServerInterface;
import com.letianpai.common.server.ServerTool;
import com.letianpai.common.tool.StringUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 14-9-25
 * Time: 下午5:53
 * 创建相册
 */
public class CreateAlbumActivity extends Activity {
    private TextView newAlbumTv, sureTv;   //添加相册,确定按钮
    private EditText albumNameEt;//相册名称
    private String username, roomId;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.create_album);
        newAlbumTv = (TextView) findViewById(R.id.home_lang_title_tianjia_tv);
        ImageButton backTv = (ImageButton) findViewById(R.id.top_back_ib);
        backTv.setOnClickListener(new BackOnClickListener());
        newAlbumTv.setOnClickListener(new AlbumOnClickListener('N'));
        albumNameEt = (EditText) findViewById(R.id.create_album_name_et);
        sureTv = (TextView) findViewById(R.id.create_album_sure_tv);
        sureTv.setOnClickListener(new AlbumOnClickListener('S'));
        Intent intent = getIntent();
        roomId = intent.getStringExtra("roomid");
        username = intent.getStringExtra("username");
    }

    class BackOnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            //成功跳转到添加照片页面
            Intent intent = new Intent();
            setResult(Activity.RESULT_OK, intent);
            //成功跳转到添加照片页面
            finish();
        }
    }

    private class AlbumOnClickListener implements View.OnClickListener {
        char type;

        private AlbumOnClickListener(char type) {
            this.type = type;
        }

        @Override
        public void onClick(View v) {
            String albumName = albumNameEt.getText().toString();
            if (StringUtil.isBlank(albumName)) {
                //提示输入内容
                Toast.makeText(CreateAlbumActivity.this, "请您输入相册名称", Toast.LENGTH_LONG).show();
            } else {
                Map<String, String> map = new HashMap<String, String>();
                map.put("action", "add");
                map.put("roomid", roomId);//房间Id
                map.put("username", username); //创建用户名
                map.put("catname", albumName);
                switch (type) {
                    case 'N': {
                        ServerTool.getInstance().methodPost(CreateAlbumActivity.this, "roomPhotoCategoryServlet", map, new AlbumNewListener());
                        break;
                    }
                    case 'S': {
                        ServerTool.getInstance().methodPost(CreateAlbumActivity.this, "roomPhotoCategoryServlet", map, new AlbumSureListener());
                        break;
                    }
                }
            }
        }
    }


    //添加按钮
    class AlbumNewListener implements ServerInterface {
        @Override
        public void gainData(ResultBean bean) {
            if (bean.isSuccess()) {
                //成功
                albumNameEt.setText(null);
            } else {
                //失败 提示
                Toast.makeText(CreateAlbumActivity.this, "服务器忙，请稍后再试", Toast.LENGTH_SHORT).show();
            }
        }
    }

    //确定 按钮
    class AlbumSureListener implements ServerInterface {
        @Override
        public void gainData(ResultBean bean) {
            if (bean.isSuccess()) {
                //成功跳转到添加照片页面
                Intent intent = new Intent();
                intent.putExtra("refresh", "create_album_return");
                setResult(Activity.RESULT_OK, intent);
                //成功跳转到添加照片页面
                finish();
            } else {
                Toast.makeText(CreateAlbumActivity.this, "服务器忙，请稍后再试", Toast.LENGTH_SHORT).show();
            }
        }
    }
}