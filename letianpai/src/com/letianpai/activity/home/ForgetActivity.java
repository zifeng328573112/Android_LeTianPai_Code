package com.letianpai.activity.home;

import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.letianpai.R;
import com.letianpai.activity.LoginActivity;
import com.letianpai.bean.BackResponse;
import com.letianpai.db.Constant;
import com.letianpai.http.RequestBean.GetPassword;
import com.letianpai.http.RequestBean.GetPasswordUserBean;
import com.letianpai.utils.JsonUtil;

public class ForgetActivity extends Activity {
    EditText username;
    Button nextstep;
    RequestQueue requestQueue;
    View nopwd,pwd_send;
    TextView pwd_text,headShare,headText;
    ImageView cancle_pwd;
    ProgressDialog pd;
    boolean flag = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.forget_pw);
        initView();
    }

    public void initView() {
        cancle_pwd = (ImageView) findViewById(R.id.cancle_pwd);
        cancle_pwd.setOnClickListener(new ForgetClick());
        pwd_text = (TextView) findViewById(R.id.pwd_text);
        requestQueue = Volley.newRequestQueue(this);
        nopwd = findViewById(R.id.nopwd);
        pwd_send = findViewById(R.id.pwd_send);
        username = (EditText) findViewById(R.id.username);
        nextstep = (Button) findViewById(R.id.nextstep);
        nextstep.setOnClickListener(new ForgetClick());
        headShare = (TextView) findViewById(R.id.headShare);
        headShare.setText("");
        headText = (TextView) findViewById(R.id.headText);
        headText.setText("找回密码");
    }

    class ForgetClick implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.nextstep:
                    final String name = username.getText().toString();
                    if(flag == false){
                        if (name.equals("")) {
                            Toast.makeText(ForgetActivity.this, "请输入找回密码的手机号码",
                                    Toast.LENGTH_SHORT).show();
                        } else {
                        	pd = new ProgressDialog(ForgetActivity.this);
                        	pd.setTitle("请稍等");
            				pd.show();
            				pd.setCanceledOnTouchOutside(false);
                            new Thread(new Runnable() {

                                @Override
                                public void run() {
                                    post(name);
                                }
                            }).start();
                        }
                    }else{
                        startActivity(new Intent(ForgetActivity.this,LoginActivity.class));
                        finish();
                    }

                    break;
                case R.id.cancle_pwd:
                    nopwd.setVisibility(View.GONE);
                    break;
                default:
                    break;
            }
        }

    }

    // 调接口获取密码
    public void post(String username) {
        Map<String, String> map = new HashMap<String, String>();
        GetPassword userInfo = new GetPassword();
        userInfo.setUsername(username);
        GetPasswordUserBean user = new GetPasswordUserBean();
        user.setHead("findPwdServlet");
        user.setContent(userInfo);
        // msg值
        String msg = JsonUtil.objectToString(user, GetPasswordUserBean.class);
        String url = Constant.URL + "?msg=" + msg;
        map.put("msg", msg);
        StringRequest request = new StringRequest(url, new ls(), new el());
        requestQueue.add(request);
    }

    // 调用服务器接口发送验证码

    class ls implements Listener<String> {

        @Override
        public void onResponse(String arg0) {
        	pd.cancel();
            BackResponse br = (BackResponse) JsonUtil.StringToObject(arg0,
                    BackResponse.class);

            Toast.makeText(ForgetActivity.this, br.getMessage(),
                    Toast.LENGTH_SHORT).show();
            if (br.getStatus().equals("SUCCESS")) {
                Toast.makeText(ForgetActivity.this, "新密码已经发送到您的手机上请及时更改密码",
                        Toast.LENGTH_SHORT).show();
                pwd_send.setVisibility(View.VISIBLE);
                pwd_text.setText("新密码已经发送到您的手机上请及时更改密码");
                nextstep.setText("返回登录");
                nopwd.setVisibility(View.GONE);
                flag = true;
            }else{
                nopwd.setVisibility(View.VISIBLE);
                pwd_send.setVisibility(View.GONE);
            }

        }

    }

    class el implements ErrorListener {

        @Override
        public void onErrorResponse(VolleyError arg0) {
        	pd.cancel();
            Toast.makeText(ForgetActivity.this, "请求失败", Toast.LENGTH_SHORT)
                    .show();
        }

    }
}
