package com.letianpai.http.RequestBean;

public class GetPasswordUserBean {
	private String head;
	private GetPassword content;
	public String getHead() {
		return head;
	}
	public void setHead(String head) {
		this.head = head;
	}
	public GetPassword getContent() {
		return content;
	}
	public void setContent(GetPassword content) {
		this.content = content;
	}
	
}
