package com.letianpai.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.letianpai.db.DataBaseHelper;
import com.letianpai.db.entity.PictureInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2014/9/29.
 */
public class PictureInfoDao {
    // picture_info(id text,pic_path text,pic_name text,pic_type text,pic_md5 text)
    private Context context;

    public PictureInfoDao(Context context) {
        this.context = context;
    }

    public void insertPicture(PictureInfo picture) {
        DataBaseHelper helper = DataBaseHelper.getInstance(context);
        SQLiteDatabase sqliteDatabase = null;
        Cursor cursor = null;
        try {
            //        ContentValues values = new ContentValues();
            // 向该对象中插入键值对，其中键是列名，值是希望插入到这一列的值，值必须和数据库当中的数据类型一致
            sqliteDatabase = helper.getWritableDatabase();
            ContentValues content = new ContentValues();
            content.put("id", picture.getId());
            content.put("pic_path", picture.getPicPath());
            content.put("pic_name", picture.getPicName());
            content.put("pic_type", picture.getPicType());
            content.put("pic_md5", picture.getPicMd5());
//            picture_info(id text,pic_path text,pic_name text,pic_type text,pic_md5 text)
            //pic_path text,pic_name text
            sqliteDatabase.insertOrThrow("picture_info", null, content);
            sqliteDatabase.setTransactionSuccessful();
            sqliteDatabase.endTransaction();
        } catch (Exception e) {
        } finally {
            helper.closeAll(cursor, sqliteDatabase, helper);
        }
    }

    //删除某个选中
    //添加(不存在)（存在 就不添加）
    public void deletePicture(String id) {
        DataBaseHelper helper = new DataBaseHelper(context);
        SQLiteDatabase sqLiteDatabase = null;
        try {
            helper = DataBaseHelper.getInstance(context);
            sqLiteDatabase = helper.getWritableDatabase();
            String[] strings = new String[]{id};
            sqLiteDatabase.delete("picture_info", "id=?", strings);
        } catch (Exception e) {

        } finally {
            helper.closeAll(null, sqLiteDatabase, helper);
        }
    }

    public void deletePictureAll() {
        DataBaseHelper helper = new DataBaseHelper(context);
        SQLiteDatabase sqLiteDatabase = null;
        try {
            helper = DataBaseHelper.getInstance(context);
            sqLiteDatabase = helper.getWritableDatabase();
            sqLiteDatabase.delete("picture_info", null, null);
        } catch (Exception e) {

        } finally {
            helper.closeAll(null, sqLiteDatabase, helper);
        }
    }

    public List<PictureInfo> gainPictureList() {
        List<PictureInfo> infoList = new ArrayList<PictureInfo>();
        DataBaseHelper helper = new DataBaseHelper(context);
        SQLiteDatabase sqLiteDatabase = null;
        Cursor cursor = null;
        try {
            helper = DataBaseHelper.getInstance(context);
            sqLiteDatabase = helper.getReadableDatabase();
            String[] columnArray = new String[]{"id", "pic_path", "pic_name", "pic_type", "pic_md5"};
            cursor = sqLiteDatabase.query("picture_info", columnArray, null, null, null, null, null);
            while (cursor.moveToNext()) {
                PictureInfo picture = new PictureInfo();
                picture.setId(cursor.getString(cursor.getColumnIndex("id")));
                picture.setPicPath(cursor.getString(cursor.getColumnIndex("pic_path")));
                picture.setPicName(cursor.getString(cursor.getColumnIndex("pic_name")));
                picture.setPicType(cursor.getString(cursor.getColumnIndex("pic_type")));
                picture.setPicMd5(cursor.getString(cursor.getColumnIndex("pic_md5")));
                infoList.add(picture);
                System.out.println("存储" + picture.getPicPath());
            }

        } catch (Exception e) {
        } finally {
            helper.closeAll(cursor, sqLiteDatabase, helper);
        }
        return infoList;
    }
}