package com.letianpai.http.homeland;

public class ChatRoomBean {
    private String ofroomid;//openfire房间标示
    private String gname;//房间名称
    private String owner;//创建者
    private int max_num;//最大人数
    private String passwd;
    private String action;
    
    
	public String getPasswd() {
		return passwd;
	}
	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}
	public String getOfroomid() {
        return ofroomid;
    }
    public void setOfroomid(String ofroomid) {
        this.ofroomid = ofroomid;
    }
    public String getGname() {
        return gname;
    }
    public void setGname(String gname) {
        this.gname = gname;
    }
    public String getOwner() {
        return owner;
    }
    public void setOwner(String owner) {
        this.owner = owner;
    }
    public int getMax_num() {
        return max_num;
    }
    public void setMax_num(int max_num) {
        this.max_num = max_num;
    }
    public String getAction() {
        return action;
    }
    public void setAction(String action) {
        this.action = action;
    }

}
