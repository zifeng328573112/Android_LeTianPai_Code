package com.letianpai.adapter.photoalbum;

import java.io.Serializable;

/**
 * Created by Administrator on 2014/10/5.
 * 服务器上的相册图片
 */
public class PictureNetworkInfo implements Serializable {
    private String photoid;
    private String photoUrl;
    private String owner;
    private String addtime;
    private boolean whetherSelected = false;

    public String getPhotoid() {
        return photoid;
    }

    public void setPhotoid(String photoid) {
        this.photoid = photoid;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getAddtime() {
        return addtime;
    }

    public void setAddtime(String addtime) {
        this.addtime = addtime;
    }

    public boolean isWhetherSelected() {
        return whetherSelected;
    }

    public void setWhetherSelected(boolean whetherSelected) {
        this.whetherSelected = whetherSelected;
    }
}
