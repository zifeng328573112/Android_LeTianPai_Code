package com.letianpai.activity.contacts;

import android.app.Activity;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.*;
import com.letianpai.R;
import com.letianpai.activity.HeadPictureActivity;
import com.letianpai.adapter.AddBtAdapter;
import com.letianpai.bean.AddType;
import com.letianpai.bean.UserContacts;
import com.letianpai.common.tool.StringUtil;
import com.letianpai.utils.ContactsUtils;
import com.umeng.analytics.MobclickAgent;

import java.io.FileNotFoundException;
import java.util.ArrayList;

/*
 * 改
 */
public class AddContactsActivity extends Activity {
    static ImageView image;
    View addMore;
    View headBackText;
    EditText realphoneNum, phoneNum;
    LinearLayout mainLayout;// 主布局
    static LinearLayout addLayout;// 添加布局
    View headShare, headText;
    public ListView biantai;
    public static ArrayList<AddType> info = new ArrayList<AddType>();
    public static AddBtAdapter b_adapter;
    static ContentResolver cr;
    static LayoutInflater inflater;

    public ArrayList<String> editNumber = new ArrayList<String>();
    public ArrayList<String> editEmail = new ArrayList<String>();
    public ArrayList<String> editAddress = new ArrayList<String>();
    public ArrayList<String> editRemarks = new ArrayList<String>();
    public ArrayList<String> editNickname = new ArrayList<String>();
    public ArrayList<String> editIm = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.contact_add);
        addMore = findViewById(R.id.addMore);
        image = (ImageView) findViewById(R.id.pic);
        headText = findViewById(R.id.headText);
        ((TextView) headText).setText("新建联系人");
        inflater = LayoutInflater.from(this);
        addLayout = (LinearLayout) inflater.inflate(R.layout.add_item, null);
        headBackText = findViewById(R.id.headBackText);
        headBackText.setOnClickListener(new ClickAdd());
        image.setOnClickListener(new ClickAdd());
        addMore.setOnClickListener(new ClickAdd());

        headShare = findViewById(R.id.headShare);
        headShare.setOnClickListener(new ClickAdd());
        mainLayout = (LinearLayout) findViewById(R.id.v_main);
        cr = this.getContentResolver();
        phoneNum = (EditText) findViewById(R.id.phoneNum);
        realphoneNum = (EditText) findViewById(R.id.realphoneNum);
        // listview 啊 能不能实现啊
        biantai = (ListView) findViewById(R.id.biantai);
        b_adapter = new AddBtAdapter(this, info);
        biantai.setAdapter(b_adapter);
        Intent intent = getIntent();
        String number = intent.getStringExtra("phoneNumber");
        if (StringUtil.isNotBlank(number)) {
            realphoneNum.setText(number);
        }
    }

    public void getAllText() {
        for (int i = 0; i < info.size(); i++) {

            String phoneNum = info.get(i).getValue();

            if (info.get(i).getType().equals("电话")) {
                editNumber.add(phoneNum);
            }
            if (info.get(i).getType().equals("邮箱")) {
                editEmail.add(phoneNum);
            }
            if (info.get(i).getType().equals("备注")) {
                editRemarks.add(phoneNum);
            }
            if (info.get(i).getType().equals("IM")) {
                editIm.add(phoneNum);
            }
            if (info.get(i).getType().equals("昵称")) {
                editNickname.add(phoneNum);
            }
            if (info.get(i).getType().equals("地址")) {
                editAddress.add(phoneNum);
            }
        }
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        MobclickAgent.onPageStart("AddContactsActivity"); // 统计页面
        MobclickAgent.onResume(this); // 统计时长
    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        MobclickAgent.onPageEnd("AddContactsActivity"); // 保证 onPageEnd 在onPause
        // 之前调用,因为 onPause 中会保存信息
        MobclickAgent.onPause(this);
    }

    public static Handler handler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
                case 0:
                    Bitmap bm = (Bitmap) msg.getData().get("data");
                    AddContactsActivity.image.setImageBitmap(bm);

                    break;
                case 1:

                    Uri uri = Uri.parse((String) msg.getData().get("a"));
                    Cursor cursor = cr.query(uri, null, null, null, null);
                    cursor.moveToFirst();

                    Bitmap bitmap = null;
                    try {
                        bitmap = BitmapFactory
                                .decodeStream(cr.openInputStream(uri));
                    } catch (FileNotFoundException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    AddContactsActivity.image.setImageBitmap(bitmap);
                    break;
                case 2:
                    int pictureId = (Integer) msg.getData().get("picture");
                    AddContactsActivity.image.setImageResource(pictureId);
                    break;
                case 3:
                    // String information = msg.getData().getString("information");
                    if (ContactAddPropertyDialog.somemore != null) {
                        info.add(new AddType(ContactAddPropertyDialog.somemore));
                        b_adapter.notifyDataSetChanged();
                    }
                    Log.i("info", "77777777777789");
                    break;
                default:
                    break;
            }

        }

    };
    private Dialog dialog;

    /*
     * 点击事件
     */
    class ClickAdd implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            // TODO Auto-generated method stub
            switch (v.getId()) {
                case R.id.pic:
                    Intent intent = new Intent(AddContactsActivity.this,
                            HeadPictureActivity.class);
                    startActivityForResult(intent, 1);
                    break;
                case R.id.addMore:
                    dialog();
                    break;
                case R.id.headBackText:
                    info.clear();
                    finish();
                    break;
                case R.id.headShare:
                    // 添加联系人
                    UserContacts uc = getEdit();
                    if (!uc.getName().equals("")) {
                        ContactsUtils.toSaveContactInfo(AddContactsActivity.this,
                                uc);
                        info.clear();
                        ContactsActivity.isUpdate = true;
                        finish();
                    } else {
                        Toast.makeText(AddContactsActivity.this, "请输入联系人姓名", Toast.LENGTH_SHORT).show();
                    }

                    break;
                default:
                    break;
            }
        }

    }

    // 自定义dialog
    protected void dialog() {

        dialog = new ContactAddPropertyDialog(AddContactsActivity.this, R.style.customDialog);
        dialog.setCanceledOnTouchOutside(false);

        dialog.show();
    }

    // 获取文本框信息
    public UserContacts getEdit() {
        UserContacts uc = new UserContacts();
        Drawable head = image.getDrawable();
        uc.setHead(head); // 头像
        uc.setName(phoneNum.getText().toString()); // 姓名

        getAllText();
        uc.setAddress(editAddress);
        uc.setEmail(editEmail);
        editNumber.add(realphoneNum.getText().toString());
        uc.setNumber(editNumber);
        uc.setRemarks(editRemarks);
        uc.setNickName(editNickname);
        uc.setUserIM(editIm);

        return uc;
    }

}
