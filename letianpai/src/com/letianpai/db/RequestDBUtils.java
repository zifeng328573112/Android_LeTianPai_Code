package com.letianpai.db;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class RequestDBUtils {
	// 保存请求记录
		public static void saveChatInformation(RequestInformation chat,
				Context context, String DBName) {
			Myhelper helper = new Myhelper(context,"request.db","d_"+DBName, null, 1);
			SQLiteDatabase db = helper.getWritableDatabase();
			String sql = "create table if not exists " + "d_"+DBName
					+ " (_id integer primary key autoincrement,uname varchar(100),utime varchar(100))";
			db.execSQL(sql);
			ContentValues values = new ContentValues();
			values.put("uname", chat.getuName());
			values.put("utime", chat.getUtime());
			db.insert("d_"+DBName, null, values);
			Log.d("zzzz", "成功保存"+chat.getUtime());
			db.close();
		}

		// 查询请求记录
		public static ArrayList<RequestInformation> selectInformation(Context context, String DBName) {
			ArrayList<RequestInformation> list = new ArrayList<RequestInformation>();
			MyhelperRequest helper = new MyhelperRequest(context,"request.db","d_"+DBName, null, 1);
			SQLiteDatabase db = helper.getWritableDatabase();
			String sql = "create table if not exists " + "d_"+DBName
					+ " (_id integer primary key autoincrement , uname varchar(100), utime varchar(100))";
			db.execSQL(sql);
			Cursor cursor = db.rawQuery("select * from "+"d_"+DBName, null);
			Log.d("zzzz", cursor.getCount()+"");
			while (cursor.moveToNext()) {
				String a = cursor.getString(1); // 获取第一列的值,第一列的索引从0开始
				String b = cursor.getString(2);
				RequestInformation rt = new RequestInformation();
				rt.setuName(a);
				rt.setUtime(b);
				list.add(rt);
			}

			cursor.close();
			db.close();
			return list;
		}
		//删除通知
		public static void deleteNotify(Context context, String DBName){
			MyhelperRequest helper = new MyhelperRequest(context,"request.db","d_"+DBName, null, 1);
			SQLiteDatabase db = helper.getWritableDatabase();
			String sql = "create table if not exists " + "d_"+DBName
					+ " (_id integer primary key autoincrement , uname varchar(100), utime varchar(100))";
			db.execSQL(sql);
			db.execSQL("drop table if exists "+ "d_"+DBName);
			db.close();
		}

}
