package com.letianpai.http.RequestBean;

public class InviteFamilyUserBean {
	private String head;
	private InviteFamily content;
	public String getHead() {
		return head;
	}
	public void setHead(String head) {
		this.head = head;
	}
	public InviteFamily getContent() {
		return content;
	}
	public void setContent(InviteFamily content) {
		this.content = content;
	}
	
}
