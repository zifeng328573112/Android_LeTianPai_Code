package com.letianpai.activity.homeland;

import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.letianpai.R;
import com.letianpai.application.ManageApplication;
import com.letianpai.dialog.wheelview.NumericWheelAdapter;
import com.letianpai.dialog.wheelview.OnWheelChangedListener;
import com.letianpai.dialog.wheelview.WheelView;

public class HomeSetDayActivity extends Activity {
	private TextView btn;
	private Dialog dialog;
	private static int START_YEAR = 1000, END_YEAR = 2014;
	// boolean is_slip=false;
	RequestQueue requestQueue;
	StringBuffer birthday = new StringBuffer();
	String myear;
	String mmonth;
	String mday;
	TextView headShare;
	View headBackText;
	String add = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		ManageApplication.getInstance().addActivity(this);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.homeland_set_day);
		add = getIntent().getStringExtra("add");
		headShare = (TextView) findViewById(R.id.headShare);
		headShare.setText("");
		headBackText = findViewById(R.id.headBackText);
		headBackText.setOnClickListener(new HomeSetDayClick());
		btn = (TextView) findViewById(R.id.btn_datetime_sure);
		requestQueue = Volley.newRequestQueue(this);
		showDateTimePicker();
	}

	private void showDateTimePicker() {
		Calendar calendar = Calendar.getInstance();

		int month = calendar.get(Calendar.MONTH);
		mmonth = calendar.get(Calendar.MONTH) + "";
		int day = calendar.get(Calendar.DATE);
		mday = calendar.get(Calendar.DATE) + "";
		int hour = calendar.get(Calendar.HOUR_OF_DAY);
		int minute = calendar.get(Calendar.MINUTE);
		Log.d("zzzz", day + hour + minute + "");
		// 添加大小月月份并将其转换为list,方便之后的判断
		String[] months_big = { "1", "3", "5", "7", "8", "10", "12" };
		String[] months_little = { "4", "6", "9", "11" };

		final List<String> list_big = Arrays.asList(months_big);
		final List<String> list_little = Arrays.asList(months_little);

		// 月
		final WheelView wv_month = (WheelView) this.findViewById(R.id.month);
		wv_month.setAdapter(new NumericWheelAdapter(1, 12));
		wv_month.setCyclic(true);
		// 添加文字 wv_month.setLabel("月");
		wv_month.setCurrentItem(month);

		// 日
		final WheelView wv_day = (WheelView) this.findViewById(R.id.day);
		wv_day.setCyclic(true);

		// 添加文字 wv_day.setLabel("日");
		wv_day.setCurrentItem(day - 1);

		// 添加"月"监听
		OnWheelChangedListener wheelListener_month = new OnWheelChangedListener() {
			public void onChanged(WheelView wheel, int oldValue, int newValue) {
				int month_num = newValue + 1;
				Log.i("info", month_num + "月");
				mmonth = month_num + "-";
				// 判断大小月及是否闰年,用来确定"日"的数据
				if (list_big.contains(String.valueOf(month_num))) {
					wv_day.setAdapter(new NumericWheelAdapter(1, 31));
				} else if (list_little.contains(String.valueOf(month_num))) {
					wv_day.setAdapter(new NumericWheelAdapter(1, 30));
				} else {
					wv_day.setAdapter(new NumericWheelAdapter(1, 29));
				}
			}
		};
		OnWheelChangedListener wheelListener_day = new OnWheelChangedListener() {

			@Override
			public void onChanged(WheelView wheel, int oldValue, int newValue) {
				int day_num = newValue + 1;
				mday = day_num + "";
			}
		};

		wv_month.addChangingListener(wheelListener_month);
		wv_day.addChangingListener(wheelListener_day);

		// 根据屏幕密度来指定选择器字体的大小
		int textSize = 0;

		textSize = 40;

		wv_day.TEXT_SIZE = textSize;
		wv_month.TEXT_SIZE = textSize;

		// 确定
		btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				// 如果是个数,则显示为"02"的样式
				birthday.append(mmonth);
				birthday.append(mday);
				if ("add".equals(add)) {
					AddAnniversaryActivity.day_home_land.setText(birthday
							.toString());
				} else {
					EditAnniversaryActivity.day_home_land.setText(birthday
							.toString());
				}
				String parten = "00";
				DecimalFormat decimal = new DecimalFormat(parten);
				finish();
			}
		});

	}

	class HomeSetDayClick implements OnClickListener {

		@Override
		public void onClick(View v) {

		}

	}

}
