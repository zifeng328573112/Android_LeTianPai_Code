package com.letianpai.activity.homeland;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.letianpai.R;
import com.letianpai.adapter.homeland.GetRoomAdapter;
import com.letianpai.db.Constant;
import com.letianpai.db.cache.UserCache;
import com.letianpai.http.LStringRequest;
import com.letianpai.http.homeland.BackBean;
import com.letianpai.http.homeland.BackGetRoom;
import com.letianpai.http.homeland.GetRoomBean;
import com.letianpai.http.homeland.GetRoomRequest;
import com.letianpai.utils.JsonUtil;

public class ChangeHomelandActivity extends Activity {
    RequestQueue requestQueue;
    String username;
    static public BackBean[] back;
    GetRoomAdapter gra;
    ListView listview;
    TextView add_button_change;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.change_homeland);
        initView();
    }

    private void initView() {
        add_button_change = (TextView) findViewById(R.id.add_button_change);
        add_button_change.setOnClickListener(new ChangeClick());
        username = getIntent().getStringExtra("username");
        listview = (ListView) findViewById(R.id.listView);
        requestQueue = Volley.newRequestQueue(this);
        new Thread(new Runnable() {

            @Override
            public void run() {
                post();
            }
        }).start();
    }

    // 查询家园列表
    public void post() {
        GetRoomBean userInfo = new GetRoomBean();
        GetRoomRequest user = new GetRoomRequest();
        userInfo.setUsername(username);
        userInfo.setAction("get");
        user.setHead("roomServlet");
        user.setContent(userInfo);
        // msg值
        String msg = JsonUtil.objectToString(user, GetRoomRequest.class);
        String url = Constant.URL + "?msg=" + msg;
        Log.d("zzzz", url);
        LStringRequest request = new LStringRequest(url, new ls(), new el());
        requestQueue.add(request);
    }

    class ls implements Listener<String> {

        @Override
        public void onResponse(String arg0) {
            Log.d("zzzz", arg0);
            BackGetRoom br = (BackGetRoom) JsonUtil.StringToObject(arg0,
                    BackGetRoom.class);
            if (br.getStatus().equals("SUCCESS")) {
                Toast.makeText(ChangeHomelandActivity.this,
                        br.getData()[0].getAddtime(), Toast.LENGTH_SHORT)
                        .show();
                back = br.getData();
                handler.sendEmptyMessage(1);
            }

            if (br.getStatus().equals("FALURE")) {
                Toast.makeText(ChangeHomelandActivity.this, "创建失败",
                        Toast.LENGTH_SHORT).show();
            }

        }

    }

    class el implements ErrorListener {

        @Override
        public void onErrorResponse(VolleyError arg0) {
            Toast.makeText(ChangeHomelandActivity.this, "请求失败",
                    Toast.LENGTH_SHORT).show();
        }

    }

    // handler
    private Handler handler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
                case 1:
                    gra = new GetRoomAdapter(ChangeHomelandActivity.this, back,
                            new ChangeClick());
                    listview.setAdapter(gra);
                    break;

                default:
                    break;
            }
        }
    };

    class ChangeClick implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.checkBox_homeland_getroom:
                    for (int i = 0; i < back.length; i++) {
                        if (i == (Integer) v.getTag()) {
                            back[i].setChecked(!back[i].isChecked());
                            gra.notifyDataSetChanged();
                        } else {
                            back[i].setChecked(false);
                        }
                    }
                    break;
                case R.id.add_button_change:
                    BackBean bb = new BackBean();
                    for (int i = 0; i < back.length; i++) {
                        if (back[i].isChecked() == true) {
                            bb = back[i];
                        }
                    }
                    if (!"".equals(bb.getOfroomid())) {
                        HomelandActivity.name_homeland.setText(bb.getGname());
                        HomelandActivity.number_homeland.setText(bb.getOfroomid());
                        HomelandActivity.signId = bb.getOfroomid();
                        //
                        UserCache.getInstance().modifyUser(ChangeHomelandActivity.this, bb.getOfroomid());
                    }
                    finish();
                    break;
                default:
                    break;
            }
        }

    }
}
