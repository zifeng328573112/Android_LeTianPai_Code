package com.letianpai.http.homeland;

public class GetRoomRequest {
	private String head;
	private GetRoomBean content;
	public String getHead() {
		return head;
	}
	public void setHead(String head) {
		this.head = head;
	}
	public GetRoomBean getContent() {
		return content;
	}
	public void setContent(GetRoomBean content) {
		this.content = content;
	}
	
}
