package com.letianpai.http.homeland;

public class BackAnniversary {
	private String status;
	private AnniverListBean[] data;
	private String message;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public AnniverListBean[] getData() {
		return data;
	}

	public void setData(AnniverListBean[] data) {
		this.data = data;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
