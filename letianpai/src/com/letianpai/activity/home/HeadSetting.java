package com.letianpai.activity.home;

import java.io.FileNotFoundException;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.letianpai.R;
import com.letianpai.activity.HeadPictureActivity3;
import com.letianpai.application.ManageApplication;
import com.letianpai.utils.ChatUtils;
import com.letianpai.utils.ImageTools;

public class HeadSetting extends Activity {
    Button btn;
    View headBackText;
    static ImageView image;
    static ContentResolver cr;
    static Bitmap bitmap;
    TextView headShare;
    ProgressDialog pd;
    public static Handler handler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
                case 0:
                    //
                    Log.i("info", "Па»ъ");
                    bitmap = (Bitmap) msg.getData().get("data");

                    image.setImageBitmap(bitmap);

                    break;
                case 1:
                    Log.i("info", "ПаІб");
                    Uri uri = Uri.parse((String) msg.getData().get("a"));
                    Cursor cursor = cr.query(uri, null, null, null, null);
                    cursor.moveToFirst();
                    try {
                        bitmap = BitmapFactory
                                .decodeStream(cr.openInputStream(uri));
                    } catch (FileNotFoundException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    image.setImageBitmap(bitmap);
                    break;
                case 2:
                    int pictureId = (Integer) msg.getData().get("picture");
                    image.setImageResource(pictureId);
                    Drawable d = image.getResources().getDrawable(pictureId);
                    bitmap = ImageTools.drawableToBitmap(d);
                    Log.i("info", bitmap + "");
                    break;
                default:
                    break;
            }

        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        ManageApplication.getInstance().addActivity(this);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.home_registe_image);
        headBackText = findViewById(R.id.headBackText);
        headBackText.setOnClickListener(new HeadClick());
        btn = (Button) findViewById(R.id.bt_next);
        image = (ImageView) findViewById(R.id.regist_img);
        cr = this.getContentResolver();
        headShare = (TextView) findViewById(R.id.headShare);
        headShare.setText("");
        btn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                ChatUtils.changeImage(bitmap);
                Intent intent = new Intent(HeadSetting.this, SexSetting.class);
                startActivity(intent);
            }
        });
        image.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Intent intent = new Intent(HeadSetting.this,
                        HeadPictureActivity3.class);
                startActivity(intent);
            }
        });
    }
    class HeadClick implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.headBackText:
                    finish();
                    break;

                default:
                    break;
            }
        }

    }
}
