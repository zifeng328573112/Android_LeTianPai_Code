package com.letianpai.activity;

import java.io.File;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Message;
import android.provider.MediaStore;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

import com.letianpai.R;
import com.letianpai.activity.contacts.AddContactsActivity;
import com.letianpai.adapter.HeadPictureAdapter;
import com.umeng.analytics.MobclickAgent;

public class HeadPictureActivity extends Activity{
	View headBackText;
	Bitmap bm;
	HeadPictureAdapter hp;
	GridView gv;
	private static final String PHOTO_FILE_NAME = "temp_photo.jpg";
	private File tempFile;
	View camara_t,pictures_p;
	int[] picture = { 
			R.drawable.a_001, R.drawable.a_002, R.drawable.a_003,
			R.drawable.a_004, R.drawable.a_005, R.drawable.a_007,
			 R.drawable.a_008, R.drawable.a_009, R.drawable.a_010, R.drawable.a_011};
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.headpicture);
		initView();
	    //
	}

	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("SplashScreen"); // 统计页面
		MobclickAgent.onResume(this); // 统计时长
	}

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd("SplashScreen"); // 保证 onPageEnd 在onPause
		MobclickAgent.onPause(this);
	}

	public void initView() {
		gv = (GridView) findViewById(R.id.listView);
		hp = new HeadPictureAdapter(this, picture);
		gv.setAdapter(hp);
		gv.setOnItemClickListener(new HeadItemClick());
		headBackText = findViewById(R.id.headBackText);
		headBackText.setOnClickListener(new headClick());
		
		camara_t=findViewById(R.id.relativeLayout1);
		pictures_p=findViewById(R.id.relativeLayout2);
		
		camara_t.setOnClickListener(new headClick());
		pictures_p.setOnClickListener(new headClick());
	}

	/*
	 * 点击
	 */
	class headClick implements View.OnClickListener {

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.headBackText:
				finish();
				break;
			case R.id.relativeLayout1:
				camera();
				break;
			case R.id.relativeLayout2:
				Intent i = new Intent(
						Intent.ACTION_PICK,
						android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
				startActivityForResult(i, 1);
				break;
			default:
				break;
			}
		}

	}

	class HeadItemClick implements OnItemClickListener {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
		
		
				Bundle bundle = new Bundle();
				bundle.putInt("picture", picture[position]);
				Message msg = new Message();
				msg.setData(bundle);
				msg.what = 2;
				AddContactsActivity.handler.sendMessage(msg);
				finish();
		}

	}

	/*
	 * 获取图片
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// 照相机
		if (resultCode == Activity.RESULT_OK && requestCode == 2) {
			if (hasSdcard()) {

				crop(Uri.fromFile(tempFile));
			}

		}
		// 系统图库
		if (requestCode == 1 && resultCode == RESULT_OK && null != data) {
			Uri uri = data.getData();
			crop(uri);

		}
		if (requestCode == 3) {
			if (data != null) {
				Bitmap bitmap = data.getParcelableExtra("data");
				Bundle bundle = data.getExtras();
				Message msg = new Message();
				msg.setData(bundle);

				msg.what = 0;
				AddContactsActivity.handler.sendMessage(msg);
				finish();
			}
			try {
				// 将临时文件删除
				tempFile.delete();
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	/*
	 * 剪切图片
	 */
	private void crop(Uri uri) {
		// 裁剪图片意图
		Intent intent = new Intent("com.android.camera.action.CROP");
		intent.setDataAndType(uri, "image/*");
		intent.putExtra("crop", "true");
		// 裁剪框的比例，1：1
		intent.putExtra("aspectX", 1);
		intent.putExtra("aspectY", 1);
		// 裁剪后输出图片的尺寸大小
		intent.putExtra("outputX", 250);
		intent.putExtra("outputY", 250);

		intent.putExtra("outputFormat", "JPEG");// 图片格式
		intent.putExtra("noFaceDetection", true);// 取消人脸识别
		intent.putExtra("return-data", true);
		// 开启一个带有返回值的Activity，请求码为PHOTO_REQUEST_CUT
		startActivityForResult(intent, 3);
	}

	/*
	 * 从相册获取
	 */
	public void gallery() {
		// 激活系统图库，选择一张图片
		Intent intent = new Intent(Intent.ACTION_PICK);
		intent.setType("image/*");
		// 开启一个带有返回值的Activity，请求码为PHOTO_REQUEST_GALLERY
		startActivityForResult(intent, 1);
	}

	/*
	 * 从相机获取
	 */
	public void camera() {
		// 激活相机
		Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
		// 判断存储卡是否可以用，可用进行存储
		if (hasSdcard()) {
			tempFile = new File(Environment.getExternalStorageDirectory(),
					PHOTO_FILE_NAME);
			// 从文件中创建uri
			Uri uri = Uri.fromFile(tempFile);
			intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
		}
		// 开启一个带有返回值的Activity，请求码为PHOTO_REQUEST_CAREMA
		startActivityForResult(intent, 2);
	}

	/*
	 * 判断sdcard是否被挂载
	 */
	private boolean hasSdcard() {
		if (Environment.getExternalStorageState().equals(
				Environment.MEDIA_MOUNTED)) {
			return true;
		} else {
			return false;
		}
	}
}
