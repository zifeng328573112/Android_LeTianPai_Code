package com.letianpai.adapter.contacts;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.SectionIndexer;
import android.widget.TextView;
import com.letianpai.R;
import com.letianpai.bean.UserContacts;

import java.util.ArrayList;
import java.util.List;

/*
 * 联系人适配器/*
 * 改
 */

public class ContactsAdapter extends BaseAdapter implements SectionIndexer {
    private Context context;
    private LayoutInflater inflater;
    List<UserContacts> arraylist = new ArrayList<UserContacts>();

    String[] seekContacts = {"添加联系人", "查找联系人"};
    int[] seekImage = {R.drawable.add_icon_90, R.drawable.seek_icon_90};

    public ContactsAdapter(Context context, List<UserContacts> arraylist) {
        super();
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.arraylist = arraylist;
    }

    public List<UserContacts> getArraylist() {
        return arraylist;
    }

    public void setArraylist(List<UserContacts> arraylist) {
        this.arraylist = arraylist;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return arraylist.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return arraylist.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {

        View layout = inflater.inflate(R.layout.contactsitem, parent, false);
        View layout1 = inflater.inflate(R.layout.contactsitem_right, parent,
                false);
//		View layoutAddSeek = inflater.inflate(R.layout.seek_add_contacts,
//				parent, false);
        TextView name = (TextView) layout.findViewById(R.id.name);
        TextView number = (TextView) layout.findViewById(R.id.number);
        ImageView image = (ImageView) layout.findViewById(R.id.name1);
        ImageView pic = (ImageView) layout.findViewById(R.id.pic);
        TextView place = (TextView) layout.findViewById(R.id.place);

        TextView name1 = (TextView) layout1.findViewById(R.id.name);
        TextView number1 = (TextView) layout1.findViewById(R.id.number);
        TextView place1 = (TextView) layout1.findViewById(R.id.place);
        ImageView image1 = (ImageView) layout1.findViewById(R.id.name1);

        ImageView pic1 = (ImageView) layout1.findViewById(R.id.pic);

//		ImageView name2 = (ImageView) layoutAddSeek.findViewById(R.id.name1);
//		TextView pic2 = (TextView) layoutAddSeek.findViewById(R.id.number);
//
//		if (position == 0) {
//
//			return layoutAddSeek;
//		}

//		if (position == 1) {
//			pic2.setText("查找联系人");
//			name2.setImageResource(seekImage[1]);
//			return layoutAddSeek;
//		}


        arraylist.get(position).setSort_number(position + "");


        if (position % 2 == 0) {
            name.setText(arraylist.get(position).getName());
            if (null != arraylist.get(position).getPicture()) {
                Bitmap bitmap = arraylist.get(position).getPicture();
                pic.setImageBitmap(bitmap);
            }
            place.setText(arraylist.get(position).getPlace());
            number.setText(arraylist.get(position).getNumber().get(0));
            return layout;
        }

        if (position % 2 == 1) {
            name1.setText(arraylist.get(position).getName());
            if (null != arraylist.get(position).getPicture()) {
                pic1.setImageBitmap(arraylist.get(position).getPicture());
            }
            if (null != arraylist.get(position).getNumber()) {
                number1.setText(arraylist.get(position).getNumber().get(0));
                place1.setText(arraylist.get(position).getPlace());
            }
            return layout1;
        }
        return layout;
    }

    final static class ViewHolder {
        TextView name;
        TextView number;
        ImageView image;
        ImageView pic;

        TextView name1;
        TextView number1;
        ImageView image1;
        ImageView pic1;

        ImageView name2;
        TextView pic2;
    }


    /**
     * 根据ListView的当前位置获取分类的首字母的Char ascii值
     */
    @Override
    public int getSectionForPosition(int position) {
        // TODO Auto-generated method stub
        return arraylist.get(position).getSortLetters().charAt(0);
    }


    /**
     * 根据分类的首字母的Char ascii值获取其第一次出现该首字母的位置
     */
    @Override
    public int getPositionForSection(int section) {
        // TODO Auto-generated method stub
        for (int i = 0; i < arraylist.size(); i++) {
            String sortStr = arraylist.get(i).getSortLetters();
            if (sortStr != null) {
                char firstChar = sortStr.toUpperCase().charAt(0);
                if (firstChar == section) {
                    return i;
                }
            }

        }

        return -1;
    }

    /**
     * 提取英文的首字母，非英文字母用#代替。
     *
     * @param str
     * @return
     */
    private String getAlpha(String str) {
        String sortStr = str.trim().substring(0, 1).toUpperCase();
        // 正则表达式，判断首字母是否是英文字母
        if (sortStr.matches("[A-Z]")) {
            return sortStr;
        } else {
            return "#";
        }
    }

    @Override
    public Object[] getSections() {
        // TODO Auto-generated method stub
        return null;
    }
}
