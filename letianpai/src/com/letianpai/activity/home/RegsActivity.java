package com.letianpai.activity.home;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.letianpai.R;
import com.letianpai.activity.LoginActivity;
import com.letianpai.application.ManageApplication;
import com.letianpai.bean.BackResponse;
import com.letianpai.bean.UserBeen;
import com.letianpai.bean.UserInfo;
import com.letianpai.db.Constant;
import com.letianpai.utils.JsonUtil;

public class RegsActivity extends Activity {
    View ll_up, nextstep;
    EditText number;
    static String phonenumber;
    RequestQueue requestQueue;
    View headBackText;
    TextView headShare, headText;
    ProgressDialog pd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ManageApplication.getInstance().addActivity(this);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.home_reg);
        ll_up = findViewById(R.id.ll_up);
        number = (EditText) findViewById(R.id.number);
        nextstep = findViewById(R.id.nextstep);
        ll_up.setVisibility(View.GONE);
        headBackText = findViewById(R.id.headBackText);
        headBackText.setOnClickListener(new registerClick());
        headShare = (TextView) findViewById(R.id.headShare);
        headShare.setVisibility(View.INVISIBLE);
        headText = (TextView) findViewById(R.id.headText);
        headText.setText("注册");

        requestQueue = Volley.newRequestQueue(this);

        nextstep.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
            	 pd = new ProgressDialog(RegsActivity.this);
                 pd.setTitle("请稍等");
                 pd.show();
                 pd.setCanceledOnTouchOutside(false);
                phonenumber = number.getText().toString();
                boolean send = isAccord(phonenumber);
                if (phonenumber.equals("")) {
                    Toast.makeText(RegsActivity.this, "手机号码不能为空",
                            Toast.LENGTH_SHORT).show();
                } else if (send == true) {
                    new Thread(new Runnable() {

                        @Override
                        public void run() {
                            post(phonenumber);
                        }
                    }).start();

                } else {
                    Toast.makeText(RegsActivity.this, "请输入正确的手机号码",
                            Toast.LENGTH_SHORT).show();
                }

            }

        });

    }

    // 手机号码验证
    public boolean isAccord(String number) {

        Pattern p = null;
        Matcher m = null;
        boolean b = false;
        p = Pattern.compile("^[1][3,4,5,8,7][0-9]{9}$"); // 验证手机号
        m = p.matcher(number);
        b = m.matches();
        return b;
    }

    // 调用服务器接口发送验证码

    class ls implements Listener<String> {

        @Override
        public void onResponse(String arg0) {
        	pd.cancel();
            BackResponse br = (BackResponse) JsonUtil.StringToObject(arg0,
                    BackResponse.class);
            Toast.makeText(RegsActivity.this, br.getMessage(),
                    Toast.LENGTH_SHORT).show();
            Log.d("zzzz",
                    br.getData().getScalar() + br.getMessage() + br.getStatus());
            if (br.getStatus().equals("SUCCESS")) {
                Intent intent = new Intent(RegsActivity.this,
                        SendmsgActivity.class);
                // 这个号码为注册账号传递给后面的设置
                intent.putExtra("number", phonenumber);
                Log.d("zzzz", br.getMessage() + "信息");
                startActivity(intent);
            }

        }

    }

    class el implements ErrorListener {

        @Override
        public void onErrorResponse(VolleyError arg0) {
        	pd.cancel();
            Toast.makeText(RegsActivity.this, "请求失败", Toast.LENGTH_SHORT)
                    .show();
        }

    }

    public void post(String username) {
        Map<String, String> map = new HashMap<String, String>();
        UserInfo userInfo = new UserInfo();
        userInfo.setUsername(username);
        userInfo.setAction("step1");
        UserBeen user = new UserBeen();
        user.setHead("registerServlet");
        user.setContent(userInfo);
        // msg值
        String msg = JsonUtil.objectToString(user, UserBeen.class);
        String url = Constant.URL + "?msg=" + msg;
        Log.d("zzzz", url);
        map.put("msg", msg);
        StringRequest request = new StringRequest(url, new ls(), new el());
        requestQueue.add(request);
    }

    class registerClick implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.headBackText:
                    finish();
                    break;

                default:
                    break;
            }
        }

    }
}
