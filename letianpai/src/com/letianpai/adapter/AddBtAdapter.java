package com.letianpai.adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.letianpai.R;
import com.letianpai.bean.AddType;

public class AddBtAdapter extends BaseAdapter{
    List<AddType>info;
    private Context context;
    private LayoutInflater inflater;

    public AddBtAdapter(Context context,List<AddType>info){
        this.context=context;
        this.info=info;
        inflater=LayoutInflater.from(context);
    }
    @Override
    public int getCount() {
        return info.size();
    }

    @Override
    public Object getItem(int arg0) {
        return info.get(arg0);
    }

    @Override
    public long getItemId(int arg0) {
        return arg0;
    }

    @Override
    public View getView(final int position, View view, ViewGroup arg2) {
        view = inflater.inflate(R.layout.add_item,null);
        TextView genre = (TextView) view.findViewById(R.id.genre);
        EditText edit = (EditText) view.findViewById(R.id.phoneNum);
        ImageView delete=(ImageView) view.findViewById(R.id.delete);

        edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count,int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                //将editText中改变的值设置的HashMap中
                //edit.setTag(s.toString());
                //hashMap.put(position, s.toString());
                info.get(position).setValue(s.toString());
            }

        });

        //如果hashMap不为空，就设置的editText
       /*  if(hashMap.get(position) != null){  
             edit.setText(hashMap.get(position));  
         }*/

        delete.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                info.remove(position);
                AddBtAdapter.this.notifyDataSetChanged();
            }
        });
        AddType type = info.get(position);
        genre.setText(type.getType());
        edit.setText(type.getValue());
        return view;
    }

}
