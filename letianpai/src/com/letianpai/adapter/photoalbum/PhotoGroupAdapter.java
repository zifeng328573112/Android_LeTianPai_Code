package com.letianpai.adapter.photoalbum;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.letianpai.R;
import com.letianpai.common.picker.noscrollgridview.NoScrollGridView;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 14-9-27
 * Time: 下午2:26
 * To change this template use File | Settings | File Templates.
 */
public class PhotoGroupAdapter extends BaseAdapter {
    private Context context;
    private List<PhotoGroupInfo> groupInfoList;
    private LayoutInflater layoutInflater;

    public PhotoGroupAdapter(Context context, List<PhotoGroupInfo> groupInfoList) {
        this.context = context;
        this.groupInfoList = groupInfoList;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        if (null == groupInfoList) {
            return 0;
        }
        return groupInfoList.size();
    }


    @Override
    public Object getItem(int position) {
        return groupInfoList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        ViewHolder holder = null;
        if (view == null) {
            holder = new ViewHolder();
            view = layoutInflater.inflate(R.layout.local_album_photo_group_item, null);
            holder.groupTitleTv = (TextView) view.findViewById(R.id.local_album_group_title_tv);
            holder.photoGv = (NoScrollGridView) view.findViewById(R.id.local_album_group_photo_gv);
            holder.photoGv.setOnFocusChangeListener(null);
            holder.photoGv.setOnHierarchyChangeListener(null);

            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        PhotoGroupInfo bean = groupInfoList.get(position);
        holder.groupTitleTv.setText(bean.getDate());
        PhotoMemberAdapter memberAdapter = new PhotoMemberAdapter(context, bean.getPhotoInfoList());
        holder.photoGv.setAdapter(memberAdapter);
        return view;
    }

    private class ViewHolder {
        TextView groupTitleTv;
        NoScrollGridView photoGv;
    }
}
