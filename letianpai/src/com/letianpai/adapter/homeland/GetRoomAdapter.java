package com.letianpai.adapter.homeland;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.letianpai.R;
import com.letianpai.activity.homeland.ChangeHomelandActivity;
import com.letianpai.http.homeland.BackBean;

public class GetRoomAdapter extends BaseAdapter{
	private Context context;
	private LayoutInflater inflater;
	BackBean[] back;
	View.OnClickListener clicker;
	public void setArray(BackBean[] back) {
		this.back = back;
	}
	
	public GetRoomAdapter(Context context, BackBean[] back,View.OnClickListener clicker) {
		super();
		this.context = context;
		inflater = LayoutInflater.from(context);
		this.back = back;
		this.clicker = clicker;
	}
	@Override
	public int getCount() {
		return back.length;
	}

	@Override
	public Object getItem(int position) {
		return back[position];
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		view = inflater.inflate(R.layout.get_homeland_item,parent,false);
		TextView num_homeland = (TextView) view.findViewById(R.id.num_homeland);
		num_homeland.setText(back[position].getOfroomid());
		Log.d("zzzz", back[position].getOfroomid());
		TextView title_homeland = (TextView) view.findViewById(R.id.title_homeland);
		title_homeland.setText(back[position].getGname());
		CheckBox checkBox_homeland_getroom = (CheckBox) view.findViewById(R.id.checkBox_homeland_getroom);
		checkBox_homeland_getroom.setOnClickListener(clicker);
		checkBox_homeland_getroom.setTag(position);
		if(ChangeHomelandActivity.back[position].isChecked() == false){
			checkBox_homeland_getroom.setChecked(false);
			Log.d("zzzz",ChangeHomelandActivity.back[position].isChecked()+"");
		}else if(ChangeHomelandActivity.back[position].isChecked() == true){
			checkBox_homeland_getroom.setChecked(true);
		}
		return view;
	}
		
}
