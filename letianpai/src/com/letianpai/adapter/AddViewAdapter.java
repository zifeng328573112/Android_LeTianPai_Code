package com.letianpai.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.letianpai.R;

public class AddViewAdapter extends BaseAdapter {
	private Context context;
	private LayoutInflater inflater;
	private ArrayList<String> arraylist;

	public void setArraylist(ArrayList<String> arraylist) {
		this.arraylist = arraylist;
	}

	public AddViewAdapter(Context context, ArrayList<String> arraylist) {
		super();
		this.context = context;
		inflater = LayoutInflater.from(context);
		this.arraylist = arraylist;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return arraylist.size()+2;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return arraylist.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		if (position == 0) {
			view = inflater.inflate(R.layout.contact_add_head, parent, false);
		} else {
			view = inflater.inflate(R.layout.add_item, parent, false);
		}
		TextView tv = (TextView) view.findViewById(R.id.genre);
		if (!(position == 0) && position != arraylist.size()+1) {
			Log.d("zzzz", position+"");
	
			tv.setText(arraylist.get(position-1));
		}
		if (position == arraylist.size()+1) {
			view = inflater.inflate(R.layout.contact_add_foot, parent, false);
		}
		return view;
	}

}
