package com.letianpai.db.cache;

/**
 * Created by Administrator on 2014/10/8.
 */

import android.content.Context;
import android.content.SharedPreferences;
import com.letianpai.common.server.Constants;
import com.letianpai.db.bean.UserInfo;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.SoftReference;
import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 13-9-14
 * Time: 上午9:40
 * To change this template use File | Settings | File Templates.
 */
public class UserCache {

    private static UserCache cache;
    private HashMap<String, UserReference> userReferences;  //用于cache内容的存储
    private ReferenceQueue<UserInfo> userQueue;//垃圾reference的队列

    //集成softreference 使得每一个实例都具有可识别的标示
    //并且该标示与其在hashmap内的key一致
    private class UserReference extends SoftReference<UserInfo> {
        private String _key = "";

        public UserReference(UserInfo user, ReferenceQueue<UserInfo> userQueue) {
            super(user, userQueue);
            _key = user.getUsername();
        }
    }

    //构建缓冲器实例
    private UserCache() {
        userReferences = new HashMap<String, UserReference>();
        userQueue = new ReferenceQueue<UserInfo>();
    }

    //取得缓冲器实例
    public static UserCache getInstance() {
        if (cache == null) {
            cache = new UserCache();
        }
        return cache;
    }

    //以软引用的方式对一个Employee对象的实例进行引用保存该引用
    private void cacheUser(UserInfo user, String logoId) {
        clearCache();//清除垃圾引用
        UserReference ref = new UserReference(user, userQueue);
        userReferences.put(logoId, ref);
    }

    public void addUser(Context context, UserInfo userInfo) {
        SharedPreferences sharePre = context.getSharedPreferences(Constants.USER_CACHE, 0);
        SharedPreferences.Editor editor = sharePre.edit();
        editor.putString("userId", userInfo.getUsername());
        editor.putString("username", userInfo.getUsername());
        editor.putString("nickname", userInfo.getNickname());
        editor.putString("password", userInfo.getPassword());
        editor.putString("sex", userInfo.getSex());
        editor.putString("email", userInfo.getEmail());
        editor.putBoolean("islogin", userInfo.isLoginType());
        editor.commit();
    }

    //进入家园设置
    public void modifyUser(Context context, String roomId) {
        UserInfo userInfo = getUser(context);
        SharedPreferences sharePre = context.getSharedPreferences(Constants.USER_CACHE, 0);
        SharedPreferences.Editor editor = sharePre.edit();
        editor.putString("userId", userInfo.getUsername());
        editor.putString("username", userInfo.getUsername());
        editor.putString("nickname", userInfo.getNickname());
        editor.putString("password", userInfo.getPassword());
        editor.putString("sex", userInfo.getSex());
        editor.putString("email", userInfo.getEmail());
        editor.putString("roomId", roomId);
        editor.putBoolean("islogin", userInfo.isLoginType());
        editor.commit();
    }

    //进入家园设置
    public void modifyLogin(Context context, boolean loginType) {
        UserInfo userInfo = getUser(context);
        SharedPreferences sharePre = context.getSharedPreferences(Constants.USER_CACHE, 0);
        SharedPreferences.Editor editor = sharePre.edit();
        editor.putString("userId", userInfo.getUsername());
        editor.putString("username", userInfo.getUsername());
        editor.putString("nickname", userInfo.getNickname());
        editor.putString("password", userInfo.getPassword());
        editor.putString("sex", userInfo.getSex());
        editor.putString("email", userInfo.getEmail());
        editor.putString("roomId", userInfo.getRoomId());
        editor.putBoolean("islogin", loginType);
        editor.commit();
    }

    //根据所指定的标示，重新获取相应user对象的实例
    public UserInfo getUser(Context context) {
        UserInfo user = null;
        //缓存中是否存在该user实例的软引用，若有，从软引用中取得
        if (userReferences.containsKey(Constants.USER_CACHE)) {
            UserReference ref = (UserReference) userReferences.get(Constants.USER_CACHE);
            user = (UserInfo) ref.get();
        }
        //若没有软引用，或者从软引用中取得的实例是null，重新构建一个实例
        if (user == null) {
            SharedPreferences sharePre = context.getSharedPreferences(Constants.USER_CACHE, 0);
            user = new UserInfo();
            user.setUserId(sharePre.getString("userId", ""));
            user.setUsername(sharePre.getString("username", ""));
            user.setPassword(sharePre.getString("password", ""));
            user.setNickname(sharePre.getString("nickname", ""));
            user.setSex(sharePre.getString("sex", ""));
            user.setEmail(sharePre.getString("email", ""));
            user.setHeadPhoto(sharePre.getString("headPhoto", ""));
            user.setRoomId(sharePre.getString("roomId", ""));
            user.setLoginType(sharePre.getBoolean("islogin", false));
//            UserDao userDao = new UserDao(context);
//            user = userDao.getUserByDefaultLogin(false);
            this.cacheUser(user, Constants.USER_CACHE);
        }
        return user;
    }

    //清除所引用的user对象，已经被回收的UserReference对象
    public void clearCache() {
        UserReference ref = null;
        while ((ref = (UserReference) userQueue.poll()) != null) {
            userReferences.remove(ref._key);
        }
    }

}

