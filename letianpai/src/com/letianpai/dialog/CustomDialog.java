package com.letianpai.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.letianpai.R;

public class CustomDialog{
	private Button cancel;
	private Button ensure;
	public static EditText editview;
	private Dialog mDialog;
	View dialogView;
	private LayoutInflater mInflater;
	private int width;
	public CustomDialog(Context context,int layout) {
		mDialog = new Dialog(context,R.style.customDialog);		//�?��无title样式
		mInflater = LayoutInflater.from(context);
		
		dialogView = mInflater.inflate(layout, null);
		
		Window window = mDialog.getWindow();
		window.setContentView(dialogView);
		
		WindowManager wm = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);
		Display display=wm.getDefaultDisplay();
		setDialogAttributes(mDialog, display, 0.95);
		
		findView(window);
	}
	
	public static void setDialogAttributes(Dialog dialog, Display d,double widthProportion) {
		Window dialogWindow = dialog.getWindow();
		WindowManager.LayoutParams p = dialogWindow.getAttributes(); // ��ȡ�Ի���ǰ�Ĳ���ֵ
		p.width = (int) (d.getWidth() *widthProportion); // ��������Ϊ��Ļ��0.65
	
		dialogWindow.setAttributes(p);
	}

	private void findView(Window window) {
		cancel = (Button)window.findViewById(R.id.custom_dialog_cancel);
		ensure = (Button)window.findViewById(R.id.custom_dialog_ensure);
		editview=(EditText) window.findViewById(R.id.setsome);
	}
	/**
	 * 左边按钮点击事件
	 * @param listener
	 */
	public void setLeftOnClick(View.OnClickListener listener){
		ensure.setOnClickListener(listener);
	}
	/**
	 * 右边按钮点击事件
	 * @param listener
	 */
	public void setRightOnClick(View.OnClickListener listener){
		cancel.setOnClickListener(listener);
	}




	
	public void show(){
		mDialog.show();
	}
	
	public void dismiss(){
		mDialog.dismiss();
	}

	public View getCustomView() {
		// TODO Auto-generated method stub
		return dialogView;
	}


}
