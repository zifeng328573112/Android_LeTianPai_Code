package com.letianpai.http.RequestBean;

public class SetPasswordUserBean {
	private String head;
	private SetPassword content;
	public String getHead() {
		return head;
	}
	public void setHead(String head) {
		this.head = head;
	}
	public SetPassword getContent() {
		return content;
	}
	public void setContent(SetPassword content) {
		this.content = content;
	}
	
}
