package com.letianpai.http;

import java.util.HashMap;
import java.util.Map;

import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.StringRequest;
import com.letianpai.bean.UserBeen;
import com.letianpai.bean.UserInfo;
import com.letianpai.utils.JsonUtil;

public class RequestUtil {
	static RequestQueue requestQueue;

	private final static String MEMBERDATA_URL = "http://dispatch.letianpai.com/accept/api";

	public void post(Listener<String> s,ErrorListener m) {

		Map<String, String> map = new HashMap<String, String>();
		UserInfo userInfo = new UserInfo();
		userInfo.setUsername("18601771888");
		UserBeen user = new UserBeen();
		user.setHead("registerServlet");
		user.setContent(userInfo);
		// msgֵ
		String msg = JsonUtil.objectToString(user, UserBeen.class);
		String url = MEMBERDATA_URL + "?msg=" + msg;
		map.put("msg", msg);
		StringRequest request = new StringRequest(url, s, m);
		requestQueue.add(request);

	}
}
