package com.letianpai.popupwindow;

import android.content.Context;
import android.view.MotionEvent;
import android.view.View;
import android.widget.PopupWindow;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 14-9-26
 * Time: 下午2:47
 * 照片获取
 */
public class GainPicturePopupWindow extends PopupWindow {
    public GainPicturePopupWindow(Context context) {
        super(context);
    }

    public GainPicturePopupWindow(View contentView) {
        super(contentView);
    }

    class PopWinOnTouchListener implements View.OnTouchListener {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (isShowing()) {
                dismiss();
            }
            return false;
        }
    }
}
