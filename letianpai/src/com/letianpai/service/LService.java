package com.letianpai.service;

import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.packet.Packet;

import android.app.PendingIntent;
import android.util.Log;

import com.letianpai.utils.XmppTool;

public class LService {

	public static final String PONG_TIMEOUT = "pong timeout";// 连接超时
	public static final String NETWORK_ERROR = "network error";// 网络错误

	// ping-pong服务器
	private String mPingID;// ping服务器的id
	private PacketListener mPongListener;// ping pong服务器动态监听
	private long mPingTimestamp;
	private PendingIntent mPongTimeoutAlarmPendIntent;// 判断服务器连接超时的闹钟

	//处理Ping消息
	private void registerPongListener() {
		mPingID = null;// 初始化ping的id

		if (mPongListener != null)
			XmppTool.con.removePacketListener(mPongListener);// 先移除之前监听对象
		mPongListener = new PacketListener(){

			@Override
			public void processPacket(Packet packet) {
				if (packet == null)
					return;
				if (packet.getPacketID().equals(mPingID)) {// 如果服务器返回的消息为ping服务器时的消息，说明没有掉线
					Log.i(String.format(
							"Ping: server latency %1.3fs",
							(System.currentTimeMillis() - mPingTimestamp) / 1000.)+"", mPingID);
					mPingID = null;
				}
			}
			
		};
	}
	
}
