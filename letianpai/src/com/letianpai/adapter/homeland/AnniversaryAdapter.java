package com.letianpai.adapter.homeland;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.letianpai.R;
import com.letianpai.bean.UserCallLog;
import com.letianpai.bean.homeland.AnniversaryBean;

public class AnniversaryAdapter extends BaseAdapter {
	private Context context;
	private LayoutInflater inflater;
	List<AnniversaryBean> arraylist = new ArrayList<AnniversaryBean>();
	View.OnClickListener cliker;

	public void setArraylist(List<AnniversaryBean> arraylist) {
		this.arraylist = arraylist;
	}

	public AnniversaryAdapter(Context context, List<AnniversaryBean> arraylist,View.OnClickListener cliker) {
		super();
		this.context = context;
		inflater = LayoutInflater.from(context);
		this.arraylist = arraylist;
		this.cliker = cliker;

	}

	@Override
	public int getCount() {
		return arraylist.size() + 1;
	}

	@Override
	public Object getItem(int position) {
		return arraylist.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		if (position == 0) {
			view = inflater.inflate(R.layout.anniversary_head, parent, false);
		} else {
			view = inflater.inflate(R.layout.anniversary_item, parent, false);
			View homeland_delete = view.findViewById(R.id.homeland_delete);
			homeland_delete.setOnClickListener(cliker);
			homeland_delete.setTag(position-1);
			//标题
			TextView home_land_title = (TextView) view.findViewById(R.id.home_land_title);
			home_land_title.setOnClickListener(cliker);
			home_land_title.setTag(position-1);
			home_land_title.setText(arraylist.get(position-1).getTitle());
			//时间
			TextView homeland_time = (TextView) view.findViewById(R.id.homeland_time);
			homeland_time.setText(arraylist.get(position-1).getPeriod());
			//提醒
			TextView day_remind = (TextView) view.findViewById(R.id.day_remind);
			day_remind.setText(arraylist.get(position-1).getAdvancedays());
			//备注
			TextView homeland_note = (TextView) view.findViewById(R.id.homeland_note);
			homeland_note.setText(arraylist.get(position-1).getNote());
		} 
		return view;
	}

}
