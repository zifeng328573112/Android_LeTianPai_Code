package com.letianpai.common.tool;

import java.io.*;
import java.util.Properties;

/**
 * Created by IntelliJ IDEA.
 * User: Chris.F
 * Date: 2010-9-21
 * Time: 18:33:16
 */
public class PropertiesUtil {
    private String propFilePath;
    private Properties prop;

    public PropertiesUtil(String propFilePath) {
        this.propFilePath = propFilePath;
        loadProperties();
    }

    private void loadProperties() {
        prop = new Properties();
        try {
            prop.load(new FileReader(propFilePath));
        } catch (IOException e) {
        }
    }

    public String get(String key) {
        return prop == null ? null : prop.getProperty(key);
    }

    public void set(String key, String value) {
        if (prop == null)
            return;
        prop.setProperty(key, value);
    }

    public boolean persist() {
        if (prop == null)
            return false;
        Writer writer = null;
        try {
            File f = new File(propFilePath);
            if (!f.exists())
                f.createNewFile();
            writer = new FileWriter(f);
            prop.store(writer, "");
            writer.flush();
            return true;
        } catch (IOException e) {
            return false;
        } finally {
            IOUtil.closeWriter(writer);
        }
    }
}
