package com.letianpai.adapter;

import u.aly.dm;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.letianpai.R;
import com.letianpai.utils.Utils;

public class HeadPictureAdapter extends BaseAdapter {
	private Context context;
	private LayoutInflater inflater;
	
	int[] picture;

	public HeadPictureAdapter(Context context, int[] picture) {
		super();
		this.context = context;
		inflater = LayoutInflater.from(context);
		this.picture = picture;
	}

	@Override
	public int getCount() {
		return picture.length;
	}

	@Override
	public Object getItem(int position) {
		return picture[position];
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View layout = inflater.inflate(R.layout.headpicture_contacts, parent,
				false);
		View backcolor=layout.findViewById(R.id.back_color);
		ImageView headPicture = (ImageView) layout.findViewById(R.id.image);
		TextView information = (TextView) layout.findViewById(R.id.information);
		headPicture.setImageResource(picture[position]);
		
		return layout;
	}

}
