package com.letianpai.activity.home;

import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Presence;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.letianpai.R;
import com.letianpai.activity.LoginActivity;
import com.letianpai.activity.TabGroupActivity;
import com.letianpai.utils.XmppTool;

public class HomeMainActivity extends Activity implements OnClickListener {
	ImageView reg, log;
	View headBackText;
	TextView headShare, headText;
	SharedPreferences sp;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.home_main);
		reg = (ImageView) findViewById(R.id.reg);
		headBackText = findViewById(R.id.headBackText);
		log = (ImageView) findViewById(R.id.login);
		reg.setOnClickListener(this);
		log.setOnClickListener(this);
		headBackText.setVisibility(View.INVISIBLE);
		headShare = (TextView) findViewById(R.id.headShare);
		headShare.setVisibility(View.INVISIBLE);
		headText = (TextView) findViewById(R.id.headText);
		headText.setText("我家");
		ProgressDialog pd;
		sp = getSharedPreferences("myPref", MODE_WORLD_READABLE);
		//automaticLogin();
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		switch (arg0.getId()) {
		case R.id.reg:
			Intent intent1 = new Intent(this, RegsActivity.class);
			startActivity(intent1);
			break;
		case R.id.login:
			Intent intent = new Intent(this, LoginActivity.class);
			startActivity(intent);
			break;
		default:
			break;

		}
	}

	// 是否有登录记录如果有记录就自动登录
	public void automaticLogin() {
		// 自动登录用户名密码
		final String username = sp.getString("username", "1");
		final String password = sp.getString("password", "1");
		final String islogin = sp.getString("islogin", "1");
		// 自动登录若果有记录，如果没有记录不做任何操作
		if ("1".equals(username)) {

		} else {
			// 判断是否已经登陆成功
			if ("1".equals(islogin)) {
				new Thread(new Runnable() {
					@Override
					public void run() {
						try {
							XmppTool.getConnection().login(username, password);
							// 状态
							Presence presence = new Presence(
									Presence.Type.available);
							XmppTool.getConnection().sendPacket(presence);
							// 登录openfire
							Intent intent = new Intent(HomeMainActivity.this,
									LoginActivity.class);
							 Window w = TabGroupActivity.group_activity
			                            .getLocalActivityManager().startActivity("family",
			                                    intent);
			                    View v = w.getDecorView();
			                    TabGroupActivity.group_activity.setContentView(v);
						} catch (XMPPException e) {
						} catch (IllegalStateException e) {
						}

					}

				}).start();
			} else {
				// 已经登录

			}

		}

	}
}
