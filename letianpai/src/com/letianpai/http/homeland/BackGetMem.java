package com.letianpai.http.homeland;

public class BackGetMem {
	
	
	private String status;
	private MemBean[] data;
	private String message;
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public MemBean[] getData() {
		return data;
	}
	public void setData(MemBean[] data) {
		this.data = data;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}

}
