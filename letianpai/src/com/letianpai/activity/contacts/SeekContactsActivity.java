package com.letianpai.activity.contacts;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.*;
import android.widget.AdapterView.OnItemClickListener;

import com.letianpai.R;
import com.letianpai.adapter.contacts.SeekAdapter;
import com.letianpai.bean.SixContacts;
import com.letianpai.bean.UserContacts;
import com.letianpai.utils.SeekContactsUtils;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;
import java.util.List;

public class SeekContactsActivity extends Activity {
    TextView headShare,headText;
    View headBackText, seek_contacts;
    EditText edit;
    String information;
    SeekAdapter contactsAdapter;
    GridView gv;
    TextView name1, name2, name3, name4, name5, name6;
    TextView nameNum1, nameNum2, nameNum3, nameNum4, nameNum5, nameNum6;
    Button view0, view1, view2, view3, view4, view5, view6;
    TextView family;
    ArrayList<UserContacts> list;
    int m = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.search_layout);
        initView();
        m = SeekContactsUtils.getFamily(this);
        List<SixContacts> list1 = SeekContactsUtils.getName(this);
        
        if(list1.size()==1){
            name1.setText(list1.get(0).getName());
            nameNum1.setText(list1.get(0).getNumber() + "");
            }
        if(list1.size()==2){
            name1.setText(list1.get(0).getName());
            name2.setText(list1.get(1).getName());
        
            nameNum1.setText(list1.get(0).getNumber() + "");
            nameNum2.setText(list1.get(1).getNumber() + "");

            }
        if(list1.size()==3){
            name1.setText(list1.get(0).getName());
            name2.setText(list1.get(1).getName());
            name3.setText(list1.get(2).getName());
    
            nameNum1.setText(list1.get(0).getNumber() + "");
            nameNum2.setText(list1.get(1).getNumber() + "");
            nameNum3.setText(list1.get(2).getNumber() + "");
            }
        if(list1.size()==4){
            name1.setText(list1.get(0).getName());
            name2.setText(list1.get(1).getName());
            name3.setText(list1.get(2).getName());
            name4.setText(list1.get(3).getName());

            nameNum1.setText(list1.get(0).getNumber() + "");
            nameNum2.setText(list1.get(1).getNumber() + "");
            nameNum3.setText(list1.get(2).getNumber() + "");
            nameNum4.setText(list1.get(3).getNumber() + "");
            }
        if(list1.size()==5){
            name1.setText(list1.get(0).getName());
            name2.setText(list1.get(1).getName());
            name3.setText(list1.get(2).getName());
            name4.setText(list1.get(3).getName());
            name5.setText(list1.get(4).getName());

            nameNum1.setText(list1.get(0).getNumber() + "");
            nameNum2.setText(list1.get(1).getNumber() + "");
            nameNum3.setText(list1.get(2).getNumber() + "");
            nameNum4.setText(list1.get(3).getNumber() + "");
            nameNum5.setText(list1.get(4).getNumber() + "");
            }
        if(list1.size()==6){
            name1.setText(list1.get(0).getName());
            name2.setText(list1.get(1).getName());
            name3.setText(list1.get(2).getName());
            name4.setText(list1.get(3).getName());
            name5.setText(list1.get(4).getName());
            name6.setText(list1.get(5).getName());

            nameNum1.setText(list1.get(0).getNumber() + "");
            nameNum2.setText(list1.get(1).getNumber() + "");
            nameNum3.setText(list1.get(2).getNumber() + "");
            nameNum4.setText(list1.get(3).getNumber() + "");
            nameNum5.setText(list1.get(4).getNumber() + "");
            nameNum6.setText(list1.get(5).getNumber() + "");
           }
   

        family.setText(m + "");
        edit.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                information = edit.getText().toString();

                list = SeekContactsUtils.seekContacts(
                        SeekContactsActivity.this, information);
                contactsAdapter = new SeekAdapter(SeekContactsActivity.this,
                        list);
                seek_contacts.setVisibility(View.VISIBLE);
                gv.setAdapter(contactsAdapter);
                if (information.equals("")) {
                	seek_contacts.setVisibility(View.GONE);
                }
            }
        });

    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        MobclickAgent.onPageStart("SplashScreen"); // ͳ��ҳ��
        MobclickAgent.onResume(this); // ͳ��ʱ��
    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        MobclickAgent.onPageEnd("SplashScreen");
        MobclickAgent.onPause(this);
    }

    public void initView() {

        name1 = (TextView) findViewById(R.id.name1);
        name2 = (TextView) findViewById(R.id.name2);

        name3 = (TextView) findViewById(R.id.name3);
        name4 = (TextView) findViewById(R.id.name4);
        name5 = (TextView) findViewById(R.id.name5);
        name6 = (TextView) findViewById(R.id.name6);

        view0 = (Button) findViewById(R.id.view0);
        view1 = (Button) findViewById(R.id.view1);
        view2 = (Button) findViewById(R.id.view2);
        view3 = (Button) findViewById(R.id.view3);
        view4 = (Button) findViewById(R.id.view4);
        view5 = (Button) findViewById(R.id.view5);
        view6 = (Button) findViewById(R.id.view6);

        seek_contacts = findViewById(R.id.seek_contacts);

        nameNum1 = (TextView) findViewById(R.id.nameNum1);
        nameNum2 = (TextView) findViewById(R.id.nameNum2);
        nameNum3 = (TextView) findViewById(R.id.nameNum3);
        nameNum4 = (TextView) findViewById(R.id.nameNum4);
        nameNum5 = (TextView) findViewById(R.id.nameNum5);
        nameNum6 = (TextView) findViewById(R.id.nameNum6);

        family = (TextView) findViewById(R.id.family);
        gv = (GridView) findViewById(R.id.listView);
        headShare = (TextView) findViewById(R.id.headShare);
        headBackText = findViewById(R.id.headBackText);
        headText=(TextView) findViewById(R.id.headText);
        headText.setText("查找联系人");
        edit = (EditText) findViewById(R.id.edit);
        headShare.setVisibility(View.INVISIBLE);
        headBackText.setOnClickListener(new SeekClick());
        gv.setOnItemClickListener(new SeekOnItemClickListener());
        view0.setOnClickListener(new SeekClick());
        view1.setOnClickListener(new SeekClick());
        view2.setOnClickListener(new SeekClick());
        view3.setOnClickListener(new SeekClick());
        view4.setOnClickListener(new SeekClick());
        view5.setOnClickListener(new SeekClick());
        view6.setOnClickListener(new SeekClick());

    }

    class SeekClick implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.headBackText:
                    finish();
                    break;
                case R.id.view0:
                    list = SeekContactsUtils.seekFamily(SeekContactsActivity.this);
                    contactsAdapter = new SeekAdapter(SeekContactsActivity.this,
                            list);
                    seek_contacts.setVisibility(View.VISIBLE);
                    gv.setAdapter(contactsAdapter);
                    break;
                case R.id.view1:
                    information = (String) name1.getText();

                    list = SeekContactsUtils.seekContacts(
                            SeekContactsActivity.this, information);
                    contactsAdapter = new SeekAdapter(SeekContactsActivity.this,
                            list);
                    seek_contacts.setVisibility(View.VISIBLE);
                    gv.setAdapter(contactsAdapter);
                    break;
                case R.id.view2:
                    information = (String) name2.getText();

                    list = SeekContactsUtils.seekContacts(
                            SeekContactsActivity.this, information);
                    contactsAdapter = new SeekAdapter(SeekContactsActivity.this,
                            list);
                    seek_contacts.setVisibility(View.VISIBLE);
                    gv.setAdapter(contactsAdapter);
                    break;
                case R.id.view3:
                    information = (String) name3.getText();

                    list = SeekContactsUtils.seekContacts(
                            SeekContactsActivity.this, information);
                    contactsAdapter = new SeekAdapter(SeekContactsActivity.this,
                            list);
                    seek_contacts.setVisibility(View.VISIBLE);
                    gv.setAdapter(contactsAdapter);
                    break;
                case R.id.view4:
                    information = (String) name4.getText();

                    list = SeekContactsUtils.seekContacts(
                            SeekContactsActivity.this, information);
                    contactsAdapter = new SeekAdapter(SeekContactsActivity.this,
                            list);
                    seek_contacts.setVisibility(View.VISIBLE);
                    gv.setAdapter(contactsAdapter);
                    break;
                case R.id.view5:
                    information = (String) name5.getText();

                    list = SeekContactsUtils.seekContacts(
                            SeekContactsActivity.this, information);
                    contactsAdapter = new SeekAdapter(SeekContactsActivity.this,
                            list);
                    seek_contacts.setVisibility(View.VISIBLE);
                    gv.setAdapter(contactsAdapter);
                    break;
                case R.id.view6:
                    information = (String) name6.getText();

                    list = SeekContactsUtils.seekContacts(
                            SeekContactsActivity.this, information);
                    contactsAdapter = new SeekAdapter(SeekContactsActivity.this,
                            list);
                    seek_contacts.setVisibility(View.VISIBLE);
                    gv.setAdapter(contactsAdapter);
                    break;
                default:
                    break;
            }
        }
    }
    class SeekOnItemClickListener implements OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id){
            String name = list.get(position).getName();
            String id1 = list.get(position).getUserId();
            Bundle bundle = new Bundle();
            bundle.putString("name", name);
            bundle.putString("id", id1);
            Intent intent = new Intent(SeekContactsActivity.this,
                    ContactsInformationActivity.class);
            intent.putExtras(bundle);
            startActivity(intent);
        }
    }
}
