package com.letianpai.activity.home;

import java.util.HashMap;
import java.util.Map;

import com.letianpai.application.ManageApplication;

import org.jivesoftware.smack.XMPPException;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.letianpai.R;
import com.letianpai.bean.BackResponse;
import com.letianpai.db.Constant;
import com.letianpai.http.RequestBean.SetPassword;
import com.letianpai.http.RequestBean.SetPasswordUserBean;
import com.letianpai.utils.JsonUtil;
import com.letianpai.utils.XmppTool;

public class PwdSettingActivity extends Activity implements OnClickListener{
    TextView pwd_1,pwd_2,title,headShare;
    View ll_up,ll_down,nextstep,headBackText;
    CheckBox cb_pwd;
    EditText number;
    RequestQueue requestQueue;
    String phonenumber;
    String pwd;
    ProgressDialog pd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ManageApplication.getInstance().addActivity(this);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.home_reg);
        init();

        title.setText("注册成功！请设置您的登录密码");
        title.setTextColor(getResources().getColor(R.color.red));
        number.setHint("请输入大于6位数的密码");
        ll_up.setVisibility(View.GONE);
        ll_down.setVisibility(View.GONE);

        cb_pwd.setVisibility(View.VISIBLE);
        pwd_1.setVisibility(View.VISIBLE);
        pwd_2.setVisibility(View.VISIBLE);
        cb_pwd.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
                if(cb_pwd.isChecked()){
                    number.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                }else {
                    number.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                }
            }
        });
        nextstep.setOnClickListener(this);


    }

    @Override
    public void onClick(View arg0) {
        switch(arg0.getId()){
            case R.id.nextstep:
                //  获得密码！！
                pwd=number.getText().toString();
                pd = new ProgressDialog(PwdSettingActivity.this);
                pd.setTitle("请稍等");
                pd.show();
                pd.setCanceledOnTouchOutside(false);
                new Thread(
                        new Runnable() {

                            @Override
                            public void run() {
                                post(phonenumber,pwd);
                            }
                        }).start();

                break;
            default:
                break;
        }
    }

    public void init(){
        headBackText = findViewById(R.id.headBackText);
        headBackText.setOnClickListener(new PasswordClick());
        headShare = (TextView) findViewById(R.id.headShare);
        headShare.setText("");
        ll_up=findViewById(R.id.ll_up);
        ll_down=findViewById(R.id.ll_down);
        cb_pwd=(CheckBox) findViewById(R.id.cb_pwd);
        number=(EditText) findViewById(R.id.number);
        pwd_1=(TextView) findViewById(R.id.pwd_1);
        requestQueue = Volley.newRequestQueue(this);
        phonenumber = getIntent().getStringExtra("username");
        pwd_2=(TextView) findViewById(R.id.pwd_2);
        title=(TextView) findViewById(R.id.title);
        nextstep=findViewById(R.id.nextstep);
    }
    public void post(String username,String password){
        Map<String, String> map = new HashMap<String, String>();
        SetPassword userInfo = new SetPassword();
        userInfo.setAction("step3");
        userInfo.setUsername(username);
        userInfo.setPassword(password);
        SetPasswordUserBean user = new SetPasswordUserBean();
        user.setHead("registerServlet");
        user.setContent(userInfo);
        // msg值
        String msg = JsonUtil.objectToString(user, SetPasswordUserBean.class);
        String url = Constant.URL + "?msg=" + msg;
        Log.d("zzzz", url);
        map.put("msg", msg);
        StringRequest request = new StringRequest(url, new ls(), new el());
        requestQueue.add(request);
    }
    class PasswordClick implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.headBackText:
                    finish();
                    break;

                default:
                    break;
            }
        }

    }
    class ls implements Listener<String> {

        @Override
        public void onResponse(String arg0) {
        	pd.cancel();
            BackResponse br = (BackResponse) JsonUtil.StringToObject(arg0,
                    BackResponse.class);
            Toast.makeText(PwdSettingActivity.this, br.getMessage(),
                    Toast.LENGTH_SHORT).show();
            Log.d("zzzz", br.getData() + br.getMessage() + br.getStatus());
            if (br.getStatus().equals("SUCCESS")) {
                Toast.makeText(PwdSettingActivity.this, "注册成功", Toast.LENGTH_SHORT)
                        .show();
                Intent intent1=new Intent(PwdSettingActivity.this,NameSetting.class);
                intent1.putExtra("username", phonenumber);
                intent1.putExtra("password", pwd);
                startActivity(intent1);
                //注册成功就登陆账号
                new Thread(new Runnable() {

                    @Override
                    public void run() {
                        try {
                            XmppTool.getConnection().login(phonenumber,
                                    pwd);
                        } catch (XMPPException e) {
                            // TODO Auto-generated catch block

                        }
                    }
                }).start();
            }



        }

    }

    class el implements ErrorListener {

        @Override
        public void onErrorResponse(VolleyError arg0) {
        	pd.cancel();
            Toast.makeText(PwdSettingActivity.this, "请求失败", Toast.LENGTH_SHORT)
                    .show();
        }

    }
}
