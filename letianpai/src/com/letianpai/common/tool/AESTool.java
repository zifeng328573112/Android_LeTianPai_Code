package com.letianpai.common.tool;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.security.MessageDigest;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 13-3-18
 * Time: 下午1:54
 * To change this template use File | Settings | File Templatesn
 */
public class AESTool {
    private final static String KEY = "letianpai";

    public static String encrypt(String sSrc) {
        try {
            return encrypt(sSrc, KEY);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String decrypt(String sSrc) {
        try {
            return decrypt(sSrc, KEY);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /*
   * 解密
  */
    public static String decrypt(String sSrc, String sKey) throws Exception {
        // 判断Key是否正确
        if (sKey == null) {
            return null;
        }
        byte[] enCodeFormat = shortMD5(sKey);
        SecretKeySpec skeySpec = new SecretKeySpec(enCodeFormat, "AES");
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.DECRYPT_MODE, skeySpec);
        byte[] encrypted1 = hex2byte(sSrc);
        byte[] original = cipher.doFinal(encrypted1);
        String originalString = new String(original);
        return originalString;
    }

    public static byte[] hex2byte(String str) {
        if (str == null) {
            return null;
        }
        str = str.trim();
        int len = str.length();
        if ((len == 0) || (len % 2 == 1))
            return null;
        byte[] b = new byte[len / 2];
        try {
            for (int i = 0; i < str.length(); i += 2) {
                b[(i / 2)] = (byte) Integer.decode(
                        "0x" + str.substring(i, i + 2)).intValue();
            }
            return b;
        } catch (Exception e) {
        }
        return null;
    }

    public static String byte2hex(byte[] b) {
        String hs = "";
        String stmp = "";
        for (int n = 0; n < b.length; n++) {
            stmp = Integer.toHexString(b[n] & 0xFF);
            if (stmp.length() == 1)
                hs += ("0" + stmp);
            else hs += stmp;
        }
        return hs.toUpperCase();
    }

    /*
     * 加密
     */
    public static String encrypt(String sSrc, String sKey) throws Exception {
        if (sKey == null) {
            return null;
        }
        byte[] enCodeFormat = shortMD5(sKey);
        SecretKeySpec skeySpec = new SecretKeySpec(enCodeFormat, "AES");
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
        byte[] encrypted = cipher.doFinal(sSrc.getBytes());
        return byte2hex(encrypted).toLowerCase();
    }

    private static byte[] shortMD5(String b) throws Exception {
        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(b.getBytes("UTF-8"));
        byte[] digest = md.digest();
        return digest;
    }
}
