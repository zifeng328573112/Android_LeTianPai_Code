package com.letianpai.activity.homeland;

import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Presence;

import com.letianpai.R;
import com.letianpai.utils.XmppTool;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;

public class LogHomeland extends Activity{
	ProgressDialog pd;// 登录的dialog
	View headBackText;
	TextView headShare, headText,login;
	EditText username, pwd;
	SharedPreferences sp;
	Editor editor;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.home_login);
		initView();
	}
	private void initView() {
		// TODO Auto-generated method stub
		username = (EditText) findViewById(R.id.username);
		pwd = (EditText) findViewById(R.id.password);
		login = (TextView) findViewById(R.id.login);
		login.setOnClickListener(new LoghomelandClick());
	}
	
	class LoghomelandClick implements View.OnClickListener{

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.login:
				final String user = username.getText().toString();
				final String pw = pwd.getText().toString();
				sp = getSharedPreferences("myPref", MODE_WORLD_READABLE);
				// 获取数据后面一个参数表示无返回时的默认值
				editor = sp.edit();
				editor.putString("username", user);
				editor.putString("password", pw);
				editor.commit();
				pd = new ProgressDialog(LogHomeland.this);
				pd.setTitle("请稍等");
				pd.setMessage("正在登录...");
				pd.show();
				pd.setCanceledOnTouchOutside(false);
				new Thread(new Runnable() {
					
					@Override
					public void run() {
						try {

							XmppTool.getConnection().login(user,
									pw);
							// 状态
							Presence presence = new Presence(
									Presence.Type.available);
				  			XmppTool.getConnection().sendPacket(presence);
							pd.cancel();
							finish();
							// 登录openfire
						} catch (XMPPException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							pd.cancel();
						}						
					}
				}).start();
				
				break;

			default:
				break;
			}
		}
		
	}  
}
