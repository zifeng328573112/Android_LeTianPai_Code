package com.letianpai.bean.homeland;

import java.io.Serializable;

public class AnniversaryBean implements Serializable{
    private String remindid;
    private String action;
    private String username;
    private String title;
    private String period;//日期
    private String advancedays;//提前多个天提醒
    private String note;
    private String roomid;

	public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getPeriod() {
        return period;
    }
    public void setPeriod(String period) {
        this.period = period;
    }
    public String getAdvancedays() {
        return advancedays;
    }
    public void setAdvancedays(String advancedays) {
        this.advancedays = advancedays;
    }
    public String getNote() {
        return note;
    }
    public void setNote(String note) {
        this.note = note;
    }
    public String getRoomid() {
        return roomid;
    }
    public void setRoomid(String roomid) {
        this.roomid = roomid;
    }
    public String getAction() {
        return action;
    }
    public void setAction(String action) {
        this.action = action;
    }
    public String getRemindid() {
        return remindid;
    }
    public void setRemindid(String remindid) {
        this.remindid = remindid;
    }


}
