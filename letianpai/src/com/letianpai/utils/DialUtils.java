package com.letianpai.utils;

import java.io.ByteArrayInputStream;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.net.Uri;
import android.provider.CallLog;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.letianpai.R;
import com.letianpai.bean.UserCallLog;
import com.letianpai.bean.UserContacts;

public class DialUtils {
    /*
     * 查询最近一条通话记录的号码
     */
    public static String getNumber(Context context, int item) {
        ContentResolver cr;
        Cursor cursor = null;
        String number = "";
        cr = context.getContentResolver();
        cursor = (cr.query(CallLog.Calls.CONTENT_URI,// 使用系统URI，取得通话记录

                new String[]{CallLog.Calls.NUMBER // 电话号码

                }, null, null, CallLog.Calls.DEFAULT_SORT_ORDER));

        // 遍历每条通话记录
        Log.d("zzzz", cursor.getCount() + "");
        if (item < cursor.getCount()) {

            cursor.moveToPosition(item);
            number = cursor.getString(0); // 呼叫号码

        }
        cursor.close();
        return number;
    }

	/*
     * 播放音乐
	 */

    public static void playMusic(final Context context, final int musicId) {

        new Thread(new Runnable() {

            @Override
            public void run() {

                MediaPlayer mp = MediaPlayer.create(context, musicId);
                mp.start();
                try {
                    Thread.sleep(1000);
                    mp.stop();
                    mp.release();
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }).start();


    }

    /*
     * 根据号码查询姓名、图片
     */
    public static UserCallLog getInformation(Context context, String number) {
        Log.d("zzzz", number + "zzzzzzznumber");
        ContentResolver resolver = context.getContentResolver();
        Cursor cursor = resolver.query(CallLog.Calls.CONTENT_URI,
                new String[]{
                        CallLog.Calls.CACHED_NAME,// 联系人
                }, "number=?", new String[]{number},
                CallLog.Calls.DEFAULT_SORT_ORDER);
        String name = "";
        Cursor cursor2 = null;
        Cursor cursor3 = null;
        String phot_ID = null;
        cursor.moveToFirst();
        UserCallLog ucl = new UserCallLog();
        name = cursor.getString(0);
        ucl.setUserName(name);

        if (null != name) {
            Uri uri = Uri
                    .parse("content://com.android.contacts/data/phones/filter/"
                            + name);
            cursor2 = resolver.query(uri, new String[]{"photo_id"}, null,
                    null, null);

            if (cursor2.moveToFirst()) {

                phot_ID = cursor2.getString(0);
                if (null != phot_ID) {
                    cursor3 = resolver.query(ContactsContract.Data.CONTENT_URI,
                            new String[]{"data15"},
                            "ContactsContract.Data._ID=" + phot_ID, null, null);

                    if (cursor3.moveToFirst()) {
                        byte[] photoicon = cursor3.getBlob(0);
                        ByteArrayInputStream inputStream = new ByteArrayInputStream(
                                photoicon);

                        Bitmap contactPhoto = BitmapFactory
                                .decodeStream(inputStream);
                        ucl.setPhoto(contactPhoto);
                        Log.i("info", contactPhoto + "每一次查出来的照片情况！");
                    }
                    cursor3.close();
                } else if (null == phot_ID) {
                    ucl.setPhoto(ImageTools.drawableToBitmap(context.getResources().getDrawable(R.drawable.contact_default_head)));
                }

            }
            cursor2.close();
        }
        cursor.close();
        return ucl;
    }

    /*
     * 显示号码的联系人、图片
     */
    public static void displayInformation(View contentLayout, UserCallLog ucl,
                                          ImageView userImage, TextView name) {
        if (ucl.getPhoto() != null) {
            userImage.setImageBitmap(ucl.getPhoto());
        } else {
            userImage.setImageResource(R.drawable.contact_default_head);
        }
        if (!"".equals(ucl.getUserName())) {
            contentLayout.setVisibility(View.VISIBLE);
            name.setText(ucl.getUserName());
        }
        if (ucl.getUserName() == null) {
            contentLayout.setVisibility(View.INVISIBLE);
        }

    }


    public static void displayContactsInformation(View contentLayout, UserContacts ucl,
                                                  ImageView userImage, TextView name) {

        if (!"".equals(ucl.getName())) {
            contentLayout.setVisibility(View.VISIBLE);
            name.setText(ucl.getName());
        }
        if (ucl.getName() == null) {
            contentLayout.setVisibility(View.INVISIBLE);
        }
        if (ucl.getPicture() != null) {
            userImage.setImageBitmap(ucl.getPicture());
        }

    }

}
