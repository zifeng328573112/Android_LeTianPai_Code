package com.letianpai.activity;

import android.app.ActivityGroup;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;

import com.letianpai.activity.family.FamilyListActivity;
import com.letianpai.activity.home.HomeMainActivity;

@SuppressWarnings("deprecation")
public class TabGroupActivity extends ActivityGroup {
    public static ActivityGroup group_activity;
    boolean isOne = true;
    SharedPreferences sp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        group_activity = this;
        sp = getSharedPreferences("myPref", MODE_WORLD_READABLE);

    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.onBackPressed();
        group_activity.getLocalActivityManager().getCurrentActivity()
                .onBackPressed();
    }

    protected void onStart() {
        super.onStart();
        if (isOne) {
            String islogin = sp.getString("islogin", "1");
            final String username = sp.getString("username", "1");
            final String password = sp.getString("password", "1");
            Log.d("zzzz", islogin);
            if("0".equals(islogin)){
                Intent intent = new Intent(this, FamilyListActivity.class);
                Window w = group_activity.getLocalActivityManager().startActivity(
                        "home_main", intent);
                View view = w.getDecorView();
                group_activity.setContentView(view);
            }else{
                if("1".equals(username)){
                    Intent intent = new Intent(this, HomeMainActivity.class);
                    Window w = group_activity.getLocalActivityManager().startActivity(
                            "home_main", intent);
                    View view = w.getDecorView();
                    group_activity.setContentView(view);
                    isOne = false;
                }else{

                    Intent intent = new Intent(TabGroupActivity.this, FamilyListActivity.class);
                    Window w = group_activity.getLocalActivityManager().startActivity(
                            "home_main", intent);
                    View view = w.getDecorView();
                    group_activity.setContentView(view);
                }

            }
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == FamilyListActivity.FAMILY_CODE) {
            Window subActivity = group_activity.getLocalActivityManager()
                    .startActivity("FamilyListActivity",
                            initIntent(FamilyListActivity.class));
            group_activity.setContentView(subActivity.getDecorView());
        }
    }

    private Intent initIntent(Class<?> cls) {
        return new Intent(this, cls).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    }

}
