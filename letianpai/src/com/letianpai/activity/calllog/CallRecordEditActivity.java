package com.letianpai.activity.calllog;

import android.app.Activity;
import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.letianpai.R;
import com.letianpai.activity.contacts.ContactsInformationActivity;
import com.letianpai.activity.contacts.AddContactsActivity;
import com.letianpai.adapter.UserCallLogAdapter;
import com.letianpai.bean.UserCallLog;
import com.letianpai.utils.CallUtils;
import com.letianpai.utils.ContactsUtils;
import com.umeng.analytics.MobclickAgent;
/*
*  Created by Administrator on 2014/10/8.
 */

public class CallRecordEditActivity extends Activity {
    public static boolean checkBoxVisible;
	TextView name;
    TextView phoneNum,headText,headShare;
    ImageView photo;
    String userNumber, userName;
    View call;
    ListView lv;
    UserCallLogAdapter userCL;
    List<UserCallLog> arraylist = new ArrayList<UserCallLog>();
    View sendMessage, checkInfo, headBackText;
    TextView add_information;
    ImageView add_a;
    String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.call_detail);
        initView();
        lv.setAdapter(userCL);
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        MobclickAgent.onPageStart("SplashScreen"); // 统计页面
        MobclickAgent.onResume(this); // 统计时长
    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        MobclickAgent.onPageEnd("SplashScreen"); // 保证 onPageEnd 在onPause
        // 之前调用,因为 onPause 中会保存信息
        MobclickAgent.onPause(this);
    }

    public void initView() {
        add_information = (TextView) findViewById(R.id.add_information);
        name = (TextView) findViewById(R.id.name);
        phoneNum = (TextView) findViewById(R.id.phoneNum);
        call = findViewById(R.id.call);
        photo = (ImageView) findViewById(R.id.userPic);
        lv = (ListView) findViewById(R.id.listView);
        sendMessage = findViewById(R.id.sendMessage);
        headBackText = findViewById(R.id.headBackText);
        checkInfo = findViewById(R.id.checkInfo);
        add_a = (ImageView) findViewById(R.id.add_a);
        headText=(TextView) findViewById(R.id.headText);
        headShare=(TextView) findViewById(R.id.headShare);
        headShare.setVisibility(View.GONE);
        headText.setText("通话记录");
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        userName = bundle.getString("username");
        userNumber = bundle.getString("number");
        String photoId = bundle.getString("photoid");
        arraylist = CallUtils.getPersonCallLog(this, userNumber);
        userCL = new UserCallLogAdapter(this, arraylist);
        if (null == userName) {
            add_information.setText("保存");
            add_a.setImageResource(R.drawable.icon_content_55);
        } else {
            name.setVisibility(View.VISIBLE);
            add_information.setText("详情");
            add_a.setImageResource(R.drawable.icon_content_56);
            id = ContactsUtils.getContactId(this, userNumber);
            Log.d("zzzz", userNumber);


        }
        if (null != CallUtils.getBitmap(photoId, this)) {

            photo.setImageBitmap(CallUtils.getBitmap(photoId, this));
        }

        name.setText(userName);
        phoneNum.setText(userNumber);
        call.setOnClickListener(new EditListener());
        sendMessage.setOnClickListener(new EditListener());
        headBackText.setOnClickListener(new EditListener());
        checkInfo.setOnClickListener(new EditListener());

    }

    class EditListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            // TODO Auto-generated method stub
            switch (v.getId()) {
                // 拨打电话
                case R.id.call:
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"
                            + userNumber));
                    CallRecordEditActivity.this.startActivity(intent);
                    break;
                // 发送短信
                case R.id.sendMessage:
                    Uri smsToUri = Uri.parse("smsto:" + userNumber);// 联系人地址
                    Intent mIntent = new Intent(
                            android.content.Intent.ACTION_SENDTO, smsToUri);
                    startActivity(mIntent);
                    break;
                case R.id.headBackText:
                    finish();
                    break;
                case R.id.checkInfo:
                    Intent intent1;
                    if (null == userName) {
                        Bundle bundle = new Bundle();
                        intent1 = new Intent(CallRecordEditActivity.this,
                                AddContactsActivity.class);
                        bundle.putString("phoneNumber", userNumber);
                        intent1.putExtras(bundle);
                    } else {
                        Bundle bundle = new Bundle();
                        intent1 = new Intent(CallRecordEditActivity.this,
                                ContactsInformationActivity.class);
                        bundle.putString("name", userName);
                        bundle.putString("id", id);
                        intent1.putExtras(bundle);

                    }
                    startActivity(intent1);
                    break;
                default:
                    break;
            }
        }

    }
}