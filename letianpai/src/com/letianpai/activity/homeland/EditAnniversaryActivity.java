package com.letianpai.activity.homeland;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.letianpai.R;
import com.letianpai.bean.homeland.AnniversaryBean;
import com.letianpai.db.Constant;
import com.letianpai.http.homeland.EditAnniversaryrequest;
import com.letianpai.utils.JsonUtil;

public class EditAnniversaryActivity extends Activity {
	TextView headText, button_homeland, clear_homeland;// 发布,日期,清空
	static TextView day_home_land;
	View headBackText, show_data_layout;
	EditText add_homeland_title, note_homeland;// 纪念的标题,内容
	TextView add_edit;
	int id;// 编辑的条目
	String username;
	String signId;
	AnniversaryBean ab;
	RequestQueue requestQueue;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.add_home);
		initView();
	}

	public void initView() {

		headText = (TextView) findViewById(R.id.headText);
		show_data_layout = findViewById(R.id.show_data_layout);
		show_data_layout.setOnClickListener(new EditClick());
		headText.setText("纪念日");
		username = getIntent().getStringExtra("username");
		signId = getIntent().getStringExtra("signId");
		headBackText = findViewById(R.id.headBackText);
		headBackText.setVisibility(View.VISIBLE);
		headBackText.setOnClickListener(new EditClick());
		add_homeland_title = (EditText) findViewById(R.id.add_homeland_title);
		button_homeland = (TextView) findViewById(R.id.button_homeland);
		button_homeland.setOnClickListener(new EditClick());
		day_home_land = (TextView) findViewById(R.id.day_home_land);
		day_home_land.setOnClickListener(new EditClick());
		note_homeland = (EditText) findViewById(R.id.note_homeland);
		clear_homeland = (TextView) findViewById(R.id.clear_homeland);
		clear_homeland.setOnClickListener(new EditClick());
		add_edit = (TextView) findViewById(R.id.add_edit);
		add_edit.setText("编辑纪念日");
		// 获取传值的对象
		ab = (AnniversaryBean) getIntent().getSerializableExtra("ab");
		id = getIntent().getIntExtra("id", 0);
		add_homeland_title.setText(ab.getTitle());
		note_homeland.setText(ab.getNote());
		day_home_land.setText(ab.getPeriod());
		requestQueue = Volley.newRequestQueue(this);

	}

	class EditClick implements View.OnClickListener {

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.headBackText:
				finish();
				break;
			// 发布
			case R.id.button_homeland:
				boolean flag = isRule();
				if (flag) {
					new Thread(new Runnable() {

						@Override
						public void run() {
							editPost();
						}
					}).start();

				}
				break;
			case R.id.clear_homeland:
				clearHomeland();
				break;
			case R.id.show_data_layout:
				dialog();
				break;
			// 日期
			case R.id.day_home_land:
				Log.d("zzzz", "纪念日");
				Intent intent = new Intent(EditAnniversaryActivity.this,
						HomeSetDayActivity.class);
				startActivity(intent);
				break;
			default:
				break;
			}
		}

	}

	// 获取编辑纪念日的信息
	public AnniversaryBean getAnniversaryBean() {
		AnniversaryBean ab = new AnniversaryBean();
		ab.setTitle(add_homeland_title.getText().toString());// 设置标题
		ab.setPeriod(day_home_land.getText().toString());
		ab.setNote(note_homeland.getText().toString());
		ab.setAdvancedays("7");
		return ab;
	}

	// 清空编辑的内容
	public void clearHomeland() {
		add_homeland_title.setText("");
		note_homeland.setText("");
	}

	// 判断信息是否符合规则
	public boolean isRule() {
		if ("".equals(add_homeland_title.getText().toString())) {
			Toast.makeText(EditAnniversaryActivity.this, "标题不能为空！",
					Toast.LENGTH_SHORT).show();
			return false;
		}
		if ("".equals(note_homeland.getText().toString())) {
			Toast.makeText(EditAnniversaryActivity.this, "备注不能为空！",
					Toast.LENGTH_SHORT).show();
			return false;
		}
		return true;
	}

	// 编辑纪念日Post
	public void editPost() {
		AnniversaryBean userInfo = new AnniversaryBean();
		EditAnniversaryrequest user = new EditAnniversaryrequest();
		userInfo = ab;
		userInfo.setAction("set");
		user.setHead("roomRemindServlet");
		user.setContent(userInfo);
		String msg = JsonUtil
				.objectToString(user, EditAnniversaryrequest.class);
		String url = Constant.URL + "?msg=" + msg;
		Log.d("zzzz", url);
		StringRequest request = new StringRequest(url, new Listener<String>() {

			@Override
			public void onResponse(String arg0) {
				JSONTokener jsonParser = new JSONTokener(arg0);
				JSONObject person;
				String a = "";
				try {
					person = (JSONObject) jsonParser.nextValue();
					Log.d("zzzz", person.getString("status"));
					a = person.getString("status");
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if (a.equals("SUCCESS")) {
					AnniversaryBean ab = getAnniversaryBean();
					AnniversaryActivity.list.remove(id);
					AnniversaryActivity.list.add(id, ab);
					AnniversaryActivity.ann.notifyDataSetChanged();
					finish();
				} else {
					Toast.makeText(EditAnniversaryActivity.this, "发布失败",
							Toast.LENGTH_LONG).show();
				}

			}
		}, new ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError arg0) {
				Toast.makeText(EditAnniversaryActivity.this, "请求失败失败",
						Toast.LENGTH_LONG).show();
			}
		});
		requestQueue.add(request);
	}

	private void dialog() {

	}
}
