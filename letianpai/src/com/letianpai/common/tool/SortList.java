package com.letianpai.common.tool;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 14-9-27
 * Time: 下午1:54
 * 数组排序
 */
public class SortList<E> {
    public void Sort(List<E> list, final String method, final String type,
                     final String sort) {
        Collections.sort(list, new Comparator() {
            public int compare(Object a, Object b) {
                int ret = 0;
                try {
                    Method m1 = ((E) a).getClass().getMethod(method, null);
                    Method m2 = ((E) b).getClass().getMethod(method, null);

                    if (sort != null && "desc".equals(sort)) {//倒序
                        if ("date".equals(type)) {
                            SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd");
                            try {
                                Date bDate = formater.parse(m2.invoke(((E) b), null).toString());
                                Date aDate = formater.parse(m1.invoke(((E) a), null).toString());
                                ret = bDate.compareTo(aDate);
                            } catch (ParseException e) {
                                ret = m2.invoke(((E) b), null).toString().compareTo(m1.invoke(((E) a), null).toString());
                            }
                        } else {
                            ret = m2.invoke(((E) b), null).toString().compareTo(m1.invoke(((E) a), null).toString());
                        }
                    } else {//正序
                        if ("date".equals(type)) {
                            SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd");
                            try {
                                Date bDate = formater.parse(m2.invoke(((E) b), null).toString());
                                Date aDate = formater.parse(m1.invoke(((E) a), null).toString());
                                ret = aDate.compareTo(bDate);
                            } catch (ParseException e) {
                                ret = m1.invoke(((E) a), null).toString().compareTo(m2.invoke(((E) b), null).toString());
                            }
                        } else {
                            ret = m1.invoke(((E) a), null).toString().compareTo(m2.invoke(((E) b), null).toString());
                        }

                    }
                } catch (NoSuchMethodException ne) {
                    System.out.println(ne);
                } catch (IllegalAccessException ie) {
                    System.out.println(ie);
                } catch (InvocationTargetException it) {
                    System.out.println(it);
                }
                return ret;
            }
        });
    }
}
