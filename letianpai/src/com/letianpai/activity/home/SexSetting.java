package com.letianpai.activity.home;

import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.letianpai.R;
import com.letianpai.application.ManageApplication;
import com.letianpai.bean.BackResponse;
import com.letianpai.db.Constant;
import com.letianpai.http.RequestBean.UserInformation;
import com.letianpai.http.RequestBean.UserInformationBean;
import com.letianpai.utils.JsonUtil;

public class SexSetting extends Activity {

    Button btn;
    RadioGroup sex;
    RadioButton male,female;
    RequestQueue requestQueue;
    int temp=1;
    TextView headShare;
    View headBackText;
    ProgressDialog pd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        ManageApplication.getInstance().addActivity(this);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.home_registe_sex);
        btn = (Button) findViewById(R.id.bt_next);
        headShare = (TextView) findViewById(R.id.headShare);
        headShare.setText("");
        headBackText = findViewById(R.id.headBackText);
        headBackText.setOnClickListener(new SexClick());
        sex=(RadioGroup) findViewById(R.id.sex);
        male=(RadioButton) findViewById(R.id.male);
        female=(RadioButton) findViewById(R.id.female);
        requestQueue = Volley.newRequestQueue(this);
        sex.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup arg0, int arg1) {
                if(R.id.male==arg1){
                    temp=1;
                    male.setBackgroundResource(R.drawable.sex_selected);
                    male.setTextColor(getResources().getColor(R.color.white));
                    female.setBackgroundResource(R.drawable.sex_select);
                    female.setTextColor(getResources().getColor(R.color.gray));

                }else if(R.id.female==arg1){
                    temp=2;
                    male.setBackgroundResource(R.drawable.sex_select);
                    male.setTextColor(getResources().getColor(R.color.gray));
                    female.setBackgroundResource(R.drawable.sex_selected);
                    female.setTextColor(getResources().getColor(R.color.white));
                }
            }
        });

        btn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
            	
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        post(temp);
                    }
                }).start();

            }
        });
    }
    class SexClick implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.headBackText:
                    finish();
                    break;

                default:
                    break;
            }
        }

    }
    class ls implements Listener<String> {

        @Override
        public void onResponse(String arg0) {
            BackResponse br = (BackResponse) JsonUtil.StringToObject(arg0,
                    BackResponse.class);
            Toast.makeText(SexSetting.this, br.getMessage(),
                    Toast.LENGTH_SHORT).show();
            if (br.getStatus().equals("SUCCESS")) {
                Toast.makeText(SexSetting.this, br.getMessage(), Toast.LENGTH_SHORT)
                        .show();
                Intent intent = new Intent(SexSetting.this, DaySetting.class);
                startActivity(intent);
            }

        }

    }

    class el implements ErrorListener {

        @Override
        public void onErrorResponse(VolleyError arg0) {
            Toast.makeText(SexSetting.this, "请求失败", Toast.LENGTH_SHORT)
                    .show();
        }

    }

    public void post(int temp){
        Map<String, String> map = new HashMap<String, String>();
        UserInformation userInfo = new UserInformation();
        userInfo.setUsername(RegsActivity.phonenumber);
        userInfo.setSex(temp);
        Log.d("zzzz", userInfo.getUsername());
        UserInformationBean user = new UserInformationBean();
        user.setHead("changeUserInfoServlet");
        user.setContent(userInfo);
        //msg值
        String msg = JsonUtil.objectToString(user, UserInformationBean.class);
        String url = Constant.URL + "?msg=" + msg;
        Log.d("zzzz", url);
        map.put("msg", msg);
        StringRequest request = new StringRequest(url, new ls(), new el());
        requestQueue.add(request);
    }
}
