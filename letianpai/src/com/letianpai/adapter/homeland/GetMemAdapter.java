package com.letianpai.adapter.homeland;

import com.letianpai.R;
import com.letianpai.http.homeland.BackBean;
import com.letianpai.http.homeland.BackGetMem;
import com.letianpai.http.homeland.MemBean;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

public class GetMemAdapter extends BaseAdapter {
    private Context context;
    private LayoutInflater inflater;
    List<MemberInfo> infoList;

    public void setArray(List<MemberInfo> infoList) {
        this.infoList = infoList;
    }

    public GetMemAdapter(Context context, List<MemberInfo> infoList) {
        super();
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.infoList = infoList;
    }

    @Override
    public int getCount() {
        return infoList.size();
    }

    @Override
    public Object getItem(int position) {
        return infoList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        view = inflater.inflate(R.layout.get_mem_item, parent, false);
        TextView get_mem_name = (TextView) view.findViewById(R.id.get_mem_name);
        MemberInfo info = infoList.get(position);
        get_mem_name.setText(info.getUsername());
        return view;
    }

}
