package com.letianpai.adapter.photoalbum;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.letianpai.R;
import com.letianpai.activity.photoalbum.NewAlbumActivity;
import com.letianpai.common.tool.StringUtil;
import com.letianpai.db.cache.PhotoAlbumCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 14-9-25
 * Time: 下午3:53
 * To change this template use File | Settings | File Templates.
 */
public class HomePhotoAdapter extends BaseAdapter {
    private List<HomePhotoBean> photoBeanList;
    private Context context;
    private String roomId, username;
    private LayoutInflater layoutInflater;
    private boolean delete;
    DisplayImageOptions options; // 配置图片加载及显示选项
    private ImageLoader imageLoader;

    public HomePhotoAdapter(Context context, String roomId, String username, boolean delete, List<HomePhotoBean> photoBeanList) {
        this.photoBeanList = photoBeanList;
        this.context = context;
        this.roomId = roomId;
        this.username = username;
        this.delete = delete;
        layoutInflater = LayoutInflater.from(context);
        // 配置图片加载及显示选项（还有一些其他的配置，查阅doc文档吧）
        options = new DisplayImageOptions.Builder()
//                .showStubImage(R.drawable.ic_launcher) // 在ImageView加载过程中显示图片
                .showImageForEmptyUri(R.drawable.photoalbum_xinjian) // image连接地址为空时
                .showImageOnFail(R.drawable.photoalbum_xinjian) // image加载失败
                .cacheInMemory(true) // 加载图片时会在内存中加载缓存
                .cacheOnDisc(true) // 加载图片时会在磁盘中加载缓存
                .displayer(new RoundedBitmapDisplayer(5, false)) // 设置用户加载图片task(这里是圆角图片显示)
                .build();
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
    }

    public int getCount() {
        if (null == photoBeanList) {
            return 0;
        }
        return photoBeanList.size();
    }

    public Object getItem(int position) {
        return photoBeanList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        if (view == null) {
            view = layoutInflater.inflate(R.layout.home_land_photo_item, null);
        }
        ImageView imageUrlIv = (ImageView) view.findViewById(R.id.home_photo_iv);
        TextView homeTitleIv = (TextView) view.findViewById(R.id.home_photo_title_tv);
        TextView numberIv = (TextView) view.findViewById(R.id.home_photo_number_value_tv);
        TextView uploadPeopleIv = (TextView) view.findViewById(R.id.home_photo_shangchuanren_value_tv);
        TextView timeIv = (TextView) view.findViewById(R.id.home_photo_time_value_tv);
        CheckBox homeCb = (CheckBox) view.findViewById(R.id.home_photo_cb);
        HomePhotoBean bean = photoBeanList.get(position);
        if (delete) {
            homeCb.setVisibility(View.VISIBLE);
        } else {
            homeCb.setVisibility(View.GONE);
        }
        if (bean.isWhetherPublic()) {
            imageUrlIv.setBackgroundResource(R.drawable.photoalbum_moren);
        } else {
            imageUrlIv.setBackgroundResource(R.drawable.photoalbum_xinjian);
        }
        homeCb.setOnCheckedChangeListener(new CheckChangeListener(bean.getHomeTitle()));
        homeTitleIv.setText(bean.getHomeTitle());
        numberIv.setText(bean.getNumber());
        uploadPeopleIv.setText(bean.getUploadPeople());
        timeIv.setText(bean.getTime());
        //异步加载
        if (StringUtil.isNotBlank(bean.getImageUrl())) {
            imageLoader.displayImage(bean.getImageUrl(), imageUrlIv, options, null);
        }
        view.setOnClickListener(new PhotoOnClickListener(bean.getAlbumId(), bean.getUploadPeople()));
        return view;
    }

    class PhotoOnClickListener implements View.OnClickListener {
        String albumId;
        String owner;

        PhotoOnClickListener(String albumId, String owner) {
            this.albumId = albumId;
            this.owner = owner;
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(context, NewAlbumActivity.class);
            intent.putExtra("username", username);
            intent.putExtra("roomid", roomId);
            intent.putExtra("albumId", albumId);
            intent.putExtra("owner", owner);
            context.startActivity(intent);
        }
    }

    class CheckChangeListener implements CompoundButton.OnCheckedChangeListener {
        String albumId;

        CheckChangeListener(String albumId) {
            this.albumId = albumId;
        }

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (isChecked) {
                PhotoAlbumCache.modifyPhotoAlbum(context, albumId);
            } else {
                PhotoAlbumCache.movePhotoAlbum(context, albumId);
            }
        }
    }


}
