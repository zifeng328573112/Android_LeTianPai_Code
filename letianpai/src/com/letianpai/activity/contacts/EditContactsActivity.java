package com.letianpai.activity.contacts;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import com.letianpai.R;
import com.letianpai.activity.HeadPictureActivity_edit;
import com.letianpai.adapter.AddBtAdapter;
import com.letianpai.bean.AddType;
import com.letianpai.bean.UserContacts;

import com.letianpai.utils.ContactsUtils;

import android.app.Activity;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

public class EditContactsActivity extends Activity implements OnClickListener {
    public static AddBtAdapter b_adapter;
    static ContentResolver cr;
    // static ImageView delete;
    // static LayoutInflater inflater;
    EditText e_realphoneNum, e_name;
    static ImageView e_imagehead;
    View delete_one;
    View addMore;
    View headBackText;
    View headShare;
    public ListView biantai;
    public static ArrayList<AddType>info=new ArrayList<AddType>();
    final static int REQUEST_CODE_DELETE=103;
    public ArrayList<String> editNumber=new ArrayList<String>();
    public ArrayList<String> editEmail=new ArrayList<String>();
    public ArrayList<String> editAddress=new ArrayList<String>();
    public ArrayList<String> editRemarks=new ArrayList<String>();
    public ArrayList<String> editNickname=new ArrayList<String>();
    public ArrayList<String> editIm=new ArrayList<String>();

    private UserContacts uc;
    private String name;
    
    public static Handler handler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
                case 0:
                    Bitmap bm = (Bitmap) msg.getData().get("data");
                    EditContactsActivity.e_imagehead.setImageBitmap(bm);

                    break;
                case 1:

                    Uri uri = Uri.parse((String) msg.getData().get("a"));
                    Cursor cursor = cr.query(uri, null, null, null, null);
                    cursor.moveToFirst();

                    Bitmap bitmap = null;
                    try {
                        bitmap = BitmapFactory
                                .decodeStream(cr.openInputStream(uri));
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                    EditContactsActivity.e_imagehead.setImageBitmap(bitmap);
                    break;
                case 2:
                    int pictureId = (Integer) msg.getData().get("picture");
                    EditContactsActivity.e_imagehead.setImageResource(pictureId);
                    break;
                case 3:
                    // String information = msg.getData().getString("information");
                    if (ContactEditPropertyDialog.somemore != null) {
                        info.add(new AddType(ContactEditPropertyDialog.somemore));
                        b_adapter.notifyDataSetChanged();
                    }
                    break;
                default:
                    break;
            }

        }

    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.contact_add);
        String id = getIntent().getStringExtra("id");
        uc = ContactsUtils.getOneContacts(this, id);
        List<String>value= ContactsUtils.getUserInformation(uc);
        List<String>type=ContactsUtils.getContactsName(uc);
        for (int i = 0; i <value.size(); i++) {
            info.add(new AddType(type.get(i),value.get(i)));
        }
        init();
    }

    private void init() {
        e_name = (EditText) findViewById(R.id.phoneNum);
        e_realphoneNum = (EditText) findViewById(R.id.realphoneNum);
        e_imagehead = (ImageView) findViewById(R.id.pic);
        delete_one = findViewById(R.id.delete_one);
        delete_one.setVisibility(View.VISIBLE);
        addMore = findViewById(R.id.addMore);
        headBackText = findViewById(R.id.headBackText);
        headShare = findViewById(R.id.headShare);

        e_imagehead.setOnClickListener(this);
        addMore.setOnClickListener(this);
        delete_one.setOnClickListener(this);
        headBackText.setOnClickListener(this);
        headShare.setOnClickListener(this);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if (null != bundle) {
            String id = bundle.getString("id");
            name = bundle.getString("name");
            Bitmap head = bundle.getParcelable("head");
            String number = bundle.getString("phoneNumber");
            if (null != name) {
                uc.setName(name);
                e_name.setText(name);
            }
            if (number != null) {
                e_realphoneNum.setText(number);
            }
            if (head != null) {
                e_imagehead.setImageBitmap(head);
                uc.setHaveHead(true);
            }
        }

        biantai = (ListView) findViewById(R.id.biantai);
        b_adapter = new AddBtAdapter(this,info);
        biantai.setAdapter(b_adapter);
    }

    @Override
    public void onClick(View arg0) {
        // TODO Auto-generated method stub
        switch (arg0.getId()) {
            case R.id.delete_one:
                ContactsUtils.delContact(EditContactsActivity.this,uc.getName());
                EditContactsActivity.this.setResult(144);
                ContactsActivity.isUpdate=true;
                info.clear();
                finish();
                break;
            case R.id.addMore:
                dialog();
                break;
            case R.id.pic:
                Intent intent = new Intent(EditContactsActivity.this,
                        HeadPictureActivity_edit.class);
                startActivity(intent);
                break;
            case R.id.headShare:
                UserContacts uc = getEdit();
                if(!uc.getName().equals("")){
                    ContactsUtils.toupdateContact(this, uc.getOldname(), uc);
                    info.clear();
                    ContactsActivity.isUpdate=true;
                    finish();
                }
                else {
                    Toast.makeText(EditContactsActivity.this, "请输入联系人姓名", 1000).show();
                }
                break;
            case R.id.headBackText:
                info.clear();
                finish();
                break;
            default:
                break;
        }
    }

    protected void dialog() {

        Dialog dialog = new ContactEditPropertyDialog(EditContactsActivity.this,
                R.style.customDialog);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(event.getKeyCode()==KeyEvent.KEYCODE_BACK){
            info.clear();
        }
        return super.onKeyDown(keyCode, event);
    }


    public UserContacts getEdit() {
        UserContacts uc_edit = new UserContacts();
        Drawable head = e_imagehead.getDrawable();
        uc_edit.setHead(head); // 头像
        uc_edit.setName(e_name.getText().toString()); // 姓名

        getAllText();
        uc_edit.setAddress(editAddress);
        uc_edit.setEmail(editEmail);
        editNumber.add(e_realphoneNum.getText().toString());
        uc_edit.setNumber(editNumber);
        uc_edit.setRemarks(editRemarks);
        uc_edit.setNickName(editNickname);
        uc_edit.setUserIM(editIm);
        uc_edit.setOldname(name);
        
        
        
        uc_edit.setHaveAddress(uc.isHaveAddress());
        uc_edit.setHaveEmail(uc.isHaveEmail());
        uc_edit.setHaveHead(uc.isHaveHead());
        uc_edit.setHaveIm(uc.isHaveIm());
        uc_edit.setHaveNickName(uc.isHaveNickName());
        uc_edit.setHaveRemarks(uc.isHaveRemarks());
        
        return uc_edit;
    }

    public void getAllText() {
        for (int i = 0; i <info.size(); i++) {
            String phoneNum =info.get(i).getValue();
            if (info.get(i).getType().equals("电话")) {
                System.out.println("editNumber "+editNumber);
                editNumber.add(phoneNum);
            }
            if (info.get(i).getType().equals("邮箱")) {
                editEmail.add(phoneNum);
            }
            if (info.get(i).getType().equals("备注")) {
                editRemarks.add(phoneNum);
            }
            if (info.get(i).getType().equals("IM")) {
                editIm.add(phoneNum);
            }
            if (info.get(i).getType().equals("昵称")) {
                editNickname.add(phoneNum);
            }
            if (info.get(i).getType().equals("地址")) {
                editAddress.add(phoneNum);
            }
        }
    }
}
