package com.letianpai.common.tool;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import java.lang.ref.SoftReference;
import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 13-6-4
 * Time: 下午3:03
 * 图片浏览功能 的图片加载，无压缩
 */
public class BrowserAsyncImageLoader {
    RelativeLayout relativeLayout;
    ProgressBar progressBar;
    Context context;
    //SoftReference是软引用，是为了更好的为了系统回收变量
    private HashMap<String, SoftReference<Drawable>> imageCache;

    public BrowserAsyncImageLoader(Context context, RelativeLayout relativeLayout) {
        this.context = context;
        this.relativeLayout = relativeLayout;
        progressBar = new ProgressBar(context);
        RelativeLayout.LayoutParams mParams = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        mParams.addRule(RelativeLayout.CENTER_IN_PARENT);
        progressBar.setLayoutParams(mParams);
        relativeLayout.addView(progressBar);
        imageCache = new HashMap<String, SoftReference<Drawable>>();
    }

    public Drawable loadDrawable(final String imageUrl, final ImageView imageView) {
        if (imageCache.containsKey(imageUrl)) {
            //从缓存中获取
            SoftReference<Drawable> softReference = imageCache.get(imageUrl);
            Drawable drawable = softReference.get();
            if (drawable != null) {
                return drawable;
            }
        }
        new StaffAsyncTask(imageUrl, imageView).execute();
        return null;
    }

    class StaffAsyncTask extends AsyncTask<String, Integer, Drawable> {
        String imageUrl;
        ImageView imageView;

        public StaffAsyncTask(String imageUrl, ImageView imageView) {
            this.imageUrl = imageUrl;
            this.imageView = imageView;
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }

        @Override
        protected Drawable doInBackground(String... params) {
            Bitmap bitmap = ImageTool.readNetWorkImage(imageUrl);
//                Bitmap bitmap = ImageTool.readNetWorkImageBuHttpClient(imageUrl);
            Drawable drawable = ImageTool.convertToBitmap(bitmap);
//                Drawable drawable = ImageTool.convetToBitmap(bitmap);
//                Drawable drawable = loadImageFromUrl(imageUrl);
            imageCache.put(imageUrl, new SoftReference<Drawable>(drawable));
            return drawable;
        }

        @Override
        protected void onPostExecute(Drawable result) {
            super.onPostExecute(result);
            progressBar.setVisibility(View.GONE);
//            relativeLayout.removeView(progressBar);
            imageView.setImageDrawable(result);
            imageView.setVisibility(View.VISIBLE);
//            imageCallback.imageLoaded(result, imageView, imageUrl);
        }

        @Override
        protected void onPreExecute() {
            imageView.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
        }
    }

    //回调接口
    public interface ImageCallback {
        public void imageLoaded(Drawable imageDrawable, ImageView imageView, String imageUrl);
    }
}
