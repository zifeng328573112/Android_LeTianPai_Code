package com.letianpai.http.RequestBean;


public class IdentifyUserBean {
	private String head;
	private Identifying content;
	public String getHead() {
		return head;
	}
	public void setHead(String head) {
		this.head = head;
	}
	public Identifying getIdentifying() {
		return content;
	}
	public void setIdentifying(Identifying content) {
		this.content = content;
	}
	
}
