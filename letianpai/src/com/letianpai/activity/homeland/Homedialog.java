package com.letianpai.activity.homeland;

import java.util.ArrayList;

import android.app.Dialog;
import android.app.LauncherActivity.ListItem;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.letianpai.R;
import com.letianpai.adapter.ListDialogAdapter;

public class Homedialog extends Dialog{
	Context context;
    ArrayList<String> listItem = new ArrayList<String>();
    
    	//{ "不提醒", "提前1天", "提前2天", "提前3天", "提前4天", "提前5天","提前6天","提前7天"};
	TextView dialog = (TextView) findViewById(R.id.dialog);
	public static String somemore = "7";
	public Homedialog(Context context, int theme) {
		super(context,theme);
		this.context = context;
        
		// TODO Auto-generated constructor stub
	}



	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.list_dialog);
		listItem.add("不提醒");
		listItem.add("提前1天");
		listItem.add("提前2天");
		listItem.add("提前3天");
		listItem.add("提前4天");
		listItem.add("提前5天");
		listItem.add("提前6天");
		listItem.add("提前7天");
		ListDialogAdapter ld = new ListDialogAdapter(context, listItem);
		ListView listview = (ListView) findViewById(R.id.listView);
		listview.setAdapter(ld);
		listview.setOnItemClickListener(new DialogListener());
		
	}

	class DialogListener implements OnItemClickListener{

		
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			somemore = String.valueOf(position);
			Homedialog.this.cancel();
			AddAnniversaryActivity.handler.sendEmptyMessage(3);
		}
		
	}
	
}
