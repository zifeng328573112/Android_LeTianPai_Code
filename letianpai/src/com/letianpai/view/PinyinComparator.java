package com.letianpai.view;

import java.util.Comparator;

import com.letianpai.bean.UserContacts;

/**
 * 
 * @author xiaanming
 *
 */
public class PinyinComparator implements Comparator<UserContacts> {

	public int compare(UserContacts o1, UserContacts o2) {
		if (o1.getSortLetters().equals("@")
				|| o2.getSortLetters().equals("#")) {
			return -1;
		} else if (o1.getSortLetters().equals("#")
				|| o2.getSortLetters().equals("@")) {
			return 1;
		} else {
			return o1.getSortLetters().compareTo(o2.getSortLetters());
		}
	}

}
