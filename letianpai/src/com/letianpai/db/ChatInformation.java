package com.letianpai.db;

public class ChatInformation {
	private String uId;
	private String mId;
	private String chatInformation;
	

	public String getmId() {
		return mId;
	}

	public void setmId(String mId) {
		this.mId = mId;
	}

	public String getuId() {
		return uId;
	}

	public void setuId(String uId) {
		this.uId = uId;
	}

	public String getChatInformation() {
		return chatInformation;
	}

	public void setChatInformation(String chatInformation) {
		this.chatInformation = chatInformation;
	}

}
