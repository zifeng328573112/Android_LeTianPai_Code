package com.letianpai.activity.homeland;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectPostRequest;
import com.android.volley.toolbox.Volley;
import com.letianpai.R;
import com.letianpai.bean.homeland.AnniversaryBean;
import com.letianpai.db.Constant;
import com.letianpai.http.homeland.Anniversaryrequest;
import com.letianpai.utils.JsonUtil;

public class AddAnniversaryActivity extends Activity {
	TextView headText, button_homeland, clear_homeland;// 发布,日期,清空
	static TextView day_home_land;
	static TextView show_data_text;
	View headBackText,show_data_layout;
	
	EditText add_homeland_title, note_homeland;// 纪念的标题,内容
	String username;
	String signId;
	RequestQueue requestQueue;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.add_home);
		initView();
	}

	private void initView() {
		username = getIntent().getStringExtra("username");
		signId = getIntent().getStringExtra("signId");
		Log.d("zzzz", signId);
		headText = (TextView) findViewById(R.id.headText);
		headText.setText("纪念日");
		headBackText = findViewById(R.id.headBackText);
		headBackText.setVisibility(View.VISIBLE);
		show_data_layout = findViewById(R.id.show_data_layout);
		show_data_layout.setOnClickListener(new AddHomeClick());
		requestQueue = Volley.newRequestQueue(this);
		headBackText.setOnClickListener(new AddHomeClick());
		add_homeland_title = (EditText) findViewById(R.id.add_homeland_title);
		button_homeland = (TextView) findViewById(R.id.button_homeland);
		button_homeland.setOnClickListener(new AddHomeClick());
		day_home_land = (TextView) findViewById(R.id.day_home_land);
		day_home_land.setOnClickListener(new AddAnniversaryClick());
		note_homeland = (EditText) findViewById(R.id.note_homeland);
		clear_homeland = (TextView) findViewById(R.id.clear_homeland);
		clear_homeland.setOnClickListener(new AddHomeClick());
		
		show_data_text=(TextView) findViewById(R.id.show_data_text);
	}

	class AddHomeClick implements View.OnClickListener {

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.headBackText:
				finish();
				break;
			// 发布
			case R.id.button_homeland:
				boolean flag = isRule();
				if (flag) {

					new Thread(new Runnable() {

						@Override
						public void run() {
							post();
						}
					}).start();
				}
				break;
			case R.id.clear_homeland:
				clearHomeland();
                break;
			case R.id.show_data_layout:
				dialog();
				break;
			default:
				break;
			}
		}

	}

	// 获取编辑纪念日的信息
	public AnniversaryBean getAnniversaryBean() {
		AnniversaryBean ab = new AnniversaryBean();
		ab.setTitle(add_homeland_title.getText().toString());// 设置标题
		ab.setPeriod(day_home_land.getText().toString());
		ab.setNote(note_homeland.getText().toString());
		ab.setAdvancedays(Homedialog.somemore);
		return ab;
	}

	// 清空编辑的内容
	public void clearHomeland() {
		add_homeland_title.setText("");
		note_homeland.setText("");
	}

	// 判断信息是否符合规则
	public boolean isRule() {
		if ("".equals(add_homeland_title.getText().toString())) {
			Toast.makeText(AddAnniversaryActivity.this, "标题不能为空！",
					Toast.LENGTH_SHORT).show();
			return false;
		}
		if ("".equals(note_homeland.getText().toString())) {
			Toast.makeText(AddAnniversaryActivity.this, "备注不能为空！",
					Toast.LENGTH_SHORT).show();
			return false;
		}
		return true;
	}

	public void post() {
		Log.d("zzzz", "post");
		AnniversaryBean userInfo = new AnniversaryBean();
		userInfo = getAnniversaryBean();
		/*userInfo.setTitle("哈哈");// 设置标题
		userInfo.setPeriod("嘿嘿");
		userInfo.setNote("呵呵");
		userInfo.setAdvancedays("你妹");*/
		
		Anniversaryrequest user = new Anniversaryrequest();
		userInfo.setAction("add");
		userInfo.setRoomid(signId);
		userInfo.setUsername(username);
		user.setContent(userInfo);
		user.setHead("roomRemindServlet");
		String msg = JsonUtil.objectToString(user, Anniversaryrequest.class);
		System.out.println("我去"+msg);
		String url = Constant.URL + "?msg=" + msg;
		Log.d("zzzz", url);
		Map<String, String>map=new HashMap<String, String>();
		map.put("msg", msg);
		JsonObjectPostRequest request = new JsonObjectPostRequest(Constant.URL, new Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject arg0) {
				Log.d("zzzzz", arg0.toString());
				JSONTokener jsonParser = new JSONTokener(arg0.toString());
				JSONObject person;
				String a = "";
				String b = "";
				try {
					person = (JSONObject) jsonParser.nextValue();
					Log.d("zzzzz", person.getString("status"));
					a = person.getString("data");
					b = person.getString("status");
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if (b.equals("SUCCESS")) {
					AnniversaryBean ab = getAnniversaryBean();
					ab.setRemindid(a);
//					AnniversaryActivity.list.add(ab);
//					AnniversaryActivity.ann.notifyDataSetChanged();
					finish();
				} else {
					Toast.makeText(AddAnniversaryActivity.this, "发布失败",
							Toast.LENGTH_LONG).show();
					;
				}

			}
		}, new ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError arg0) {
				Log.d("zzzzz", arg0.toString());
				Toast.makeText(AddAnniversaryActivity.this, "请求失败",
						Toast.LENGTH_LONG).show();
			}
		},map);
		requestQueue.add(request);

	}
	
	private void dialog() {
		Dialog dialog = new Homedialog(AddAnniversaryActivity.this,
				R.style.customDialog);
		dialog.setCanceledOnTouchOutside(false);

		dialog.show();
	}
	
	public static Handler handler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);

			switch (msg.what) {
	
			case 3:
				if (Homedialog.somemore != null) {
					//传这个？
					System.out.println(Homedialog.somemore);
					show_data_text.setText("提前"+Homedialog.somemore+"天");
				}
				break;
			default: 
				break;
			}

		}

	};
	class AddAnniversaryClick implements View.OnClickListener{

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.day_home_land:
				Intent intent = new Intent(AddAnniversaryActivity.this,
						HomeSetDayActivity.class);
				intent.putExtra("add", "add");
				startActivity(intent);
				break;

			default:
				break;
			}
		}
		
	}
}
