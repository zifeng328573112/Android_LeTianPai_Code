package com.letianpai.http.homeland;

public class UniqueRequest {
	private String head;
	private UniqueBean content;

	public String getHead() {
		return head;
	}

	public void setHead(String head) {
		this.head = head;
	}

	public UniqueBean getContent() {
		return content;
	}

	public void setContent(UniqueBean content) {
		this.content = content;
	}

}
