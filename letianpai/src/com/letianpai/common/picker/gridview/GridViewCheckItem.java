package com.letianpai.common.picker.gridview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Checkable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.letianpai.R;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 14-9-27
 * Time: 下午5:49
 * To change this template use File | Settings | File Templates.
 */
public class GridViewCheckItem extends RelativeLayout implements Checkable {

    private Context mContext;
    private boolean mChecked;
    private ImageView mImgView = null;
    private ImageView mSecletView = null;

    public GridViewCheckItem(Context context) {
        this(context, null, 0);
    }

    public GridViewCheckItem(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public GridViewCheckItem(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        mContext = context;
        LayoutInflater.from(mContext).inflate(R.layout.gridview_right_top_check_item, this);
        mImgView = (ImageView) findViewById(R.id.gridview_check_no_iv);
        mSecletView = (ImageView) findViewById(R.id.gridview_check_yes_iv);
    }

    @Override
    public void setChecked(boolean checked) {
        mChecked = checked;
        setBackgroundDrawable(checked ? getResources().getDrawable(
                R.drawable.background) : null);
        mSecletView.setVisibility(checked ? View.VISIBLE : View.GONE);
    }

    @Override
    public boolean isChecked() {
        return mChecked;
    }

    @Override
    public void toggle() {
        setChecked(!mChecked);
    }

    public void setImgResId(int resId) {
        if (mImgView != null) {
            mImgView.setBackgroundResource(resId);
        }
    }
}
