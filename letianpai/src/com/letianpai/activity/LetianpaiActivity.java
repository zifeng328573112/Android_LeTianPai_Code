package com.letianpai.activity;

import android.app.ActivityGroup;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;
import android.widget.Toast;

import com.letianpai.R;
import com.letianpai.activity.calllog.CallRecordActivity;
import com.letianpai.activity.contacts.ContactsActivity;
import com.letianpai.activity.dial.DialActivity;
import com.letianpai.activity.homeland.HomelandActivity;
import com.letianpai.application.ManageApplication;
import com.letianpai.db.cache.UserCache;
import com.umeng.analytics.MobclickAgent;

@SuppressWarnings("deprecation")
public class LetianpaiActivity extends ActivityGroup {
    /*
     * luo
     */
    TabHost tabHost;
    static TabSpec tab1, tab2, tab3, tab4, tab5;
    View indicator1, indicator2, indicator3, indicator4, indicator5;

    private long exitTime = 0;

    // 再按一次退出程序
    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            if (event.getAction() == KeyEvent.ACTION_DOWN
                    && event.getRepeatCount() == 0) {
                this.exitApp();
            }
            return true;
        }
        return super.dispatchKeyEvent(event);
    }

    /**
     * 退出程序
     */
    private void exitApp() {
        // 判断2次点击事件时间
        if ((System.currentTimeMillis() - exitTime) > 2000) {
            Toast.makeText(LetianpaiActivity.this, "再按一次退出程序",
                    Toast.LENGTH_SHORT).show();
            exitTime = System.currentTimeMillis();
        } else {
            UserCache.getInstance().modifyLogin(LetianpaiActivity.this, false);
            ManageApplication.getInstance().finishAll();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);
        initView();
        // 选中指定的tab
        tabHost.setCurrentTab(0);
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        MobclickAgent.onPageStart("SplashScreen"); // 统计页面
        MobclickAgent.onResume(this); // 统计时长
    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        MobclickAgent.onPageEnd("SplashScreen"); // 保证 onPageEnd 在onPause
        // 之前调用,因为 onPause 中会保存信息
        MobclickAgent.onPause(this);
    }

    public void initView() {
        tabHost = (TabHost) findViewById(android.R.id.tabhost);

        tabHost.setup(this.getLocalActivityManager());
        Intent intent = new Intent(this, CallRecordActivity.class);
        // 第一个选项
        tab1 = tabHost.newTabSpec("first");
        indicator1 = LayoutInflater.from(this).inflate(R.layout.contacts, null);
        tab1.setIndicator(indicator1);

        // 设置内容
        tab1.setContent(new Intent(this, ContactsActivity.class));

        tabHost.addTab(tab1);
        // 第二个选项
        tab2 = tabHost.newTabSpec("second");
        indicator2 = LayoutInflater.from(this).inflate(R.layout.call_log, null);
        tab2.setIndicator(indicator2);
        tab2.setContent(intent);
        tabHost.addTab(tab2);
        // 第三个选项
        TabSpec tab3 = tabHost.newTabSpec("third");
        indicator3 = LayoutInflater.from(this).inflate(R.layout.dial, null);
        tab3.setIndicator(indicator3);
        tab3.setContent(new Intent(this, DialActivity.class));
        tabHost.addTab(tab3);
        // 第四个选项
        // 第四个选项
        tab4 = tabHost.newTabSpec("fourth");
        indicator4 = LayoutInflater.from(this).inflate(R.layout.homeland, null);
        tab4.setIndicator(indicator4);
        tab4.setContent(new Intent(this, TabGroupActivity.class));
        tabHost.addTab(tab4);
        // 第五个选项
        tab5 = tabHost.newTabSpec("fiveth");
        indicator5 = LayoutInflater.from(this).inflate(R.layout.home, null);
        tab5.setIndicator(indicator5);

        tab5.setContent(new Intent(this, HomelandActivity.class));

        tabHost.addTab(tab5);
    }

}
