package com.letianpai.adapter.contacts;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.letianpai.R;
import com.letianpai.bean.UserContacts;

import java.util.ArrayList;
import java.util.List;

/*
 * 联系人适配器
 */
public class SeekAdapter extends BaseAdapter {
    private Context context;
    private LayoutInflater inflater;
    List<UserContacts> arraylist = new ArrayList<UserContacts>();

    public SeekAdapter(Context context, List<UserContacts> arraylist) {
        super();
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.arraylist = arraylist;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return arraylist.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return arraylist.get(position + 2);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View layout = inflater
                .inflate(R.layout.seekcontactsitem, parent, false);

        TextView name = (TextView) layout.findViewById(R.id.name);
        TextView number = (TextView) layout.findViewById(R.id.number);
        ImageView image = (ImageView) layout.findViewById(R.id.name1);
        TextView place = (TextView) layout.findViewById(R.id.place);

        if (arraylist.get(position).getNumber().get(0) == null) {
            number.setText("");
        } else {
            name.setText(arraylist.get(position).getName());
            number.setText(arraylist.get(position).getNumber().get(0));
            place.setText(arraylist.get(position).getPlace());
        }

        return layout;
    }

}
