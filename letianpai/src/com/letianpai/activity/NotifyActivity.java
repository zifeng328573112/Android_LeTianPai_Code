package com.letianpai.activity;

import java.util.ArrayList;

import org.jivesoftware.smack.packet.Presence;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.letianpai.R;
import com.letianpai.adapter.NotifyAdapter;
import com.letianpai.db.RequestDBUtils;
import com.letianpai.db.RequestInformation;
import com.letianpai.service.XmppService;
import com.letianpai.utils.Utils;
import com.letianpai.utils.XmppTool;

public class NotifyActivity extends Activity {
	NotifyAdapter np;
	ListView listview;
	TextView headShare, headText;
	View headBackText;
	ArrayList<RequestInformation> listChat;
	SharedPreferences sp;
	View notify_none;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.notify_information);
		sp = getSharedPreferences("myPref", MODE_WORLD_READABLE);

		listview = (ListView) findViewById(R.id.listView);
		initView();
		listview.setOnItemClickListener(new ItemLongClick());
		getRequest();
	}

	// 初始化
	public void initView() {
		headBackText = findViewById(R.id.headBackText);
		headBackText.setOnClickListener(new LClick());
		headShare = (TextView) findViewById(R.id.headShare);
		headShare.setText("清空");
		headShare.setOnClickListener(new LClick());
		notify_none = findViewById(R.id.notify_none);
		headText = (TextView) findViewById(R.id.headText);
		headText.setText("通知");
	}

	// 获取消息
	public void getRequest() {
		// 获取历史通知记录
		listChat = RequestDBUtils.selectInformation(NotifyActivity.this,
				sp.getString("username", "1"));
		Log.d("zzzz", "历史通知" + listChat.size());
		if (listChat.size() != 0) {
			notify_none.setVisibility(View.GONE);
		}
		np = new NotifyAdapter(this, listChat);
		listview.setAdapter(np);
	}

	class LClick implements View.OnClickListener {

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.headBackText:
				finish();
				break;
			case R.id.headShare:
				RequestDBUtils.deleteNotify(NotifyActivity.this,
						sp.getString("username", "1"));
				listChat.clear();
				notify_none.setVisibility(View.VISIBLE);
				np.setArraylist(listChat);
				np.notifyDataSetChanged();
				break;
			default:
				break;
			}
		}

	}

	// 条目点击事件
	class ItemLongClick implements OnItemClickListener {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			Log.d("zzzz", "点击了");
			Presence subscription = new Presence(Presence.Type.subscribe);
			subscription.setTo(Utils.getUserNameToJid(listChat.get(position)
					.getuName()));
			XmppTool.con.sendPacket(subscription);
			Log.d("zzzz",
					Utils.getUserNameToJid(listChat.get(position).getuName()));
			XmppTool.getConnection().sendPacket(subscription);
			XmppService.addUserToGroup(
					Utils.getUserNameToJid(listChat.get(position).getuName()),
					"我的好友", XmppTool.con);
		}

	}
}
