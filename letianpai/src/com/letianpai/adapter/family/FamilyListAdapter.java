package com.letianpai.adapter.family;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.letianpai.R;
import com.letianpai.bean.FriendInfo;

public class FamilyListAdapter extends BaseAdapter {
    private LayoutInflater inflater;
    ArrayList<FriendInfo> arrayList;
    View.OnClickListener cliker;

    public FamilyListAdapter(Context context, ArrayList<FriendInfo> arrayList){
        this.arrayList = arrayList;
        inflater = LayoutInflater.from(context);
    }
    public FamilyListAdapter(Context context, ArrayList<FriendInfo> arrayList,
                             View.OnClickListener clicker) {
        this.arrayList = arrayList;
        inflater = LayoutInflater.from(context);
        this.cliker = clicker;
    }

    public void setArrayList(ArrayList<FriendInfo> arrayList) {
        this.arrayList = arrayList;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return arrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        view = inflater.inflate(R.layout.family_list, parent, false);
        TextView userName = (TextView) view.findViewById(R.id.topLeft);
        ImageView userImage = (ImageView) view.findViewById(R.id.userImage);
        userName.setText(arrayList.get(position).getNickname());
        Bitmap map = arrayList.get(position).getPic();
        if (!(null == map)) {
            userImage.setImageBitmap(map);
        }
        TextView message1 = (TextView) view.findViewById(R.id.message1);
        message1.setText(arrayList.get(position).getMessageCount() + "");

        if(arrayList.get(position).getMessageCount()==0){
            //未读消息为0
            message1.setVisibility(View.INVISIBLE);
        }
        ImageView phone = (ImageView) view.findViewById(R.id.bottomLeft2);
        ImageView sms_image = (ImageView) view.findViewById(R.id.sms_image);
        ImageView chat_image = (ImageView) view.findViewById(R.id.chat_image);
        phone.setOnClickListener(cliker);
        phone.setTag(position);
        sms_image.setOnClickListener(cliker);
        sms_image.setTag(position);
        chat_image.setOnClickListener(cliker);
        chat_image.setTag(position);
        return view;
    }
}
