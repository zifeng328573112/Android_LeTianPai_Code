package com.letianpai.http.homeland;

public class AnnBean {
	private String roomid;
	private String username;
	private String title;
	private String period;
	private String advancedays;
	private String note;
	private String action;
	public String getRoomid() {
		return roomid;
	}
	public void setRoomid(String roomid) {
		this.roomid = roomid;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getPeriod() {
		return period;
	}
	public void setPeriod(String period) {
		this.period = period;
	}
	public String getAdvancedays() {
		return advancedays;
	}
	public void setAdvancedays(String advancedays) {
		this.advancedays = advancedays;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	
}
