package com.letianpai.utils;

import java.util.HashMap;
import java.util.Map;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class DatabaseDAO{
	private SQLiteDatabase db;
	
	public DatabaseDAO(SQLiteDatabase db){
		this.db = db;
	}
	
	/**閼惧嘲褰囬幐鍥х暰閸栧搫褰块惃鍕阜娴犺棄鎷伴崷鏉垮隘閸氾拷*/
	public Map<String,String> queryAeraCode(String number){
		return queryNumber("0", number);
	}

	/**閼惧嘲褰囬幐鍥х暰閸欓鐖滈惃鍕阜娴犺棄鎷伴崷鏉垮隘閸氬秲锟�
	 * <code>select city_id from number_0 limit arg1,arg2.</code>
	 * arg1鐞涖劎銇氭禒搴ｎ儑閸戠姾顢戦敍鍫ｎ攽閺侀绮犻梿璺虹磻婵绱氬锟筋瀶閿涘畮rg2鐞涖劎銇氶弻銉嚄閸戠姾顢戦弫鐗堝祦.*/
	public Map<String,String> queryNumber(String prefix, String center){
		Map<String, String> map = new HashMap<String, String>();
		
		if (center.isEmpty() || !isTableExists("number_" + prefix)){
			map.put("province", "未知");
			map.put("city", "未知");
			return map;
	     }
		int num = Integer.parseInt(center) - 1;
		String sql1 = "select city_id from number_" + prefix + " limit " + num + ",1";//缁岀儤鐗告稉宥堝厴鐏忥拷
		String sql2 = "select province_id from city where _id = (" + sql1 + ")";
		String sql = "select province,city from province,city where _id=("+sql1+")and id=("+sql2+")";
		
		return getCursorResult(sql);
	}



	private Map<String, String> getCursorResult(String sql) {
		Cursor cursor = getCursor(sql);
		int col_len = cursor.getColumnCount();
		Map<String, String> map = new HashMap<String, String>();
		
		while (cursor.moveToNext()){
			for (int i = 0; i < col_len; i++){
				String columnName = cursor.getColumnName(i);
				String columnValue = cursor.getString(cursor.getColumnIndex(columnName));
				if (columnValue == null)
					columnValue = "";
				map.put(columnName, columnValue);
			}
		}
		cursor.close();
		return map;
	}

	private Cursor getCursor(String sql) {
		return  db.rawQuery(sql, null);
	}


	public boolean isTableExists(String tableName){
		boolean result = false;
		if (tableName == null)
			return false;
		Cursor cursor = null;
		try{
			String sql = "select count(*) as c from sqlite_master where type='table' and " +
					"name = '" + tableName.trim() +"' ";
			cursor = db.rawQuery(sql, null);
			if (cursor.moveToNext()){
				int count = cursor.getInt(0);
				if (count > 0)
					result = true;
			}
		}catch(Exception e){
			
		}
		cursor.close();
		return result;
	}
	

	public void closeDB(){
		if(db != null){
			db = null;
			db.close();
		}
	}
}