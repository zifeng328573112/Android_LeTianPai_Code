package com.letianpai.adapter.contacts;

import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.TextView;

import com.letianpai.R;
import com.letianpai.activity.contacts.ContactsInformationActivity;
import com.letianpai.bean.UserContacts;

public class ContactsInformationAdapter extends BaseAdapter {
    private Context context;
    private LayoutInflater inflater;
    UserContacts uc;
    List<String> userInformation;
    List<String> contactsName;

    public ContactsInformationAdapter(Context context,
                                      List<String> userInformation, List<String> contactsName) {
        super();
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.userInformation = userInformation;
        this.contactsName = contactsName;
    }

    @Override
    public int getCount() {
        return userInformation.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return userInformation.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View layout = inflater.inflate(R.layout.contactslist_information,
                parent, false);
        TextView information = (TextView) layout.findViewById(R.id.information);
        final TextView user_information = (TextView) layout
                .findViewById(R.id.user_information);
     
        information.setText(contactsName.get(position));
        user_information.setText(userInformation.get(position));
      
        if(contactsName.get(position).equals("电话")){
        	user_information.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
					Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"
							+ user_information.getText().toString()));
					context.startActivity(intent);
				}
			});
        }
       
        
        return layout;
    }
}
