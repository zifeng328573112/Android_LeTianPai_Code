package com.letianpai.http.homeland;

public class BackGetRoom {
	private String status;
	private BackBean[] data;
	private String message;
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public BackBean[] getData() {
		return data;
	}
	public void setData(BackBean[] data) {
		this.data = data;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
}
