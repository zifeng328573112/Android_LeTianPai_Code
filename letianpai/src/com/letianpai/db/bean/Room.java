package com.letianpai.db.bean;

import java.io.Serializable;

/**
 * Created by Administrator on 2014/10/8.
 */
public class Room implements Serializable {
    private String roomname;
    private String roomId;

    public String getRoomname() {
        return roomname;
    }

    public void setRoomname(String roomname) {
        this.roomname = roomname;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }
}

