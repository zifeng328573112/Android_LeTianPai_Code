package com.letianpai.application;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

import android.app.Activity;
import android.app.Application;
import com.letianpai.common.client.CommonTool;

public class ManageApplication extends Application {

    private List<Activity> mList = new LinkedList<Activity>();
    private static ManageApplication instance;

    public ManageApplication() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        //初始化
        File projectFile = new File(CommonTool.DATA_FILE_PATH);
        if (!projectFile.exists()) {
            projectFile.mkdirs();
        }
        File imageFile = new File(CommonTool.PROJECT_IMAGE_PATH);
        if (!imageFile.exists()) {
            imageFile.mkdirs();
        }
        File videoFile = new File(CommonTool.PROJECT_VIDEO_PATH);
        if (!videoFile.exists()) {
            videoFile.mkdirs();
        }
        File tempFile = new File(CommonTool.PROJECT_TEMP_PATH);
        if (!tempFile.exists()) {
            tempFile.mkdirs();
        }
        File cacheFile = new File(CommonTool.PROJECT_CACHE_PATH);
        if (!cacheFile.exists()) {
            cacheFile.mkdirs();
        }

        // CommonTool.DATABASE_PATH;
    }

    public synchronized static ManageApplication getInstance() {
        if (null == instance) {
            instance = new ManageApplication();
        }
        return instance;
    }

    public void addActivity(Activity activity) {
        mList.add(activity);
    }

    // ע��
    public void unLogin() {
        try {
            for (Activity activity : mList) {
                if (activity != null)
                    activity.finish();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        System.gc();
    }

    public void finishAll() {
        for (Activity activity : mList) {
            activity.finish();
        }
        System.exit(0);
    }
}
