package com.letianpai.activity.dial;

import android.app.Activity;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.util.Log;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.letianpai.R;
import com.letianpai.activity.contacts.AddContactsActivity;
import com.letianpai.bean.UserCallLog;
import com.letianpai.bean.UserContacts;
import com.letianpai.common.tool.StringUtil;
import com.letianpai.utils.ContactsUtils;
import com.letianpai.utils.DialUtils;
import com.umeng.analytics.MobclickAgent;

/**
 * 拨号***
 */
public class DialActivity extends Activity {
    View num1, num2, num3, num4, num5, num6, num7, num8, num9, num0;
    StringBuffer number = new StringBuffer("");
    TextView callNum;
    View call;
    View delete;
    View backCallLog;
    int i = 0;
    ImageView userImage;
    TextView name;
    View contentLayout;
    Button addPhoneNumBtn;

    private SensorManager sensorManager = null;
    private Vibrator vibrator;
    private static final int SENSOR_SHAKE = 10;


    /**
     * 动作执行
     */
    /**
     * 重力感应监听
     */
    private SensorEventListener sensorEventListener = new SensorEventListener() {

        @Override
        public void onSensorChanged(SensorEvent event) {
            // 传感器信息改变时执行该方法
            float[] values = event.values;
            float x = values[0]; // x轴方向的重力加速度，向右为正
            float y = values[1]; // y轴方向的重力加速度，向前为正
            float z = values[2]; // z轴方向的重力加速度，向上为正
            //   Log.i("info", "x轴方向的重力加速度" + x +  "；y轴方向的重力加速度" + y +  "；z轴方向的重力加速度" + z);
            // 一般在这三个方向的重力加速度达到40就达到了摇晃手机的状态。
            int medumValue = 19;// 三星 i9250怎么晃都不会超过20，没办法，只设置19了
            if (Math.abs(x) > medumValue || Math.abs(y) > medumValue || Math.abs(z) > medumValue) {
                vibrator.vibrate(200);
                Message msg = new Message();
                msg.what = SENSOR_SHAKE;
                handler.sendMessage(msg);
            }
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {

        }

    };
    Handler handler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case SENSOR_SHAKE:
                    GetDailrecord();
                    Toast.makeText(DialActivity.this, "摇一摇拨号", Toast.LENGTH_SHORT).show();
                    Log.i("info", "检测到摇晃，执行操作！");
                    break;
            }
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.call_num);
        initView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onPageStart("SplashScreen"); // 统计页面
        MobclickAgent.onResume(this); // 统计时长

        if (null != sensorManager) {// 注册监听器
            Sensor mAccelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
            sensorManager.registerListener(sensorEventListener, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
            // 第一个参数是Listener，第二个参数是所得传感器类型，第三个参数值获取传感器信息的频率
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd("SplashScreen"); // 保证 onPageEnd 在onPause
        // 之前调用,因为 onPause 中会保存信息
        MobclickAgent.onPause(this);

        if (null != sensorManager) {// 取消监听器
            sensorManager.unregisterListener(sensorEventListener);
        }
    }

    public void initView() {

        callNum = (TextView) findViewById(R.id.callNum);
        call = findViewById(R.id.call);
        delete = findViewById(R.id.delete);
        name = (TextView) findViewById(R.id.topLeft);
        backCallLog = findViewById(R.id.family);
        userImage = (ImageView) findViewById(R.id.userImage);
        contentLayout = findViewById(R.id.contentLayout);
        num1 = findViewById(R.id.num1);
        num2 = findViewById(R.id.num2);
        num3 = findViewById(R.id.num3);
        num4 = findViewById(R.id.num4);
        num5 = findViewById(R.id.num5);
        num6 = findViewById(R.id.num6);
        num7 = findViewById(R.id.num7);
        num8 = findViewById(R.id.num8);
        num9 = findViewById(R.id.num9);
        num0 = findViewById(R.id.num0);
        addPhoneNumBtn = (Button) findViewById(R.id.addPhoneNum);
        num1.setOnClickListener(new NumberListener());
        num2.setOnClickListener(new NumberListener());
        num3.setOnClickListener(new NumberListener());
        num4.setOnClickListener(new NumberListener());
        num5.setOnClickListener(new NumberListener());
        num6.setOnClickListener(new NumberListener());
        num7.setOnClickListener(new NumberListener());
        num8.setOnClickListener(new NumberListener());
        num9.setOnClickListener(new NumberListener());
        num0.setOnClickListener(new NumberListener());
        call.setOnClickListener(new NumberListener());
        delete.setOnClickListener(new NumberListener());
        delete.setOnLongClickListener(new DialOnLongClick());
        backCallLog.setOnClickListener(new NumberListener());
        addPhoneNumBtn.setOnClickListener(new NumberListener());

        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);

    }

    class NumberListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            UserContacts uc = new UserContacts();
            addPhoneNumBtn.setVisibility(View.VISIBLE);
            switch (v.getId()) {
                case R.id.num1:
                    number.append(1);
                    callNum.setText(number);
                    DialUtils.playMusic(DialActivity.this, R.raw.sound1);
                    uc = ContactsUtils.getContactsInformation(
                            DialActivity.this, number + "");
                    DialUtils.displayContactsInformation(contentLayout, uc, userImage,
                            name);
                    break;
                case R.id.num2:
                    number.append(2);
                    callNum.setText(number);
                    DialUtils.playMusic(DialActivity.this, R.raw.sound2);
                    uc = ContactsUtils.getContactsInformation(
                            DialActivity.this, number + "");
                    DialUtils.displayContactsInformation(contentLayout, uc, userImage,
                            name);
                    break;
                case R.id.num3:
                    number.append(3);
                    callNum.setText(number);
                    DialUtils.playMusic(DialActivity.this, R.raw.sound3);
                    uc = ContactsUtils.getContactsInformation(
                            DialActivity.this, number + "");
                    DialUtils.displayContactsInformation(contentLayout, uc, userImage,
                            name);
                    break;
                case R.id.num4:
                    number.append(4);
                    callNum.setText(number);
                    DialUtils.playMusic(DialActivity.this, R.raw.sound4);
                    uc = ContactsUtils.getContactsInformation(
                            DialActivity.this, number + "");
                    DialUtils.displayContactsInformation(contentLayout, uc, userImage,
                            name);
                    break;
                case R.id.num5:
                    number.append(5);
                    callNum.setText(number);
                    DialUtils.playMusic(DialActivity.this, R.raw.sound5);
                    uc = ContactsUtils.getContactsInformation(
                            DialActivity.this, number + "");
                    DialUtils.displayContactsInformation(contentLayout, uc, userImage,
                            name);
                    break;
                case R.id.num6:
                    number.append(6);
                    callNum.setText(number);
                    DialUtils.playMusic(DialActivity.this, R.raw.sound6);
                    uc = ContactsUtils.getContactsInformation(
                            DialActivity.this, number + "");
                    DialUtils.displayContactsInformation(contentLayout, uc, userImage,
                            name);
                    break;
                case R.id.num7:
                    number.append(7);
                    callNum.setText(number);
                    DialUtils.playMusic(DialActivity.this, R.raw.sound7);
                    uc = ContactsUtils.getContactsInformation(
                            DialActivity.this, number + "");
                    DialUtils.displayContactsInformation(contentLayout, uc, userImage,
                            name);
                    break;
                case R.id.num8:
                    number.append(8);
                    callNum.setText(number);
                    DialUtils.playMusic(DialActivity.this, R.raw.sound8);
                    uc = ContactsUtils.getContactsInformation(
                            DialActivity.this, number + "");
                    DialUtils.displayContactsInformation(contentLayout, uc, userImage,
                            name);
                    break;
                case R.id.num9:
                    number.append(9);
                    callNum.setText(number);
                    DialUtils.playMusic(DialActivity.this, R.raw.sound9);
                    uc = ContactsUtils.getContactsInformation(
                            DialActivity.this, number + "");
                    DialUtils.displayContactsInformation(contentLayout, uc, userImage,
                            name);
                    break;
                case R.id.num0:
                    number.append(0);
                    callNum.setText(number);
                    DialUtils.playMusic(DialActivity.this, R.raw.sound0);
                    uc = ContactsUtils.getContactsInformation(
                            DialActivity.this, number + "");
                    DialUtils.displayContactsInformation(contentLayout, uc, userImage,
                            name);
                    break;
                case R.id.call:
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"
                            + number));
                    DialActivity.this.startActivity(intent);
                    DialUtils.playMusic(DialActivity.this, R.raw.dtmf11);

                    break;
                case R.id.delete:
                    if (number.length() > 0) {
                        number.delete(number.length() - 1, number.length());
                        callNum.setText(number);
                    }
                    if (number.length() == 0) {
                        addPhoneNumBtn.setVisibility(View.GONE);
                    }
                    DialUtils.playMusic(DialActivity.this, R.raw.dtmf11);
                    uc = ContactsUtils.getContactsInformation(
                            DialActivity.this, number + "");
                /*DialUtils.displayContactsInformation(contentLayout, uc, userImage,
                        name);*/
                    break;

                case R.id.family:
                    GetDailrecord();
                    break;
                case R.id.addPhoneNum:
                    Intent intentAdd = new Intent(DialActivity.this,
                            AddContactsActivity.class);
                    intentAdd.putExtra("phoneNumber", String.valueOf(number));
                    startActivity(intentAdd);
                    break;
                default:
                    break;
            }
        }


    }


    public void GetDailrecord() {
        DialUtils.playMusic(DialActivity.this, R.raw.on);
        String currNumber = DialUtils.getNumber(DialActivity.this, i);
        if (StringUtil.isNotBlank(currNumber)) {
            number = new StringBuffer();
            number.append(currNumber);
            callNum.setText(number);
            UserCallLog ucl = DialUtils.getInformation(DialActivity.this,
                    number + "");
            DialUtils.displayInformation(contentLayout, ucl, userImage,
                    name);
            i++;
        } else {
            i = 0;

        }


//        number.delete(0, number.length());
//        number.append(DialUtils.getNumber(DialActivity.this, i));
//        callNum.setText(number);
//        String a = number + "";
//
//        if (StringUtil.isNotBlank(a)) {
//
//        }
    }

    class DialOnLongClick implements OnLongClickListener {

        @Override
        public boolean onLongClick(View v) {
            number.delete(0, number.length());
            callNum.setText(number);
            return false;
        }

    }
}