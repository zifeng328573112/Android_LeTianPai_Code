package com.letianpai.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.letianpai.R;
import com.letianpai.bean.UserCallLog;

/*
 * 联系人所有通话记录适配器
 */
public class UserCallLogAdapter extends BaseAdapter {
    private Context context;
    private LayoutInflater inflater;
    public UserCallLog ucl;
    List<UserCallLog> arraylist = new ArrayList<UserCallLog>();

    public UserCallLogAdapter(Context context, List<UserCallLog> arraylist) {
        super();
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.arraylist = arraylist;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return arraylist.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return arraylist.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        // TODO Auto-generated method stub
        ViewHolder viewHolder = null;
        if (view == null) {
            viewHolder = new ViewHolder();
            view = inflater.inflate(R.layout.user_calllog, parent, false);
            viewHolder.callIcon = (ImageView) view.findViewById(R.id.callIcon);
            viewHolder.time = (TextView) view.findViewById(R.id.time);
            viewHolder.time2 = (TextView) view.findViewById(R.id.time2);
            viewHolder.duration = (TextView) view.findViewById(R.id.duration);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
       String time_t= arraylist.get(position).getConverseTime().substring(5, 11);
     //   time_t=time_t.replace("-", "月")+"日";
        viewHolder.time.setText(time_t);
        viewHolder.time2.setText(arraylist.get(position).getConverseTime().substring(11, 17));
        viewHolder.duration.setText(arraylist.get(position).getDuration());
        int type = arraylist.get(position).getCallType();
        switch (type) {
            case 1:
                viewHolder.callIcon.setImageResource(R.drawable.call_go_icon);
                break;
            case 2:
                viewHolder.callIcon.setImageResource(R.drawable.call_out_icon);
                break;
            case 3:
                viewHolder.callIcon.setImageResource(R.drawable.call_hang_icon);
                break;
            default:
                break;
        }
        return view;
    }

    final static class ViewHolder {
        ImageView callIcon;
        TextView time;
        TextView time2;
        TextView duration;
    }

}
