package com.letianpai.db.entity;

/**
 * 选中的图片
 */
public class PictureInfo {
    private String id;
    private String picPath;
    private String picName;
    private String picType;
    private String picMd5;
    private boolean whetherSelect = false;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPicPath() {
        return picPath;
    }

    public void setPicPath(String picPath) {
        this.picPath = picPath;
    }

    public String getPicName() {
        return picName;
    }

    public void setPicName(String picName) {
        this.picName = picName;
    }

    public String getPicType() {
        return picType;
    }

    public void setPicType(String picType) {
        this.picType = picType;
    }

    public String getPicMd5() {
        return picMd5;
    }

    public void setPicMd5(String picMd5) {
        this.picMd5 = picMd5;
    }

    public boolean isWhetherSelect() {
        return whetherSelect;
    }

    public void setWhetherSelect(boolean whetherSelect) {
        this.whetherSelect = whetherSelect;
    }
}

