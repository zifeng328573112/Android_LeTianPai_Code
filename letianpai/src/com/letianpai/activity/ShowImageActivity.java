package com.letianpai.activity;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.view.Window;
import android.widget.ImageView;

import com.letianpai.R;
import com.letianpai.utils.ChatUtils;
/*
 * 点击放大图片
 */
public class ShowImageActivity extends Activity{
    public static String RECORD_ROOT_PATH = Environment
            .getExternalStorageDirectory().getPath() + "/chat/record/";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.show_image);
        initView();
    }
    private void initView() {
        ImageView show = (ImageView) findViewById(R.id.show_image);
        String filePath = getIntent().getStringExtra("filepath");
        String from = getIntent().getStringExtra("from");
        if(from.equals("OUT")){
            Bitmap bitmap = ChatUtils.decodeBitmap(filePath,500);
            show.setImageBitmap(bitmap);
        }else if(from.equals("IN")){
            Bitmap bitmap = ChatUtils.decodeBitmap(RECORD_ROOT_PATH+filePath,500);
            show.setImageBitmap(bitmap);
        }

    }
}
