/*
 * SimpleIDGenerator.java        1.0     2009-7-3  9:58:54
 *
 * Copyright (c) 2004, 2005 Works Systems, Inc.
 * Copyright (c) 2004, 2005 Works Systems (Tianjin) Co., Ltd.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Works Systems, Inc and Works Systems (Tianjin) Co., Ltd. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Works Systems.
 */
package com.letianpai.common.tool;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by IntelliJ IDEA.
 * User: Chris.F
 * Date: 2009-7-3
 * Time: 9:58:54
 */
public final class IntIDGenerator {
    private AtomicInteger uniqueId = new AtomicInteger(0);

    public int nextId() {
        return uniqueId.incrementAndGet();
    }
}
