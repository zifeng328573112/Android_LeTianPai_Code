package com.letianpai.http.RequestBean;

public class GetInformationBean {
	private String head;
	private GetInformation content;
	public String getHead() {
		return head;
	}
	public void setHead(String head) {
		this.head = head;
	}
	public GetInformation getContent() {
		return content;
	}
	public void setContent(GetInformation content) {
		this.content = content;
	}
	
}
