package com.letianpai.http.homeland;

public class GetRoomData {
	private String status;
	private GetRoom data;
	private String message;
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public GetRoom getData() {
		return data;
	}
	public void setData(GetRoom data) {
		this.data = data;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
}
