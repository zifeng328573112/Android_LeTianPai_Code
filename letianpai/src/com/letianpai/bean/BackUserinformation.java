package com.letianpai.bean;

import com.letianpai.http.RequestBean.UserinformationData;

public class BackUserinformation {
	private String status;
	private UserinformationData data;
	private String message;
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public UserinformationData getData() {
		return data;
	}
	public void setData(UserinformationData data) {
		this.data = data;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
}
