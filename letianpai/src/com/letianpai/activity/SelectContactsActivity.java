package com.letianpai.activity;

import java.util.ArrayList;
import java.util.List;

import com.letianpai.R;
import com.letianpai.adapter.contacts.SeekAdapter;
import com.letianpai.bean.UserContacts;
import com.letianpai.utils.ContactsUtils;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.TextView;

public class SelectContactsActivity extends Activity {
    GridView listview;
    SeekAdapter adapter;
    List<UserContacts> arraylist = new ArrayList<UserContacts>();
    View headBackText;
    TextView headShare;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.select_layout);
        listview = (GridView) findViewById(R.id.listView);
        arraylist = ContactsUtils.getContacts(this);
        adapter = new SeekAdapter(this, arraylist);
        headBackText = findViewById(R.id.headBackText);
        headBackText.setOnClickListener(new Contactback());
        headShare = (TextView) findViewById(R.id.headShare);
        headShare.setText("");
        listview.setAdapter(adapter);
        listview.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                Intent intent = new Intent();
                intent.putExtra("contact",
                        arraylist.get(arg2).getNumber().get(0));
                setResult(200, intent);
                finish();
            }
        });
    }

    class Contactback implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.headBackText:
                    Intent intent = new Intent();
                    intent.putExtra("contact", "");
                    setResult(200, intent);
                    finish();
                    break;

                default:
                    break;
            }
        }

    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            Intent intent = new Intent();
            intent.putExtra("contact", "");
            setResult(200, intent);
            finish();
        }
        return false;
    }
}
