package com.letianpai.http.homeland;

public class GetRoom {
	private String ofroomname;
	private String ofroomid;
	private String gname;
	private String owner;
	private String max_num;
	private String user_num;
	private String addtime;
	private String status;
	private String id;
	private String create_time;
	private String passwd;
	private String gicon;
	
	public String getGicon() {
		return gicon;
	}
	public void setGicon(String gicon) {
		this.gicon = gicon;
	}
	public String getPasswd() {
		return passwd;
	}
	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}
	public String getOfroomname() {
		return ofroomname;
	}
	public void setOfroomname(String ofroomname) {
		this.ofroomname = ofroomname;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCreate_time() {
		return create_time;
	}
	public void setCreate_time(String create_time) {
		this.create_time = create_time;
	}
	public String getOfroomid() {
		return ofroomid;
	}
	public void setOfroomid(String ofroomid) {
		this.ofroomid = ofroomid;
	}
	public String getGname() {
		return gname;
	}
	public void setGname(String gname) {
		this.gname = gname;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public String getMax_num() {
		return max_num;
	}
	public void setMax_num(String max_num) {
		this.max_num = max_num;
	}
	public String getUser_num() {
		return user_num;
	}
	public void setUser_num(String user_num) {
		this.user_num = user_num;
	}
	public String getAddtime() {
		return addtime;
	}
	public void setAddtime(String addtime) {
		this.addtime = addtime;
	}
	
}
